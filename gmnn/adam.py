import tensorflow as tf
from typing import *


class Adam(tf.Module):
    """ Implementation of the Adam optimization algorithm which allows to set different learning rates for
    different variables.

    :param vars: Variables which should be optimized.
    :param beta_1: :math:`\\beta_1` parameter of Adam (momentum).
    :param beta_2: :math:`\\beta_2` parameter of Adam (momentum for second-order statistics).
    :param eps: :math:`\\epsilon` parameter of Adam (to avoid division by zero).

    Example:

    .. highlight:: python
    .. code-block:: python

        ...

        from gmnn.adam import Adam

        ...

        # determine optimizer
        optimizers = [Adam(model.trainable_variables, beta_1=0.9, beta_2=0.999) for model in models]

        ...

        # apply gradients
        for i in range(len(models)):
            optimizers[i].step(gradients[i])
    """
    def __init__(self, vars: List[tf.Variable], beta_1: float = 0.9, beta_2: float = 0.95, eps: float = 1e-7):
        super().__init__()
        self.n_vars = len(vars)
        self.vars = vars
        self.lr = tf.Variable(tf.zeros(shape=(self.n_vars,)), trainable=False)
        self.beta_1 = beta_1
        self.beta_2 = beta_2
        self.eps = eps
        self.means = [tf.Variable(tf.zeros(shape=vars[j].shape), trainable=False) for j in range(self.n_vars)]
        self.sq_means = [tf.Variable(tf.zeros(shape=vars[j].shape), trainable=False) for j in range(self.n_vars)]
        self.mean_missing = tf.Variable(1.0, trainable=False)
        self.sq_mean_missing = tf.Variable(1.0, trainable=False)

    def set_lr(self, lr: tf.Tensor):
        """ Update the learning rate.

        :param lr: Tensor of learning rates (separately for each trainable variable).
        """
        self.lr.assign(lr)

    def step(self, grads_list: List[tf.Tensor]):
        """ Perform an Adam optimization step with the provided gradients.

        :param grads_list: List of gradients for each variable provided in the constructor.
        """
        for j in range(self.n_vars):
            grad = grads_list[j]
            if grad is None:
                continue
            grad = tf.convert_to_tensor(grad)
            self.means[j].assign(self.beta_1 * self.means[j] + (1.0 - self.beta_1) * grad)
            self.sq_means[j].assign(self.beta_2 * self.sq_means[j] + (1.0 - self.beta_2) * (grad**2))

            self.mean_missing.assign(self.beta_1 * self.mean_missing)
            self.sq_mean_missing.assign(self.beta_2 * self.sq_mean_missing)

            mean_correction = 1.0 / (1.0 - self.mean_missing)
            sq_mean_correction = 1.0 / (1.0 - self.sq_mean_missing)

            self.vars[j].assign(self.vars[j] - self.lr[j] * mean_correction * self.means[j]
                                / tf.sqrt(sq_mean_correction * self.sq_means[j] + self.eps))
