from ase import Atoms
import numpy as np
from typing import *
import tensorflow as tf
from ase.neighborlist import NeighborList


def simple_neighbor_list(n_at: int) -> Tuple[np.ndarray, np.ndarray]:
    """A simple neighbor list provider for small molecules where all atoms are each other’s neighbors.
    It calculates full distance matrices and does not support cutoffs or periodic boundary conditions.

    :param n_at: Number of atoms in the structure.
    :return: Central and neighboring atoms indices of shape (n_neighbors, ).

    Example:

    .. highlight:: python
    .. code-block:: python

        import numpy as np
        from ase.io import write, read
        from gmnn.neighborlist import simple_neighbor_list

        atoms = read("ethanol.xyz")
        idx_i, idx_j = simple_neighbor_list(n_at=len(atoms))
        offsets = np.zeros([len(atoms) * (len(atoms) - 1), 3], dtype=float)

        print(idx_j)
        #[[1 2 3 4 5 6 7 8]
        # [0 2 3 4 5 6 7 8]
        # [0 1 3 4 5 6 7 8]
        # [0 1 2 4 5 6 7 8]
        # [0 1 2 3 5 6 7 8]
        # [0 1 2 3 4 6 7 8]
        # [0 1 2 3 4 5 7 8]
        # [0 1 2 3 4 5 6 8]
        # [0 1 2 3 4 5 6 7]]

    .. note::
        An example ethanol structure can be taken from `Ethanol <https://github.com/nutjunkie/IQmol/blob/master/share/fragments/Molecules/Alcohols/Ethanol.xyz>`_.
    """
    idx_i = np.empty([n_at, n_at - 1], dtype=int)
    idx_j = np.empty([n_at, n_at - 1], dtype=int)
    for i in range(idx_i.shape[0]):
        for j in range(idx_i.shape[1]):
            idx_i[i, j] = i
    for i in range(idx_j.shape[0]):
        c = 0
        for j in range(idx_j.shape[0]):
            if j != i:
                idx_j[i, c] = j
                c += 1

    return idx_i, idx_j


class NeighborListASE:
    """Neighbor list provider making use of ASE neighbor lists. Supports cutoffs and periodic boundary conditions.

    :param cutoff: Cutoff radius used to define neighborlist. Please define the cutoff in accordance with
                   employed units.
    :param n_at: Number of atoms in the structure.

    Example:

    .. highlight:: python
    .. code-block:: python

        from ase.io import write, read
        from gmnn.neighborlist import NeighborListASE

        atoms = read("ethanol.xyz")
        nl = NeighborListASE(cutoff=6.5, n_at=len(atoms))
        idx_i, idx_j, offsets = nl.neighbor_list(atoms)

        print(idx_j)
        # [1 2 3 4 5 6 7 8 2 3 4 5 6 7 8 0 3 4 5 6 7 8 0 1 4 5 6 7 8 0 1 2 5 6 7 8 0
        # 1 2 3 6 7 8 0 1 2 3 4 7 8 0 1 2 3 4 5 8 0 1 2 3 4 5 6 0 1 2 3 4 5 6 7]
    """
    def __init__(self, cutoff: float, n_at: int):
        self.cutoff = cutoff
        self.n_at = n_at

        cutoffs = np.asarray([cutoff / 2.0 for i_cutoffs in range(n_at)])
        self.nl = NeighborList(cutoffs, skin=0.0, self_interaction=False, bothways=True)

    def neighbor_list(self, atoms: Atoms) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        """
        :param atoms: Atomistic system.
        :return: Central and neighboring atoms indices of shape (n_neighbors, ) and offsets of shape (n_neighbors, 3).
        """
        self.nl.update(atoms)
        idx_i = []
        idx_j = []
        offsets = []

        for j in range(self.n_at):
            a, b = self.nl.get_neighbors(j)
            idx_i.extend([j] * len(a))
            idx_j.extend(a)
            offsets.extend(b)

        return np.array(idx_i), np.array(idx_j), np.array(offsets)


class NeighborListTF:
    """Neighbor list provider making use of TensorFlow library. Supports cutoffs and periodic boundary conditions.
    The code is adapted from `TorchAni <https://github.com/aiqm/torchani/blob/master/torchani/aev.py>`_.

    :param cutoff: Cutoff radius is used to define the local atomic neighborhood.
                   Please define the cutoff in accordance with the units used in the database.
    :param wrap_positions: Set ``True`` if positions of atoms need to be wrapped back to the cell.

    Example:

    .. highlight:: python
    .. code-block:: python

        from ase.io import write, read
        from gmnn.neighborlist import NeighborListTF

        atoms = read("ethanol.xyz")
        nl = NeighborListTF(cutoff=6.5, wrap_positions=False)
        idx_i, idx_j, offsets = nl.neighbor_list(atoms)

        print(idx_j)
        # tf.Tensor(
        # [1 2 3 4 5 6 7 8 2 3 4 5 6 7 8 0 3 4 5 6 7 8 0 1 4 5 6 7 8 0 1 2 5 6 7 8 0
        #  1 2 3 6 7 8 0 1 2 3 4 7 8 0 1 2 3 4 5 8 0 1 2 3 4 5 6 0 1 2 3 4 5 6 7], shape=(72,), dtype=int32)
    """
    def __init__(self, cutoff: float, wrap_positions: bool = False):
        self.cutoff = cutoff
        self.wrap_positions = wrap_positions

    def neighbor_list(self, atoms: Atoms) -> Tuple[tf.Tensor, tf.Tensor, tf.Tensor]:
        """
        :param atoms: Atomistic system.
        :return: Central and neighboring atoms indices of shape (n_neighbors, ) and offsets of shape (n_neighbors, 3).
        """
        coordinates = tf.convert_to_tensor(atoms.get_positions(wrap=self.wrap_positions), dtype=tf.float32)
        pbc = tf.convert_to_tensor(atoms.pbc)

        if not atoms.cell.any():
            cell = tf.eye(3, dtype=tf.float32)
        else:
            cell = tf.convert_to_tensor(atoms.cell, dtype=tf.float32)

        shifts = self._compute_shifts(cell=cell, pbc=pbc)

        # the returned indices are bi-directional
        idx_i, idx_j, idx_S = self._neighbor_pairs(coordinates, cell, shifts)

        return idx_i, idx_j, idx_S

    def _cartesian_prod(self, shifts_1, shifts_2, shifts_3):
        intermediate = tf.reshape(tf.stack(tf.meshgrid(shifts_1, shifts_2, indexing='ij'), axis=-1), (-1, 2))
        intermediate = tf.tile(intermediate[:, None, :], [1, len(shifts_3), 1])
        shifts_3 = tf.tile(shifts_3[None, :, None], [len(intermediate), 1, 1])
        return tf.reshape(tf.concat([intermediate, shifts_3], axis=2), [-1, 3])

    @tf.function
    def _compute_shifts(self, cell: tf.Tensor, pbc: tf.Tensor) -> tf.Tensor:

        reciprocal_cell = tf.transpose(tf.linalg.inv(cell))
        inv_distances = tf.norm(reciprocal_cell, ord=2, axis=-1)
        num_repeats = tf.cast(tf.math.ceil(self.cutoff * inv_distances), dtype=tf.int32)
        num_repeats = tf.where(pbc, num_repeats, tf.zeros_like(num_repeats))

        r1 = tf.range(start=1, limit=num_repeats[0] + 1)
        r2 = tf.range(start=1, limit=num_repeats[1] + 1)
        r3 = tf.range(start=1, limit=num_repeats[2] + 1)
        o = tf.zeros(1, dtype=tf.int32)

        return tf.concat(
            [
                self._cartesian_prod(r1, r2, r3),
                self._cartesian_prod(r1, r2, o),
                self._cartesian_prod(r1, r2, -r3),
                self._cartesian_prod(r1, o, r3),
                self._cartesian_prod(r1, o, o),
                self._cartesian_prod(r1, o, -r3),
                self._cartesian_prod(r1, -r2, r3),
                self._cartesian_prod(r1, -r2, o),
                self._cartesian_prod(r1, -r2, -r3),
                self._cartesian_prod(o, r2, r3),
                self._cartesian_prod(o, r2, o),
                self._cartesian_prod(o, r2, -r3),
                self._cartesian_prod(o, o, r3),
            ],
            axis=0
        )

    @tf.function
    def _neighbor_pairs(self, coordinates: tf.Tensor, cell: tf.Tensor,
                        shifts: tf.Tensor) -> Tuple[tf.Tensor, tf.Tensor, tf.Tensor]:
        num_atoms = coordinates.shape[0]
        all_atoms = tf.range(num_atoms)

        # step 2: center cell
        # shape of triu indices: 2 x num_atoms * (num_atoms - 1) / 2
        p12_center = self._triu_indices(num_atoms)
        #p12_center = tf.convert_to_tensor(np.stack(np.triu_indices(num_atoms, 1)), dtype=tf.int32)
        shifts_center = tf.zeros(shape=(num_atoms * (num_atoms - 1) // 2, 3), dtype=tf.int32)

        # step 3: cells with shifts
        # shape convention (shift index, molecule index, atom index, 3)
        num_shifts = shifts.shape[0]
        all_shifts = tf.range(num_shifts)
        prod = tf.transpose(self._cartesian_prod(all_shifts, all_atoms, all_atoms))
        shift_index = prod[0]
        p12 = prod[1:]
        shifts_outside = tf.gather(shifts, shift_index, axis=0)

        # step 4: combine results for all cells
        shifts_all = tf.concat([shifts_center, shifts_outside], axis=0)
        p12_all = tf.concat([p12_center, p12], axis=1)
        shift_values = tf.matmul(tf.cast(shifts_all, dtype=cell.dtype), cell)

        # step 5: compute distances, and find all pairs within cutoff
        selected_coordinates = tf.reshape(tf.gather(coordinates, tf.reshape(p12_all, [-1]), axis=0), [2, -1, 3])
        distances = tf.norm((selected_coordinates[0] - selected_coordinates[1] + shift_values), 2, -1)
        in_cutoff = tf.where((distances < self.cutoff))[:, 0]

        p12_all_transposed = tf.transpose(p12_all)
        atom_index12 = tf.gather(p12_all_transposed, in_cutoff)
        shifts = tf.gather(shifts_all, in_cutoff)

        # step 6: prepare bi-directional indices and offsets
        shifts = tf.concat([-shifts, shifts], axis=0)
        atom_index1 = tf.concat([atom_index12[:, 0], atom_index12[:, 1]], axis=0)
        atom_index2 = tf.concat([atom_index12[:, 1], atom_index12[:, 0]], axis=0)

        indices = tf.argsort(atom_index1)
        atom_index1 = tf.gather(atom_index1, indices)
        atom_index2 = tf.gather(atom_index2, indices)
        shifts = tf.gather(shifts, indices)

        return atom_index1, atom_index2, tf.cast(shifts, dtype=tf.float32)

    def _triu_indices(self, num_atoms: int) -> tf.Tensor:
        bool_mat = tf.ones((num_atoms, num_atoms), dtype=tf.bool)
        indices = tf.where(~tf.linalg.band_part(bool_mat, -1, 0))
        indices = tf.cast(indices, dtype=tf.int32)
        return tf.transpose(indices)
