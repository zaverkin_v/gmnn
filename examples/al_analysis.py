import numpy as np
from typing import *
from pathlib import Path


def get_path():
    return './'


def writeToFile(filename, content):
    file = open(filename, 'w')
    file.truncate()
    file.write(content)
    file.close()


def escape(name: str):
    return name.replace('_', r'\_')


class ExperimentResults:
    def __init__(self, results_dict: dict):
        self.results_dict = results_dict

    def get_last_errors(self, key: str) -> 'ExperimentResults':
        return ExperimentResults({task_name: {
            alg_name: np.mean(alg_results[key], axis=0)[-1] if key in alg_results.keys() else None for
            alg_name, alg_results in task_results.items()} for task_name, task_results in self.results_dict.items()})

    def get_last_errors_std(self, key: str) -> 'ExperimentResults':
        return ExperimentResults({task_name: {
            alg_name: np.std(alg_results[key], axis=0)[-1] if key in alg_results.keys() else None for
            alg_name, alg_results in task_results.items()} for task_name, task_results in self.results_dict.items()})

    def get_learning_curves(self, key: str) -> 'ExperimentResults':
        return ExperimentResults({task_name: {
            alg_name: np.mean(alg_results[key], axis=0) if key in alg_results.keys() else None for alg_name, alg_results
            in task_results.items()} for task_name, task_results in self.results_dict.items()})

    def get_learning_curves_std(self, key: str) -> 'ExperimentResults':
        return ExperimentResults({task_name: {
            alg_name: np.std(alg_results[key], axis=0) if key in alg_results.keys() else None for alg_name, alg_results
            in task_results.items()} for task_name, task_results in self.results_dict.items()})

    @staticmethod
    def load() -> 'ExperimentalResults':
        results_path = Path(get_path())
        results_raw = np.load(results_path / 'all_results.npz', allow_pickle=True)

        results_dict = {}
        for ds in results_raw.keys():
            results_ds = results_raw[ds].item()
            for kernel in results_ds.keys():
                results_ds_kernel = results_ds[kernel]
                for selection in results_ds_kernel.keys():
                    results_ds_kernel_selection = results_ds_kernel[selection]

                    for iexp in range(len(results_ds_kernel_selection)):

                        results_ds_kernel_selection_iexp = results_ds_kernel_selection[iexp]

                        min_n_train = results_ds_kernel_selection_iexp['min_n_train']
                        max_n_train = results_ds_kernel_selection_iexp['max_n_train']
                        n_rp = results_ds_kernel_selection_iexp['n_rp']
                        max_al_batch_size = results_ds_kernel_selection_iexp['max_al_batch_size']

                        del results_ds_kernel_selection_iexp['min_n_train']
                        del results_ds_kernel_selection_iexp['max_n_train']
                        del results_ds_kernel_selection_iexp['n_rp']
                        del results_ds_kernel_selection_iexp['max_al_batch_size']

                        if ds + '_' + f'{min_n_train}' + '_' + f'{max_n_train}' + '_' + f'{max_al_batch_size}' in results_dict.keys():
                            results_dict[
                                ds + '_' + f'{min_n_train}' + '_' + f'{max_n_train}' + '_' + f'{max_al_batch_size}'].update(
                                {kernel + '_' + f'{n_rp}' + '_' + selection: {key: value for key, value in
                                                                              results_ds_kernel_selection_iexp.items()}})
                        else:
                            results_dict.update({
                                                    ds + '_' + f'{min_n_train}' + '_' + f'{max_n_train}' + '_' + f'{max_al_batch_size}': {
                                                        kernel + '_' + f'{n_rp}' + '_' + selection: {key: value for
                                                                                                     key, value in
                                                                                                     results_ds_kernel_selection_iexp.items()}}})
        return ExperimentResults(results_dict=results_dict)


def get_latex_method(task: str) -> str:
    conversion_dict = {'random_0_max_diag': r'MaxDiag+RND',
                       'qbc-energy_0_max_diag': r'MaxDiag+QBC(E)',
                       'qbc-force_0_max_diag': r'MaxDiag+QBC(F)',
                       'ae-energy_0_max_diag': r'MaxDiag+AE(E)',
                       'ae-force_0_max_diag': r'MaxDiag+AE(F)',
                       'F_inv_0_max_diag': r'MaxDiag+GP(LL)',
                       'F_inv_512_max_diag': r'MaxDiag+GP(RP)',
                       'F_inv_0_max_det_greedy': r'MaxDet+GP(LL)',
                       'F_inv_512_max_det_greedy': r'MaxDet+GP(RP)',
                       'F_inv_0_max_dist_greedy': r'MaxDist+GP(LL)',
                       'F_inv_512_max_dist_greedy': r'MaxDist+GP(RP)',
                       'g_0_max_dist_greedy': r'MaxDist+FEAT(LL)',
                       'g_512_max_dist_greedy': r'MaxDist+FEAT(RP)',
                       'g_0_lcmd_greedy': r'LCMD+FEAT(LL)',
                       'g_512_lcmd_greedy': r'LCMD+FEAT(RP)'}
    return conversion_dict[task]


def get_latex_kernel(task: str) -> str:
    conversion_dict = {'random_0_max_diag': r'RND',
                       'qbc-energy_0_max_diag': r'QBC(E)',
                       'qbc-force_0_max_diag': r'QBC(F)',
                       'ae-energy_0_max_diag': r'AE(E)',
                       'ae-force_0_max_diag': r'AE(F)',
                       'F_inv_0_max_diag': r'GP(LL)',
                       'F_inv_512_max_diag': r'GP(RP)',
                       'F_inv_0_max_det_greedy': r'GP(LL)',
                       'F_inv_512_max_det_greedy': r'GP(RP)',
                       'F_inv_0_max_dist_greedy': r'GP(LL)',
                       'F_inv_512_max_dist_greedy': r'GP(RP)',
                       'g_0_max_dist_greedy': r'FEAT(LL)',
                       'g_512_max_dist_greedy': r'FEAT(RP)',
                       'g_0_lcmd_greedy': r'FEAT(LL)',
                       'g_512_lcmd_greedy': r'FEAT(RP)'}
    return conversion_dict[task]


def get_latex_metric(task: str) -> str:
    conversion_dict = {'fmaxe': r'Force MAXE in kcal/mol/$\AA{}$',
                       'frmse': r'Force RMSE in kcal/mol/$\AA{}$',
                       'fmae': r'Force MAE in kcal/mol/$\AA{}$',
                       'emaxe': r'Energy MAXE in kcal/mol',
                       'ermse': r'Energy RMSE in kcal/mol',
                       'emae': r'Energy MAE in kcal/mol',
                       'selection_time': r'Selection time',
                       'matrix_time': r'Kernel time',
                       'epr': r'Energy $R$',
                       'esr': r'Energy $\rho$',
                       'fpr': r'Force $R$',
                       'fsr': r'Force $\rho$',
                       'eq95': r'Energy 95\%',
                       'fq95': r'Force 95\%'}
    return conversion_dict[task]


def get_latex_metric_table(task: str) -> str:
    conversion_dict = {'fmaxe': r'MAXE',
                       'frmse': r'RMSE',
                       'fmae': r'MAE',
                       'emaxe': r'MAXE',
                       'ermse': r'RMSE',
                       'emae': r'MAE',
                       'epr': r'PCC',
                       'esr': r'SRC',
                       'fpr': r'PCC',
                       'fsr': r'SRC',
                       'eq95': r'95\%',
                       'fq95': r'95\%',
                       'matrix_time': r'Kernel',
                       'selection_time': r'Selection'}
    return conversion_dict[task]


def save_latex_table_learning_curves_eng(results: ExperimentResults, filename: Union[str, Path],
                                         metric_names: List[str], task_name: str, alg_names: List[str]):
    # creates a table for all algorithms and all tasks
    table_header = '\\begin{tabular}{lcccccccccccccc}\n' + '\\toprule\\toprule\n' + \
                   'Method & \\multicolumn{6}{c}{Energy} & \\multicolumn{6}{c}{Force} ' \
                   '& \\multicolumn{2}{c}{Time}' + '\\\\\n' + '\\cmidrule(lr){2-7}\\cmidrule(lr){8-13}' \
                                                              '\\cmidrule(lr){14-15}' + \
                   ' & '.join([''] + [get_latex_metric_table(metric_name) for metric_name in metric_names]) + \
                   '\\\\\n\\midrule\\midrule\n'
    table_footer = '\\\\\n\\bottomrule\\bottomrule' + '\n\\end{tabular}'
    table_rows = []
    for alg_name in alg_names:
        metric_results = []
        for metric_name in metric_names:
            learning_curve = results.get_learning_curves(metric_name)
            learning_curve_std = results.get_learning_curves_std(metric_name)
            metric_results.append(
                '~' + f'{np.mean(learning_curve.results_dict[task_name][alg_name]):8.2f}' + '~' +
                '(' + f'{np.mean(learning_curve_std.results_dict[task_name][alg_name] / np.sqrt(4)):8.2f}'
                + ')' + '~')
        row_strs = [get_latex_method(alg_name)] + metric_results
        table_rows.append(' & '.join(row_strs))
    result_str = table_header + ' \\\\\n\\midrule\n'.join(table_rows) + table_footer
    table_name = Path(get_path()) / filename
    writeToFile(table_name, result_str)


def save_latex_table_last_error_eng(results: ExperimentResults, filename: Union[str, Path],
                                    metric_names: List[str], task_name: str, alg_names: List[str]):
    # creates a table for all algorithms and all tasks
    table_header = '\\begin{tabular}{lcccccccccccc}\n' + '\\toprule\\toprule\n' + \
                   'Method & \\multicolumn{6}{c}{Energy} & \\multicolumn{6}{c}{Force} ' \
                   + '\\\\\n' + '\\cmidrule(lr){2-7}\\cmidrule(lr){8-13}' + \
                   ' & '.join([''] + [get_latex_metric_table(metric_name) for metric_name in metric_names]) + \
                   '\\\\\n\\midrule\\midrule\n'
    table_footer = '\\\\\n\\bottomrule\\bottomrule' + '\n\\end{tabular}'
    table_rows = []
    for alg_name in alg_names:
        metric_results = []
        for metric_name in metric_names:
            last_error = results.get_last_errors(metric_name)
            last_error_std = results.get_last_errors_std(metric_name)
            metric_results.append(
                '~' + f'{np.mean(last_error.results_dict[task_name][alg_name]):8.2f}' + '~' +
                '(' + f'{np.mean(last_error_std.results_dict[task_name][alg_name]  / np.sqrt(4)):8.2f}'
                + ')' + '~')
        row_strs = [get_latex_method(alg_name)] + metric_results
        table_rows.append(' & '.join(row_strs))
    result_str = table_header + ' \\\\\n\\midrule\n'.join(table_rows) + table_footer
    table_name = Path(get_path()) / filename
    writeToFile(table_name, result_str)
