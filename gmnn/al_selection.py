from typing import *
import numpy as np
import tensorflow as tf
from gmnn.al_kernels import KernelMatrix


def max_diag(matrix: KernelMatrix, batch_size: int) -> tf.Tensor:
    """
    Selects batch elements by greedily selecting the indices of the largest diagonal entries in the kernel matrix.

    :param matrix: Kernel matrix.
    :param batch_size: Size of the active learning batch.
    :return: Indices of the selected structures.
    """
    diag = matrix.get_diag()
    return tf.argsort(diag, direction="DESCENDING")[:batch_size]


def max_det_greedy(matrix: KernelMatrix, batch_size: int) -> tf.Tensor:
    """
    Selects batch elements by greedily maximizing the determinant of the corresponding submatrix.

    :param matrix: Kernel matrix.
    :param batch_size: Size of the active learning batch.
    :return: Indices of the selected structures.
    """
    # shape: n_candidates
    l_nn_sq = matrix.get_diag()

    batch_idxs = [tf.argmax(l_nn_sq)]

    # shape: current_batch_size x n_candidates
    l_n = None

    for n in range(1, batch_size):
        opt_idx = batch_idxs[-1]
        # shape: n_candidates
        l_n_T_l_n = 0.0 if l_n is None else tf.einsum('w,wc->c', l_n[:, opt_idx], l_n)
        # shape: n_candidates
        mat_col = matrix.get_column(opt_idx)
        # shape: n_candidates
        update = (1.0 / tf.sqrt(l_nn_sq[batch_idxs[-1]])) * (mat_col - l_n_T_l_n)
        # shape: n_candidates
        l_nn_sq = l_nn_sq - update ** 2
        # shape: (n-1) x n_candidates
        l_n = update[None, :] if l_n is None else tf.concat([l_n, update[None, :]], axis=0)
        new_idx = tf.argmax(l_nn_sq)
        if l_nn_sq[new_idx] <= 1e-12 or new_idx in batch_idxs:
            break
            # determinant is numerically zero
        else:
            batch_idxs.append(new_idx)
    return tf.convert_to_tensor(batch_idxs)


def max_dist_greedy(matrix: KernelMatrix, batch_size: int, n_train: int) -> tf.Tensor:
    """
    Selects batch elements by greedily picking those with the maximum distance to the previously selected points,
    including training points. Assumes that the last ``n_train`` columns of ``matrix`` correspond to training points.

    :param matrix: Kernel matrix.
    :param batch_size: Size of the active learning batch.
    :param n_train: Number of training structures.
    :return: Indices of the selected structures.
    """
    # assumes that the matrix contains pool samples, optionally followed by train samples
    n_pool = matrix.get_num_columns() - n_train
    sq_dists = matrix.get_diag()
    batch_idxs = [n_pool if n_train > 0 else tf.argmax(sq_dists)]
    min_sq_dists = matrix.get_sq_dists(batch_idxs[-1])[:n_pool]

    for i in range(1, batch_size + n_train):
        batch_idxs.append(n_pool+i if i < n_train else tf.argmax(min_sq_dists))
        min_sq_dists = tf.math.minimum(min_sq_dists, matrix.get_sq_dists(batch_idxs[-1])[:n_pool])

    return tf.convert_to_tensor(batch_idxs[n_train:])


def lcmd_greedy(matrix: KernelMatrix, batch_size: int, n_train: int) -> tf.Tensor:
    """
    Selects batch elements by greedily picking those with the maximum distance in the largest cluster,
    including training points. Assumes that the last ``n_train`` columns of ``matrix`` correspond to training points.

    :param matrix: Kernel matrix.
    :param batch_size: Size of the AL batch.
    :param n_train: Number of training structures.
    :return: Indices of the selected structures.
    """
    # assumes that the matrix contains pool samples, optionally followed by train samples
    n_pool = matrix.get_num_columns() - n_train
    sq_dists = matrix.get_diag()
    batch_idxs = [n_pool if n_train > 0 else tf.argmax(sq_dists)]
    closest_idxs = tf.zeros(n_pool, dtype=tf.int32)
    min_sq_dists = matrix.get_sq_dists(batch_idxs[-1])[:n_pool]

    for i in range(1, batch_size + n_train):
        if i < n_train:
            batch_idxs.append(n_pool+i)
        else:
            bincount = tf.math.bincount(closest_idxs, weights=min_sq_dists, minlength=i)
            max_bincount = tf.math.reduce_max(bincount)
            new_idx = tf.argmax(tf.where(tf.gather(bincount, closest_idxs) == max_bincount, min_sq_dists, -np.Inf))
            batch_idxs.append(new_idx)
        sq_dists = matrix.get_sq_dists(batch_idxs[-1])[:n_pool]
        new_min = sq_dists < min_sq_dists
        closest_idxs = tf.where(new_min, i, closest_idxs)
        min_sq_dists = tf.where(new_min, sq_dists, min_sq_dists)

    return tf.convert_to_tensor(batch_idxs[n_train:])
