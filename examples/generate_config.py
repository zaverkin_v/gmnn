

if __name__ == "__main__":
    for ds in ["ethanol_1000K"]:
        for kernel, selection in [("random", "max_diag"), ("qbc-force", "max_diag"), ("ae-force", "max_diag"),
                                  ("F_inv", "max_diag"), ("F_inv", "max_det_greedy"), ("g", "max_dist_greedy"),
                                  ("g", "lcmd_greedy")]:
            n_models = 3 if kernel == "qbc-energy" or kernel == "qbc-force" else 1

            for n_random_projections in [0, 512] if kernel in ["F_inv", "g"] else [0]:
                for data_seed in range(5):
                    options = [f'--data_path={ds}.npz',
                               f'--data_seed={data_seed}',
                               f'--model_seed={data_seed}',
                               f'--n_models={n_models}',
                               f'--kernel={kernel}',
                               f'--selection={selection}',
                               f'--n_random_features={n_random_projections}',
                               f'--forces=1'
                               ]

                    # ethanol: 10 -> 100 [2, 5, 10] (total 5000 structures, 4000 reserved as the pool data)
                    step_configs = [(10, 100, 4000, [2, 5, 10])]
                    options.extend([
                                '--cutoff=4.0',
                                '--n_valid=200',
                                '--neighbor_list=0'
                            ])

                    for n_train, max_n_train, n_pool, max_al_batch_sizes in step_configs:
                        for max_al_batch_size in max_al_batch_sizes:
                            model_name = f'{data_seed}'
                            all_options = options + [
                                f'--model_path=models/{ds}_{kernel}_{selection}_{n_random_projections}_{n_train}_{max_n_train}_{max_al_batch_size}_{data_seed}',
                                '--model_name=model',
                                f'--n_pool={n_pool}',
                                f'--max_n_train={max_n_train}',
                                f'--n_train={n_train}',
                                f'--max_al_batch_size={max_al_batch_size}',
                                f'--batch_size={min(32, n_train)}'
                            ]
                            config_filename = f'{ds}_{kernel}_{selection}_{n_random_projections}_{n_train}_{max_n_train}_{max_al_batch_size}_{data_seed}.txt'
                            with open(config_filename, 'w') as f:
                                f.write('\n'.join(all_options))
