import numpy as np
from ase.io import read
from gmnn.calculators import MATCalculator

def magnetic_anisotropy(D):
    DT = np.zeros((3,3))
    DT[0, 0] = D[0]
    DT[0, 1] = D[1]
    DT[1, 0] = D[1]
    DT[0, 2] = D[2]
    DT[2, 0] = D[2]
    DT[1, 1] = D[3]
    DT[1, 2] = D[4]
    DT[2, 1] = D[4]
    DT[2, 2] = -(D[0]+D[3])
    w, _ = np.linalg.eigh(DT)
    return w[0]*3./2., abs(w[1] - w[2])*1./2.

if __name__ == '__main__':

    # read the inital structure
    atoms = read("cosar.xyz")

    # setup calculator
    calc = MATCalculator(atoms=atoms, model_path="/SCR/HADES2_I1/zaverkin/tutorials/cosar/models/example_1", 
                         device_number=0, use_all_features=True, config="mat_training.txt")

    # compute D-tensor
    print(calc.get_mat(atoms))
    # [-51.675316   38.28411    -7.2990475  16.714611    4.1719584]

    # compute uncertainty
    print(np.sqrt(calc.get_mat_variance(atoms)))
    # [0.00879334 0.00778365 0.01504688 0.0080507  0.00162213]

    # compute magnetic anisotropy
    print(magnetic_anisotropy(calc.get_mat(atoms)))
    # (-104.22383388236975, 1.2120193850543792)

