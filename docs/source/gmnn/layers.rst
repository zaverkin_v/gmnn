.. _layers:

=====================
Neural Network Layers
=====================

Example of a customized GM-NN model:

.. highlight:: python
.. code-block:: python

    import tensorflow as tf
    from gmnn.data_pipeline import *
    from gmnn.layers import *

    # define data pipeline
    dictionary = np.load('ethanol_1000K.npz')
    dc = DataContainer(dictionary, cutoff=6.5, neighbor_list=True, skin=0.0)
    dcs = dc.split({'train': 1000, 'valid': 200}, seed=1)

    # define architecture, 360 input features correspond to selected n_radial=5
    architecture = [360, 512, 512, 1]

    # define activation function
    activation = lambda x: 1.6765324703310907 * tf.nn.swish(x)

    # define the model
    models = []

    for i in range(3):
        # define layers
        layers =[GaussianMomentsLayer(n_radial=5, n_contr=8, n_basis=7, r_max=6.5,
                                      emb_init='uniform', use_all_features=True)]
        for j in range(len(architecture) - 1):
            layers.append(
                LinearLayer(in_features=architecture[j], out_features=architecture[j + 1],
                            init_normalization='ntk', weight_init_mode='normal',
                            bias_init_mode='zeros', bias_factor=0.1))
            if j < len(architecture) - 2:
                layers.append(ActivationLayer(activation))

        # final model
        model = ScaleShiftModel(Sequential(layers), atomic_energy_std=dcs['train'].EperA_stdev,
                                atomic_energy_regression=dcs['train'].EperA_regression)
        models.append(model)

    print(models)
    #[<gmnn.layers.ScaleShiftModel object at 0x7f0a40e4ab50>, <gmnn.layers.ScaleShiftModel object at 0x7f0a40e9fdd0>, <gmnn.layers.ScaleShiftModel object at 0x7f0a40e60590>]

Sequential
__________
.. autoclass:: gmnn.layers.Sequential
    :members:

Gaussian Moments Layer
______________________
.. autoclass:: gmnn.layers.GaussianMomentsLayer
    :members:

Pair Distances
______________
.. autofunction:: gmnn.utils.get_distance_vectors

Linear Layer
____________
.. autoclass:: gmnn.layers.LinearLayer
    :members:

Activation Layer
________________
.. autoclass:: gmnn.layers.ActivationLayer
    :members:

Batch Normalization
___________________
.. autoclass:: gmnn.layers.CustomBatchNorm
    :members:

Scale Shift Energy
__________________
.. autoclass:: gmnn.layers.ScaleShiftModel
    :members:

Scale Shift Tensor
__________________
.. autoclass:: gmnn.layers.ScaleShiftTensor
    :members:
