==================
MAT Training
==================

Trainer
_______
.. autoclass:: gmnn.trainer.MATTrainer
    :members:

Fit Model
____________________________________
.. autoclass:: gmnn.mat_fit.MATFit
    :members:
