import matplotlib
#matplotlib.use('pdf')
matplotlib.rcParams.update({
#    "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'font.size': 10,
    'text.usetex': True,
    'pgf.rcfonts': False,
    'text.latex.preamble': r'\usepackage{times} \usepackage{amsmath} \usepackage{amsfonts} \usepackage{amssymb}'
})
import matplotlib.pyplot as plt
from typing import *
import numpy as np
from al_analysis import *
from pathlib import Path
import seaborn as sns
import matplotlib.ticker as ticker
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)


fontsize = 12
axis_font = {'size': str(fontsize)}
sns.axes_style("whitegrid")


def get_path():
    return './'


def escape(name: str):
    return name.replace('_', r'\_')


def rgb_to_hex(rgb):
    return '#%02x%02x%02x' % rgb


def color_dict():
    # generated using https://github.com/taketwo/glasbey
    c_list = [(0, 0, 0), (174, 20, 20), (0, 85, 239), (0, 143, 0), (120, 78, 120), (148, 55, 252), (0, 184, 196),
              (239, 91, 255), (255, 110, 128), (114, 90, 36), (148, 155, 255), (19, 109, 100), (145, 180, 125),
              (225, 149, 10), (202, 16, 130), (97, 122, 157)]
    colors = [rgb_to_hex(c) for c in c_list]
    all_algs = ['random_0_max_diag', 'qbc-energy_0_max_diag', 'qbc-force_0_max_diag', 'ae-energy_0_max_diag',
                'ae-force_0_max_diag', 'F_inv_0_max_diag', 'F_inv_512_max_diag', 'F_inv_0_max_det_greedy',
                'F_inv_512_max_det_greedy', 'F_inv_0_max_dist_greedy', 'F_inv_512_max_dist_greedy',
                'g_0_max_dist_greedy', 'g_512_max_dist_greedy', 'g_0_lcmd_greedy', 'g_512_lcmd_greedy']
    return {alg: c for alg, c in zip(all_algs, colors)}


def symbol_dict():
    # generated using https://github.com/taketwo/glasbey
    symbols = ['s', 'o', 'o', 'o', 'o', 'o', 'o', '>', '>', '>', '>', '>', '>', 'd', 'd']
    all_algs = ['random_0_max_diag', 'qbc-energy_0_max_diag', 'qbc-force_0_max_diag', 'ae-energy_0_max_diag',
                'ae-force_0_max_diag', 'F_inv_0_max_diag', 'F_inv_512_max_diag', 'F_inv_0_max_det_greedy',
                'F_inv_512_max_det_greedy', 'F_inv_0_max_dist_greedy', 'F_inv_512_max_dist_greedy',
                'g_0_max_dist_greedy', 'g_512_max_dist_greedy', 'g_0_lcmd_greedy', 'g_512_lcmd_greedy']
    return {alg: c for alg, c in zip(all_algs, symbols)}


def plot_multiple_learning_curves(results: ExperimentResults, filename: Union[str, Path], metric_names: List[str],
                                  task_name: str, alg_names: List[str], n_points: int = 10):
    # all xlocs
    all_xlocs = np.array([8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16385, 32768])
    c_dict = color_dict()
    s_dict = symbol_dict()
    # get n_train
    n_train_min = int(task_name.split('_')[-3])
    n_train_max = int(task_name.split('_')[-2])
    n_batch_max = int(task_name.split('_')[-1])
    n_train = np.array([n_train_min + istep * n_batch_max for istep
                        in range((n_train_max - n_train_min) // n_batch_max + 1)])
    if len(n_train) > n_points:
        logspace = np.logspace(np.log10(n_train[0]), np.log10(n_train[-1]), n_points, endpoint=True)
        idxs = np.zeros(n_points, dtype=int)
        for ii in range(len(logspace)):
            dist = np.inf
            for jj in range(len(n_train)):
                if abs(n_train[jj] - logspace[ii]) < dist:
                    idxs[ii] = jj
                    dist = abs(n_train[jj] - logspace[ii])
    else:
        idxs = np.arange(len(n_train))
    # prepare plots
    fig, axs = plt.subplots(1, len(metric_names), figsize=(10, 4))
    xlocs = all_xlocs[np.argmin(abs(all_xlocs - n_train_min)):np.argmin(abs(all_xlocs - n_train_max)) + 1]
    for i, metric_name in enumerate(metric_names):
        ax = axs[i]
        ax.set_xscale('log')
        ax.xaxis.set_minor_locator(ticker.FixedLocator(xlocs))
        ax.xaxis.set_major_locator(ticker.NullLocator())
        ax.xaxis.set_minor_formatter(ticker.ScalarFormatter())
        learning_curves = results.get_learning_curves(metric_name)
        y_max = -np.inf
        y_min = np.inf
        for alg_name in alg_names:
            alg_results = learning_curves.results_dict[task_name][alg_name]
            if min(alg_results) < y_min:
                y_min = min(alg_results)
            if max(alg_results) > y_max:
                y_max = max(alg_results)
            ax.plot([], [], '--', marker=s_dict[alg_name], color=c_dict[alg_name], label=get_latex_method(alg_name))
            ax.plot(n_train, np.log(alg_results), '--', color=c_dict[alg_name])
            ax.plot(n_train[idxs], np.log(alg_results[idxs]), s_dict[alg_name], color=c_dict[alg_name])
            ax.set_xlabel(r'Training set size $N_{\mathrm{train}}$', **axis_font)
            ax.set_ylabel(f'{get_latex_metric(metric_name)}', **axis_font)

        logspace = np.round(np.logspace(np.log10(y_min), np.log10(y_max), 5, endpoint=True), 2)
        ylocs = [np.log(ival) for ival in logspace]
        ylabels = (f'{ival:8.2f}' for ival in logspace)
        ax.set_yticks(ylocs)
        ax.set_yticklabels(ylabels)

    ncol = len(alg_names) // 2
    if len(alg_names) % 2 != 0:
        ncol += 1
    # change the marker size manually for both lines
    fig.legend(*axs[0].get_legend_handles_labels(), loc='upper center', bbox_to_anchor=(0.5, 0.15), ncol=ncol)
    plt.tight_layout(rect=[0, 0.15, 1.0, 1.0])
    plot_name = Path(get_path()) / filename
    plt.savefig(plot_name)
    plt.close(fig)


def plot_multiple_batch_sizes(results: ExperimentResults, filename: Union[str, Path], metric_names: List[str],
                              task_names: List[str], alg_names: List[str]):
    c_dict = color_dict()
    s_dict = symbol_dict()

    fig, axs = plt.subplots(1, len(metric_names), figsize=(10, 4))
    for i, metric_name in enumerate(metric_names):
        ax = axs[i]
        last_errors = results.get_last_errors(metric_name)
        last_errors_std = results.get_last_errors_std(metric_name)
        y_max = -np.inf
        y_min = np.inf
        for alg_name in alg_names:
            alg_results = []
            for task_name in task_names:
                n_batch = int(task_name.split('_')[-1])
                last_error = last_errors.results_dict[task_name][alg_name]
                last_error_std = last_errors_std.results_dict[task_name][alg_name]
                alg_results.append([n_batch, last_error, last_error_std])
            alg_results = np.array(alg_results)
            plus = alg_results[:, 1] + alg_results[:, 2] / np.sqrt(4)
            minus = alg_results[:, 1] - alg_results[:, 2] / np.sqrt(4)
            if min(minus) < y_min:
                y_min = min(minus)
            if max(plus) > y_max:
                y_max = max(plus)
            ax.fill_between(alg_results[:, 0], np.log(minus), np.log(plus), alpha=0.2, color=c_dict[alg_name])
            ax.plot(alg_results[:, 0], np.log(alg_results[:, 1]), '--', marker=s_dict[alg_name],
                    color=c_dict[alg_name], label=get_latex_method(alg_name))
            ax.set_xlabel(r'Acquisition batch size $N_{\mathrm{batch}}$', **axis_font)
            ax.set_ylabel(f'{get_latex_metric(metric_name)}', **axis_font)
            ax.xaxis.set_minor_locator(AutoMinorLocator(n=2))

        logspace = np.round(np.logspace(np.log10(y_min), np.log10(y_max), 5, endpoint=True), 2)
        ylocs = [np.log(ival) for ival in logspace]
        ylabels = (f'{ival:8.2f}' for ival in logspace)
        ax.set_yticks(ylocs)
        ax.set_yticklabels(ylabels)

    ncol = len(alg_names) // 2
    if len(alg_names) % 2 != 0:
        ncol += 1
    fig.legend(*axs[0].get_legend_handles_labels(), loc='upper center', bbox_to_anchor=(0.5, 0.15), ncol=ncol)
    plt.tight_layout(rect=[0, 0.15, 1.0, 1.0])
    plot_name = Path(get_path()) / filename
    plt.savefig(plot_name)
    plt.close(fig)
