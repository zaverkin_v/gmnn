from ase import units
from ase.io import read
from ase.md import MDLogger
from ase.md.langevin import Langevin
from ase.io.trajectory import Trajectory
from gmnn.calculators import ASECalculator
from ase.md.velocitydistribution import MaxwellBoltzmannDistribution


if __name__ == '__main__':
    # read initial strcture
    atoms = read("ethanol.xyz")

    # setup calculator
    calc = ASECalculator(atoms=atoms, model_path="/SCR/HADES2_I1/zaverkin/tutorials/ethanol/models/example_1",
                         device_number=0, use_all_features=True, wrap_positions=False, compute_stress=False,
                         set_units="kcal/mol to eV", config="pes_training.txt")
    atoms.set_calculator(calc)
    
    # define temperature in K
    T = 200

    # define initial velocities
    MaxwellBoltzmannDistribution(atoms, temperature_K=T)
    
    # nvt dynamics with 0.5 fs time step
    dyn = Langevin(atoms, 0.5 * units.fs, T * units.kB, 0.02)
    dyn.attach(MDLogger(dyn, atoms, "ethanol.log", header=True, peratom=False, mode="w"), interval=1)
    traj = Trajectory("ethanol.traj", 'w', atoms)
    dyn.attach(traj.write, interval=1)
    # run dynamics for 50 ps 
    dyn.run(100000)

    # to get the ensemble statisics use
    #calc.get_energy_variance(atoms) # energy variance
    #calc.get_force_variance(atoms) # force variance
