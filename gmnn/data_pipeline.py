import time
import threading
from typing import *
from queue import Queue, Full

import numpy as np

from gmnn.neighborlist import *


class DataContainer:
    """ Stores the dictionary with raw atomic data and provides a function to load the data in a format suitable for
        GM-NN models. Provides statistics of atomic properties.

        :param dictionary: Dictionary containing the database, see below for an example.
        :param cutoff: Cutoff radius is used to define the local atomic neighborhood.
                       Please define the cutoff in accordance with the units used in the database.
        :param neighbor_list: Set to ``True`` if local neighborhood has to be computed.
        :param skin: If ``neighbor_list=True`` and an atom has no neighbors within ``cutoff``,
                     compute all neighbors within ``cutoff+skin``.
        :param n_species: The maximal atomic number of atomic species.
        :param idx_i: Only for internal use. Can be used to provide indices of central atoms,
                      otherwise they will be computed according to the selected ``neighbor_list`` and ``skin``.
        :param idx_j: Only for internal use. Can be used to provide indices of neighboring atoms,
                      otherwise they will be computed according to the selected ``neighbor_list`` and ``skin``.
        :param offsets: Only for internal use. Can be used to provide periodic boundary offsets of neighbors,
                        otherwise they will be computed according to the selected ``neighbor_list`` and ``skin``.

        .. note::

            The ``dictionary`` may contain, depending on the application, the following keys and respective values
            (Dipole moments and total charges cannot be used during training, yet. An implementation of electrostatic
            contributions will appear soon!)

            .. highlight:: python
            .. code-block:: python

                dictionary["N"]     # Number of atoms
                dictionary["Z"]     # Atomic numbers
                dictionary["D"]     # Dipole moments
                dictionary["Q"]     # Total charges
                dictionary["R"]     # Cartesian coordinates
                dictionary["E"]     # Total energies
                dictionary["F"]     # Atomic forces
                dictionary["C"]     # Cell vectors
                dictionary["W"]     # Virial stresses
                dictionary["MAT"]   # Magnetic anisotropy tensors

        Example:

        .. highlight:: python
        .. code-block:: python

            import numpy as np
            from gmnn.data_pipeline import DataContainer

            # dictionary can be be stored in an .npz file
            dictionary = np.load("ethanol_1000K.npz")

            dc = DataContainer(dictionary, cutoff=6.5, neighbor_list=True, skin=0.0)

            print(dc[100]['E']) # [-97138.27]

        .. note::
            ethanol_1000K.npz can be found in `Ethanol <https://github.com/zaverkin/ethanol_datasets_git>`_ but has to be
            changed to fit this version of the code

            .. highlight:: python
            .. code-block:: python

                import numpy as np

                dictionary = np.load("ethanol_1000K.npz")

                np.savez("ethanol_1000K.npz", E=dictionary['E'][:, 0], R=dictionary['R'], Z=dictionary['Z'],
                         N=dictionary['N'], F=dictionary['F'])

    """
    def __init__(self, dictionary: dict, cutoff: float, neighbor_list: bool = True, skin: float = 0.05,
                 n_species: int = 119, idx_i: Optional[Union[np.ndarray, List[np.ndarray]]] = None,
                 idx_j: Optional[Union[np.ndarray, List[np.ndarray]]] = None,
                 offsets: Optional[Union[np.ndarray, List[np.ndarray]]] = None):
        self.dictionary = dictionary
        self.cutoff = cutoff
        self.neighbor_list = neighbor_list
        self.skin = skin
        self.n_species = n_species

        self.N = self.dictionary.get('N', None)
        self.Z = self.dictionary.get('Z', None)
        self.D = self.dictionary.get('D', None)
        self.Q = self.dictionary.get('Q', None)
        self.R = self.dictionary.get('R', None)
        self.E = self.dictionary.get('E', None)
        self.F = self.dictionary.get('F', None)
        self.C = self.dictionary.get('C', None)
        self.W = self.dictionary.get('W', None)

        # magnetic anisotropy tensor: e.g. :math:`D`-tensor, but can also be :math:`g`-tensor
        self.MAT = self.dictionary.get('MAT', None)

        self.N_max = self.Z.shape[1]

        if idx_i is None or idx_j is None or offsets is None:
            self._compute_neighbors()
        else:
            self.idx_i = idx_i
            self.idx_j = idx_j
            self.offsets = offsets

        self._EperA_mean = None
        self._EperA_stdev = None
        self._MATperA_mean = None
        self._MATperA_stdev = None

    def __len__(self) -> int:
        """
        :return: The number of structures in the ``gmnn.data_container.DataContainer``.
        """
        return self.Z.shape[0]

    def __getitem__(self, idx: Union[int, List[int]]) -> dict:
        """
        :param idx: Structure index or list of structure indices.
        :return: Structure dictionary prepared for a GM-NN model.
        """

        if type(idx) is int or type(idx) is np.int64:
            idx = [idx]

        data = {"N": [],
                "E": [],
                "F": [],
                "Z": [],
                "D": [],
                "Q": [],
                "R": [],
                "C": [],
                "W": [],
                "MAT": [],
                "idx_i": [],
                "idx_j": [],
                "offsets": [],
                "batch_seg": []
                }

        N_tot = 0
        for k, i in enumerate(idx):
            N = self.N[i]
            if self.N is not None:
                data['N'].append(self.N[i])
            else:
                data['N'].append(np.nan)
            if self.E is not None:
                data['E'].append(self.E[i])
            else:
                data['E'].append(np.nan)
            if self.Q is not None:
                data['Q'].append(self.Q[i])
            else:
                data['Q'].append(np.nan)
            if self.Z is not None:
                data['Z'].append(self.Z[i, :N])
            else:
                data['Z'].append(0)
            if self.D is not None:
                data['D'].append(self.D[i:i + 1, :])
            else:
                data['D'].append([np.array([np.nan, np.nan, np.nan])])
            if self.R is not None:
                data['R'].append(self.R[i, :N, :])
            else:
                data['R'].append([np.array([np.nan, np.nan, np.nan])])
            if self.F is not None:
                data['F'].append(self.F[i, :N, :])
            else:
                data['F'].append([np.array([np.nan, np.nan, np.nan])])
            if self.C is not None:
                data['C'].append([self.C[i, :, :]])
            else:
                data['C'].append([np.array([[np.nan, np.nan, np.nan],
                                            [np.nan, np.nan, np.nan],
                                            [np.nan, np.nan, np.nan]])])
            if self.W is not None:
                data['W'].append([self.W[i, :, :]])
            else:
                data['W'].append([np.array([[np.nan, np.nan, np.nan],
                                            [np.nan, np.nan, np.nan],
                                            [np.nan, np.nan, np.nan]])])
            if self.MAT is not None:
                data['MAT'].append(self.MAT[i:i + 1, :])
            else:
                data['MAT'].append([np.array([np.nan, np.nan, np.nan, np.nan, np.nan, np.nan])])

            if not self.neighbor_list:
                data['idx_i'].append(np.reshape(self.idx_i[:N, :N - 1] + N_tot, [-1]).tolist())
                data['idx_j'].append(np.reshape(self.idx_j[:N, :N - 1] + N_tot, [-1]).tolist())
                data['offsets'].append([np.array([np.nan, np.nan, np.nan])])
            else:
                data['idx_i'].append(self.idx_i[i] + N_tot)
                data['idx_j'].append(self.idx_j[i] + N_tot)
                data['offsets'].append(self.offsets[i])
            data['batch_seg'].extend([k] * N)
            N_tot += N

        data['F'] = np.concatenate(data['F'], axis=0)
        data['D'] = np.concatenate(data['D'], axis=0)
        data['C'] = np.concatenate(data['C'], axis=0)
        data['W'] = np.concatenate(data['W'], axis=0)
        data['Z'] = np.concatenate(data['Z'], axis=0)
        data['R'] = np.concatenate(data['R'], axis=0)
        data['MAT'] = np.concatenate(data['MAT'], axis=0)
        data['idx_i'] = np.concatenate(data['idx_i'], axis=0)
        data['idx_j'] = np.concatenate(data['idx_j'], axis=0)
        data['offsets'] = np.concatenate(data['offsets'], axis=0)

        return data

    def split(self, sizes: Dict[str, int], seed: int = None) -> Dict[str, 'DataContainer']:
        """Splits this ``DataContainer`` into multiple subsets. Creates data splits for training, validation,
        and testing the model.

        :param sizes: Dictionary containing names and sizes of split components except for the test set.
        :param seed: Random seed used to generate the data splits.
        :return: Dictionary containing splits for the provided sizes and, additionally,
                 a test split with the remaining data if there is any.

        Example:

        .. highlight:: python
        .. code-block:: python

            import numpy as np
            from gmnn.data_pipeline import DataContainer

            # dictionary can be be stored in an .npz file
            dictionary = np.load('ethanol_1000K.npz')

            dc = DataContainer(dictionary, cutoff=6.5, neighbor_list=True, skin=0.0)
            dcs = dc.split({'train': 1000, 'valid': 200}, seed=1)
            # there will be three DataContainers in dcs: dcs['train'], dcs['valid'] and dcs['test']

            print(dcs['train'].EperA_stdev) # 0.5409116503689811

        .. note::

            ``n_test`` is calculated as ``n_test = n_data - (n_train + n_valid + n_pool)``.
        """
        random_state = np.random.RandomState(seed=seed)
        idx = random_state.permutation(np.arange(len(self)))
        sub_idxs = {}
        for key, val in sizes.items():
            sub_idxs.update({key: idx[0:val]})
            idx = idx[val:]
        if len(idx) > 0:
            sub_idxs.update({"test": idx})
        return {name: DataContainer({key: value[si] for key, value in self.dictionary.items()}, self.cutoff,
                                    self.neighbor_list, self.skin, self.n_species) for name, si in sub_idxs.items()}

    def split_by_indices(self, idxs: Union[List[int], int]) -> Dict[str, 'DataContainer']:
        """Creates two splits, 'selected' and 'remaining', where 'selected' is built using the provided idxs and
        'remaining' contains the remaining structures.

        :param idxs: Structure indices for the 'selected' split.
        :return: Dictionary containing splits with selected and remaining structures.
        """
        all_idxs = np.arange(len(self))
        sub_idxs = {
            'selected': idxs,
            'remaining': np.delete(all_idxs, idxs)
            }
        if self.neighbor_list:
            return {name: DataContainer({key: value[si] for key, value in self.dictionary.items()}, self.cutoff,
                                        self.neighbor_list, self.skin, self.n_species,
                                        idx_i=[self.idx_i[index] for index in si],
                                        idx_j=[self.idx_j[index] for index in si],
                                        offsets=[self.offsets[index] for index in si]) for name, si in sub_idxs.items()}
        else:
            return {name: DataContainer({key: value[si] for key, value in self.dictionary.items()}, self.cutoff,
                                        self.neighbor_list, self.skin, self.n_species) for name, si in sub_idxs.items()}

    def join_containers(self, dc: 'DataContainer') -> 'DataContainer':
        """Returns a ``DataContainer`` containing the structures of this ``DataContainer`` and ``dc``.

        :param dc: ``DataContainer`` which has to be joined with the current ``DataContainer``.
        :return: Joint ``DataContainer``.
        """
        if self.neighbor_list:
            return DataContainer({key: np.concatenate([self.dictionary[key], dc.dictionary[key]], axis=0)
                                  for key in self.dictionary}, self.cutoff, self.neighbor_list, self.skin,
                                 self.n_species, idx_i=self.idx_i + dc.idx_i, idx_j=self.idx_j + dc.idx_j,
                                 offsets=self.offsets + dc.offsets)
        else:
            return DataContainer({key: np.concatenate([self.dictionary[key], dc.dictionary[key]], axis=0)
                                  for key in self.dictionary}, self.cutoff, self.neighbor_list, self.skin,
                                 self.n_species)

    def _compute_neighbors(self):
        if not self.neighbor_list:
            # generate a neighbor list which includes all atoms
            self.idx_i, self.idx_j = simple_neighbor_list(n_at=self.N_max)
        elif self.neighbor_list:
            # generate neighbor list with nearest neighbors within cutoff
            nl = NeighborListTF(cutoff=self.cutoff + self.skin)
            self.idx_i = []
            self.idx_j = []
            self.offsets = []
            for i in range(self.Z.shape[0]):
                # atoms ASE objects:
                if self.C is None:
                    atoms = Atoms(self.Z[i, :self.N[i]], positions=self.R[i, :self.N[i]])
                else:
                    atoms = Atoms(self.Z[i, :self.N[i]], positions=self.R[i, :self.N[i]], cell=self.C[i],
                                  pbc=[True, True, True])
                idx_i, idx_j, offsets = nl.neighbor_list(atoms)
                self.idx_i.append(idx_i.numpy())
                self.idx_j.append(idx_j.numpy())
                self.offsets.append(offsets.numpy())

    def _compute_E_statistics(self):
        # compute mean
        E_sum = 0.0
        atoms_sum = 0
        for i in range(len(self.E)):
            N = self.N[i]
            E_sum += self.E[i]
            atoms_sum += len(self.Z[i, :N])
        EperA_mean = E_sum / atoms_sum

        # compute regression from (n_per_species_1, ..., n_per_species_119) to E - N_at * EperA_mean
        # The reason that we subtract EperA_mean is that we don't want to regularize the mean
        mean_err_sse = 0.0
        XTy = np.zeros(self.n_species)
        XTX = np.zeros(shape=(self.n_species, self.n_species), dtype=np.int64)
        for i in range(len(self.E)):
            N = self.N[i]
            Z_counts = np.zeros(self.n_species, dtype=np.int64)
            for z in self.Z[i, :N]:
                Z_counts[int(z)] += 1
            err = self.E[i] - len(self.Z[i, :N]) * EperA_mean
            XTy += err * Z_counts
            XTX += Z_counts[None, :] * Z_counts[:, None]
            mean_err_sse += err ** 2 / len(self.Z[i, :N])

        lam = 1.0  # regularization, should be a float such that the integer matrix XTX is converted to float
        EperA_regression = np.linalg.solve(XTX + lam * np.eye(self.n_species), XTy) + EperA_mean
        EperA_std = np.sqrt(mean_err_sse / atoms_sum)

        self._EperA_mean, self._EperA_stdev, self._EperA_regression = EperA_mean, EperA_std, EperA_regression

    def _compute_MAT_statistics(self):
        MATperA_sum = 0.0
        MATperA_std_sum = 0.0
        for i in range(len(self.MAT)):
            N = self.N[i]
            MATperA = np.mean(self.MAT[i, :-1]) / len(self.Z[i, :N])
            MATperA_std = np.std(self.MAT[i, :-1]) / len(self.Z[i, :N])
            MATperA_sum += MATperA
            MATperA_std_sum += MATperA_std
        MATperA_mean = MATperA_sum / len(self.MAT)
        MATperA_std = MATperA_std_sum / len(self.MAT)

        self._MATperA_mean = MATperA_mean
        self._MATperA_stdev = MATperA_std

    @property
    def EperA_mean(self) -> float:
        """
        :return: Atomic energy shift parameter computed as the mean of per atom energy.
        """
        if self._EperA_mean is None:
            self._compute_E_statistics()
        return self._EperA_mean

    @property
    def EperA_regression(self) -> np.ndarray:
        """
        :return: A species dependent array with atomic energy shift parameters computed by solving a linear
                 regression problem.
        """
        if self._EperA_mean is None:
            self._compute_E_statistics()
        return self._EperA_regression

    @property
    def EperA_stdev(self) -> float:
        """
        :return: Atomic energy scale parameter computed as root-mean-square error (RMSE) per atom of ``EperA_mean``.
        """
        if self._EperA_stdev is None:
            self._compute_E_statistics()
        return self._EperA_stdev

    @property
    def MATperA_mean(self) -> float:
        """
        :return: Magnetic anisotropy tensor shift parameter computed as the mean over all elements.
        """
        if self._MATperA_mean is None:
            self._compute_MAT_statistics()
        return self._MATperA_mean

    @property
    def MATperA_stdev(self) -> float:
        """
        :return: Magnetic anisotropy tensor scale parameter computed as the standard deviation over all elements.
        """
        if self._MATperA_stdev is None:
            self._compute_MAT_statistics()
        return self._MATperA_stdev


class DataLoaderInternal:
    # Class for internal use, see DataLoader below.
    # Reason why the implementation for DataLoader functions is in this class and not in DataLoader itself:
    # DataLoaderInternal and the thread in DataLoaderInternal contain a reference to each other
    # while the thread is still running.
    # Therefore, Python memory management cannot delete DataLoaderInternal before the thread terminates.
    # However, with this implementation,
    # Python memory management can delete DataLoader while the thread is still running,
    # and the __del__ implementation of DataLoader can then terminate the thread,
    # which will cause DataLoaderInternal to be able to get deleted as well.
    # With this, there is no need to manually call coord.request_stop() in order to initiate thread termination,
    # which can be easily forgotten.
    def __init__(self, dc: 'DataContainer', device: str, batch_size: int = 32,
                 seed: Optional[int] = None, shuffle: bool = False, capacity: int = 10, drop_last: bool = True,
                 coord: Optional[tf.train.Coordinator] = None, daemon: bool = False, dtype=tf.float32):
        self.dc = dc
        self.n_data = len(dc)
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.idx = np.arange(self.n_data)
        self.idx_in_epoch = 0
        self.random_state = np.random.RandomState(seed=seed)
        self.drop_last = drop_last
        self.n_batches = self.n_data // self.batch_size
        if not self.drop_last and self.n_data % self.batch_size != 0:
            self.n_batches += 1
        self.device = device
        self.dtype = dtype

        self.capacity = capacity

        if self.capacity > 0:
            self.coord = coord or tf.train.Coordinator()
            self.queue = Queue(maxsize=capacity)

            self.thread = threading.Thread(target=self._run)
            self.thread.daemon = daemon
            self.thread.start()

    def _move_to_device(self, data):
        return {'N': tf.convert_to_tensor(data['N'], self.dtype),
                'E': tf.convert_to_tensor(data['E'], self.dtype),
                'F': tf.convert_to_tensor(data['F'], self.dtype),
                'Z': tf.cast(tf.convert_to_tensor(data['Z'], self.dtype), tf.int32),
                'D': tf.convert_to_tensor(data['D'], self.dtype),
                'Q': tf.convert_to_tensor(data['Q'], self.dtype),
                'R': tf.convert_to_tensor(data['R'], self.dtype),
                'C': tf.convert_to_tensor(data['C'], self.dtype),
                'W': tf.convert_to_tensor(data['W'], self.dtype),
                'MAT': tf.convert_to_tensor(data['MAT'], self.dtype),
                'idx_i': tf.convert_to_tensor(data['idx_i'], tf.int32),
                'idx_j': tf.convert_to_tensor(data['idx_j'], tf.int32),
                'offsets': tf.convert_to_tensor(data['offsets'], self.dtype),
                'batch_seg': tf.convert_to_tensor(data['batch_seg'], tf.int32)
                }

    def _run(self):
        with tf.device(self.device):
            while not self.coord.should_stop():
                time.sleep(0)
                data = self._next_raw_batch()
                time.sleep(0)
                try:
                    time.sleep(0)
                    data = self._move_to_device(data)
                    time.sleep(0)
                    while True:
                        try:
                            self.queue.put(data, timeout=0.1)
                            break
                        except Full:
                            if self.coord.should_stop():
                                return
                except Exception as e:
                    print(e)
                    self.coord.request_stop(e)

    def _shuffle(self):
        self.idx = self.random_state.permutation(self.idx)

    def _next_raw_batch(self) -> dict:
        start = self.idx_in_epoch
        self.idx_in_epoch += self.batch_size
        if self.idx_in_epoch > self.n_data:
            if self.shuffle:
                self._shuffle()
            if self.drop_last or start > self.n_data - 1:
                start = 0
                self.idx_in_epoch = self.batch_size
            else:
                self.idx_in_epoch = self.n_data
        end = self.idx_in_epoch
        return self.dc[self.idx[start:end]]

    def next_batch(self):
        if self.capacity > 0:
            return self.queue.get()
        else:
            return self._move_to_device(self._next_raw_batch())

    def __len__(self):
        return self.n_batches

    def __iter__(self):
        for i in range(len(self)):
            yield self.next_batch()


class DataLoader:
    """Takes a ``DataContainer`` and provides mini-batches. Allows to create a thread which pre-loads the
    data mini-batches in parallel.

    :param dc: ``DataContainer`` containing the database.
    :param device: Set CPU or GPU device, e.g. '/CPU:0' or '/GPU:0'
    :param batch_size: Mini-batch size.
    :param seed: Used to create a certain index permutation in case of data shuffling.
    :param shuffle: Set ``True`` to shuffle data before each epoch.
    :param capacity: Maximum number of pre-loaded batches (high number uses more RAM, too low number might be a
                     bit slower). Set to 0 if you want to avoid pre-loading in a separate thread.
    :param drop_last: Set ``True`` to have all mini-batches of size ``batch_size``
                      by omitting the last mini-batch if it is smaller. Set ``False`` to also use
                      a potentially smaller last mini-batch to cover all data structures.
    :param coord: A train coordinator object.
    :param daemon: Whether to run a thread as daemon.
    :param dtype: Set floating point type (e.g. tf.float32).

    Example:

    .. highlight:: python
    .. code-block:: python

        import numpy as np
        import tensorflow as tf
        from gmnn.data_pipeline import DataContainer, DataLoader

        # dictionary can be be stored in an .npz file
        dictionary = np.load('ethanol_1000K.npz')

        dc = DataContainer(dictionary, cutoff=6.5, neighbor_list=True, skin=0.0)
        dcs = dc.split({'train': 1000, 'valid': 200}, seed=1)

        coord = tf.train.Coordinator()
        dl = DataLoader(dc=dcs["train"], batch_size=10, shuffle=True, seed=1, device='/CPU:0', capacity=10,
                        coord=coord, daemon=True)
        data = dl.next_batch()

        print(data['E'])
        #tf.Tensor(
        #[-97138.94  -97144.75  -97145.6   -97135.08  -97133.78  -97133.3
        # -97138.234 -97132.734 -97133.82  -97130.83 ], shape=(10,), dtype=float32)

    """
    def __init__(self, dc: 'DataContainer', device: str, batch_size: int = 32,
                 seed: Optional[int] = None, shuffle: bool = False, capacity: int = 10, drop_last: bool = True,
                 coord: Optional[tf.train.Coordinator] = None, daemon: bool = False, dtype=tf.float32):
        self.data_loader_internal = DataLoaderInternal(dc, device, batch_size, seed=seed, shuffle=shuffle, capacity=capacity,
                                                       drop_last=drop_last, coord=coord, daemon=daemon, dtype=dtype)

    def next_batch(self):
        """
        :return: The next mini-batch dictionary.
        """
        return self.data_loader_internal.next_batch()

    def __iter__(self):
        """
        Can be used to iterate over all batches of an epoch with a `for`-loop.

        :return: Iterator object to iterate through the mini-batches.

        Example:

        .. highlight:: python
        .. code-block:: python

            import numpy as np
            import tensorflow as tf
            from gmnn.data_pipeline import DataContainer, DataLoader

            # dictionary can be be stored in an .npz file
            dictionary = np.load('ethanol_1000K.npz')

            dc = DataContainer(dictionary, cutoff=6.5, neighbor_list=True, skin=0.0)
            dcs = dc.split({'train': 1000, 'valid': 200}, seed=1)

            coord = tf.train.Coordinator()
            dl = DataLoader(dc=dcs["train"], batch_size=10, shuffle=True, seed=1, device='/CPU:0', capacity=10,
                            coord=coord, daemon=True)

            for batch_idx, batch in enumerate(dl):
                print(f'Batch {batch_idx} contains data: {batch}')
        """
        for elem in self.data_loader_internal:
            yield elem

    def __len__(self):
        """
        :return: Number of mini-batches per epoch.
        """
        return len(self.data_loader_internal)

    def __del__(self):
        # causes the thread in self.data_loader_internal to terminate itself
        if self.data_loader_internal.capacity > 0:
            self.data_loader_internal.coord.request_stop()
