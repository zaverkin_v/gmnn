# The Gaussian Moment Neural Network Package

Welcome to the Gaussian Moment Neural Network (GM-NN) package developed for large-scale atomistic simulations employing 
atomistic neural networks based on Gaussian Moments [1, 2].

## Currently Provided Features:
* Classical PES learning for periodic and non-periodic atomistic structures [1, 2]. (Code is permanently archived [here](https://doi.org/10.18419/darus-2136))
* Atomistic simulations with ASE (Atomic Simulation Environment).
* Learning magnetic anisotropy tensors [3].
* Active learning interatomic potentials [4, 5].
* Transfer learning interatomic potentials [6]. Pre-trained and fine-tuned ANI models can be downloaded from [here](https://doi.org/10.18419/darus-3299) (Code is permanently archived [here](https://doi.org/10.18419/darus-2136))

We plan major updates in the future. Stay tuned!

## Requirements

* Python (>=3.6)
* NumPy (<=1.19.2 or a version compatible with the chosen TensorFlow 2.X version)
* SciPy (>=1.14)
* TensorFlow (>=2.2)
* ASE (>=3.16)

## Installation

First, clone this repository into a directory of your choice. To do so, just run in your terminal

```bash
$ git clone https://gitlab.com/zaverkin_v/gmnn.git <dest_dir>
```

and change your current directory to the new directory ``<dest_dir>`` via

```bash
$ cd <dest_dir>
```

Alternatively you may download the repository contents from [DaRUS](https://doi.org/10.18419/darus-2136) into a new 
directory ``<dest_dir>``. Note that [DaRUS](https://doi.org/10.18419/darus-2136) contains only PES training. For 
the current version use [GitLab](https://gitlab.com/zaverkin_v/gmnn).

Next, install all requirements by running, e.g.,

```bash
$ pip install -r requirements.txt
```

or by installing packages of your choice. Please make sure that the installed packages are compatible. 
This holds especially for the ```numpy``` and ```tensorflow``` packages.

Finally, set your ``PYTHONPATH`` environment variable to

```bash
$ export PYTHONPATH=path_to_gmnn:$PYTHONPATH
```

and you are ready to go!

## How to use

### PES models

To train a GM-NN model you have to specify model parameters in the ``pes_training.txt`` file including the path to 
the data set and model parameters such as the number of hidden layers and learning rates, see the 
[Documentation Site](https://zaverkin_v.gitlab.io/gmnn) for more details. 
To use the default ``pes_training.txt.default`` file, run

```bash
$ cp pes_training.txt.default pes_training.txt
```

Then, copy the default ``pes_train.py.default`` to ``pes_train.py`` via

```bash
$ cp pes_train.py.default pes_train.py
```

and simply run

```bash
$ python3 pes_train.py
```

Note that, for this example, you need the ``ethanol_1000K.npz`` data set to train your GM-NN model,
the download and preparation of which are described at the [Documentation Site](https://zaverkin_v.gitlab.io/gmnn). 
The description of all modules necessary to build custom models, train the default and custom models, and run atomistic 
simulations using ASE can also be found at the [Documentation Site](https://zaverkin_v.gitlab.io/gmnn).

### Learning magnetic anisotropy tensors

Similarly, a GM-NN model for magnetic anisotropy tensors can be trained. For this purpose, you can use equivalents of
``mat_train.py.default`` and ``mat_training.txt.default``. A detailed example for 
the [CoSar](https://zenodo.org/record/5172156) data presented at 
the [Documentation Site](https://zaverkin_v.gitlab.io/gmnn).

### Active learning for PES models

The current version includes active learning (AL) for PES models. An example of AL is provided by the 
default ``active_pes_training.txt.default`` and ``active_pes_train.py.default``. In this example, we employ the 
MaxDet selection method and Gaussian process posterior kernel for uncertainty estimation [5]. For more details on 
possible AL methods see Ref. [5] and [Documentation Site](https://zaverkin_v.gitlab.io/gmnn).

### Transfer learning PES models
The current version of the code implements discriminative fine-tuning for transfer learning interatomic potentials [6].
An example is provided by ``pre-training.txt.default`` and ``fine-tuning.txt.default`` config files as well as the Python 
script ``transfer-learning.py.default``. For more details on running transfer learning applications including the 
[pre-trained and fine-tuned general purpose ANI potentials](https://doi.org/10.18419/darus-3299)
see [Documentation Site](https://zaverkin_v.gitlab.io/gmnn).

## License

This code is licensed under the MIT license.

## Contributors

For questions or other inquiries, please contact one of the following contributors:
* [Viktor Zaverkin](https://www.itheoc.uni-stuttgart.de/institute/team/Zaverkin/) (zaverkin *at* theochem.uni-stuttgart.de, main developer)
* [David Holzmüller](https://www.isa.uni-stuttgart.de/institut/team/Holzmueller/) (david.holzmueller *at* mathematik.uni-stuttgart.de)
* [Johannes Kästner](https://www.itheoc.uni-stuttgart.de/institute/team/Kaestner-00003/) (kaestner *at* theochem.uni-stuttgart.de)

If you would like to contribute to the code, please contact Viktor Zaverkin.

## References
* [1] V. Zaverkin and J. Kästner, [“Gaussian Moments as Physically Inspired Molecular Descriptors for Accurate and Scalable Machine Learning Potentials,”](https://doi.org/10.1021/acs.jctc.0c00347) J. Chem. Theory Comput. **16**, 5410–5421 (2020).
* [2] V. Zaverkin, D. Holzmüller, I. Steinwart,  and J. Kästner, [“Fast and Sample-Efficient Interatomic Neural Network Potentials for Molecules and Materials Based on Gaussian Moments,”](https://pubs.acs.org/doi/10.1021/acs.jctc.1c00527) J. Chem. Theory Comput. **17**, 6658–6670 (2021).
* [3] V. Zaverkin, J. Netz, F. Zills, A. Köhn, and J. Kästner, [“Thermally Averaged Magnetic Anisotropy Tensors via Machine Learning Based on Gaussian Moments,”](https://pubs.acs.org/doi/abs/10.1021/acs.jctc.1c00853) J. Chem. Theory Comput. **18**, 1–12 (2022).
* [4] V. Zaverkin and J. Kästner, [“Exploration of transferable and uniformly accurate neural network interatomic potentials using optimal experimental design,”](https://iopscience.iop.org/article/10.1088/2632-2153/abe294/meta) Mach. Learn.: Sci. Technol. **2**, 035009 (2021).
* [5] V. Zaverkin, D. Holzmüller, I. Steinwart,  and J. Kästner, [“Exploring Chemical and Conformational Spaces by Batch Mode Deep Active Learning,”](https://pubs.rsc.org/en/content/articlelanding/2022/DD/D2DD00034B) Digital Discovery **1**, 605-620 (2022).
* [6] V. Zaverkin, D. Holzmüller, L. Bonfirraro,  and J. Kästner, “Transfer learning for chemically accurate interatomic neural network potentials,” **submitted** (2022).
