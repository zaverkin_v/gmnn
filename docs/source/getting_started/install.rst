============
Installation
============

Requirements
============

* Python_ (>=3.6)
* NumPy_ (<=1.19.2 or a version compatible with TensorFlow_ 2.X)
* SciPy_ (>=1.14)
* TensorFlow_ (>=2.2)
* ASE_ (>=3.16)

Building from Source
====================

First, build a path from the source through the git clone. To do so, just run in your terminal

.. highlight:: bash
.. code-block:: bash

    $ git clone https://gitlab.com/zaverkin_v/gmnn.git <dest_dir>

and move to the new directory ``<dest_dir>``

.. highlight:: bash
.. code-block:: bash

    $ cd <dest_dir>

Next, install all requirements by running, e.g.

.. highlight:: bash
.. code-block:: bash

    $ pip install -r requirements.txt

or by installing packages of your choice.

.. note::
    To install the desired version of TensorFlow_ follow instructions from
    `pip installation <https://www.tensorflow.org/install/pip>`_ or
    `conda installation <https://docs.anaconda.com/anaconda/user-guide/tasks/tensorflow/>`_. All other packages can also be installed
    either by using ``pip`` or ``conda``.

Finally, set your ``PYTHONPATH`` environment variable to

.. highlight:: bash
.. code-block:: bash

    $ export PYTHONPATH=path_to_gmnn:$PYTHONPATH

and you are ready to go!


.. _Python: https://www.python.org/
.. _NumPy: https://docs.scipy.org/
.. _SciPy: https://docs.scipy.org/
.. _TensorFlow: https://tensorflow.org/
.. _ASE: https://wiki.fysik.dtu.dk/ase/index.html