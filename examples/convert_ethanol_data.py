import numpy as np


if __name__ == '__main__':

    # run over both data set in the ethanol_datasets_git folder and store data with keys suitable for GM-NN models
    for temp in ['500K', '1000K']:
        old_dict = np.load(f'data/ethanol_datasets_git/ethanol_{temp}.npz')
        new_dict = {key: (np.squeeze(old_dict[key]) if key == 'E' else old_dict[key])
                    for key in ['E', 'F', 'R', 'Z', 'N']}
        np.savez(f'data/ethanol_datasets_git/ethanol_{temp}-converted.npz', **new_dict)

    # run over train and test splits in ethanol_ccsd_t folder and store data with keys suitable for GM-NN models
    for split in ['train', 'test']:
        old_dict = np.load(f'data/ethanol_ccsd_t/ethanol_ccsd_t-{split}.npz')
        new_dict = {
            'E': np.squeeze(old_dict['E']),
            'F': old_dict['F'],
            'R': old_dict['R'],
            'Z': [np.array(old_dict['z']).astype(int)] * len(old_dict['R']),
            'N': [int(len(old_dict['z']))] * len(old_dict['R'])
        }
        np.savez(f'data/ethanol_ccsd_t/ethanol_ccsd_t-{split}-converted.npz', **new_dict)