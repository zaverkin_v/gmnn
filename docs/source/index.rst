.. GM-NN documentation master file, created by
   sphinx-quickstart on Thu Jul 29 15:42:32 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=============================================
GM-NN: Gaussian Moment Neural Network Package
=============================================

GM-NN is an atomistic Neural Network (NN) built upon the Gaussian moments (GM) atomistic representation for the
prediction of potential energy surfaces (PES). This software package manages the construction and training of
atomistic NNs with GMs, a trainable, local, species-dependent atomic representation. Additionally, the GM-NN
provides an uncertainty estimation for atomistic NNs defined in the framework of optimal experimental design (OED)
or, equivalently, by treating the last layer of a trained network as a Bayesian linear regression model. Recently,
the GM-NN has been extended to predict magnetic anisotropy tensors (MAT).

Contents
========
.. toctree::
   :maxdepth: 1
   :caption: Getting Started

   getting_started/install
   getting_started/parameters

.. toctree::
   :maxdepth: 1
   :caption: Tutorials

   tutorials/ethanol
   tutorials/tio2
   tutorials/cosar
   tutorials/ethanol_active
   tutorials/transfer_learning

.. toctree::
   :maxdepth: 1
   :caption: Modules

   gmnn/data
   gmnn/neighborlist
   gmnn/layers
   gmnn/adam
   gmnn/pes_training
   gmnn/mat_training
   gmnn/active_learning
   gmnn/calculators

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
