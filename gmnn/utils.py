import numpy as np
import pickle
import tensorflow as tf
from typing import *


def get_distance_vectors(R: tf.Tensor, C: tf.Tensor, idx_i: tf.Tensor, idx_j: tf.Tensor, offsets: tf.Tensor,
                         batch_seg: tf.Tensor, pbc: bool = False, unittest: bool = False) \
        -> Union[Tuple[tf.Tensor, tf.Tensor], tf.Tensor]:
    """Returns distance vectors.

    :param R: Cartesian coordinates of shape (n_strct, n_maxat, 3).
    :param C: Cell vectors of shape (n_strct, 3, 3).
    :param idx_i: Central atom indices of shape (n_neighbors, ).
    :param idx_j: Neighbor atom indices of shape (n_neighbors, ).
    :param offsets: Periodic offsets of shape (n_neighbors, 3).
    :param batch_seg: Structure indices in the mini-batch (n_neighbors, ).
    :param pbc: Set ``True`` if periodic boundary conditions has to be implied.
    :param unittest: Set ``True`` during unittest.
    :return: Pair distance vectors.
    """
    # R_i, R_j have shape n_neighbors x 3
    R_i = tf.gather(R, idx_i)
    R_j = tf.gather(R, idx_j)
    if pbc:
        C = tf.gather(tf.gather(C, batch_seg), idx_i)
        replica_update = tf.squeeze(tf.matmul(offsets[:, None, :], C))
        R_j = tf.add(R_j, replica_update)
    R_ij = R_i - R_j
    if unittest:
        return R_ij, R_j
    else:
        return R_ij

def save_object(filename: str, obj: dict):
    file = open(filename, 'wb')
    pickle.dump(obj, file, protocol=3)
    file.close()


def load_object(filename: str) -> dict:
    file = open(filename, 'rb')
    result = pickle.load(file)
    file.close()
    return result


def padded_str(strs: List[str], lens: List[int]) -> str:
    # strs should be a list of strings and lens should be a list of integers of the same length.
    # This function concatenates the strings in strs but fills them up with whitespaces at the end such that they have
    # (at least) the corresponding lengths
    result = ''
    for s, l in zip(strs, lens):
        result = result + s + (' ' * max(0, l-len(s)))
    return result


def create_header(file: str):
    f = open(file, 'a+')
    f.write('******************************************************************** \n')
    f.write('**                                                                ** \n')
    f.write('**          ___          ___          ___          ___            ** \n')
    f.write('**         /\  \        /\__\        /\__\        /\__\           ** \n')
    f.write('**        /::\  \      /::|  |      /::|  |      /::|  |          ** \n')
    f.write('**       /:/\:\  \    /:|:|  |     /:|:|  |     /:|:|  |          ** \n')
    f.write('**      /:/  \:\  \  /:/|:|__|__  /:/|:|  |__  /:/|:|  |__        ** \n')
    f.write('**     /:/__/_\:\__\/:/ |::::\__\/:/ |:| /\__\/:/ |:| /\__\       ** \n')
    f.write('**     \:\  /\ \/__/\/__/~~/:/  /\/__|:|/:/  /\/__|:|/:/  /       ** \n')
    f.write('**      \:\ \:\__\        /:/  /     |:/:/  /     |:/:/  /        ** \n')
    f.write('**       \:\/:/  /       /:/  /      |::/  /      |::/  /         ** \n')
    f.write('**        \::/  /       /:/  /       /:/  /       /:/  /          ** \n')
    f.write('**         \/__/        \/__/        \/__/        \/__/           ** \n')
    f.write('**                                                                ** \n')
    f.write('******************************************************************** \n')
    f.write('**                                                                ** \n')
    f.write('**           A package for training and applying MLPs             ** \n')
    f.write('**                                                                ** \n')
    f.write('**            V. Zaverkin, D. Holzmüller, J. Kästner              ** \n')
    f.write('**                                                                ** \n')
    f.write('**  Please include this references in published work using GM-NN  ** \n')
    f.write('**  interatomic potentials:                                       ** \n')
    f.write('**                                                                ** \n')
    f.write('**        J. Chem. Theory Comput. 2020, 16, 8, 5410–5421          ** \n')
    f.write('**        J. Chem. Theory Comput. 2021, 17, 10, 6658–6670         ** \n')
    f.write('**                                                                ** \n')
    f.write('**  Please include this references in published work using GM-NN  ** \n')
    f.write('**  model for magnetic anisotropy tensors:                        ** \n')
    f.write('**                                                                ** \n')
    f.write('**        J. Chem. Theory Comput. 2022, 18, 1, 1–12               ** \n')
    f.write('**                                                                ** \n')
    f.write('**  Please include this references in published work using GM-NN  ** \n')
    f.write('**  for active learning interatomic potentials:                   ** \n')
    f.write('**                                                                ** \n')
    f.write('**        Mach. Learn.: Sci. Technol. 2021, 2, 035009             ** \n')
    f.write('**                          ...                                   ** \n')
    f.write('**                                                                ** \n')
    f.write('******************************************************************** \n')
    f.write(' \n')
    f.write(' \n')
    f.close()


def augment_rmse(metrics: dict):
    metrics['Energy-RMSE'] = np.sqrt(metrics['Energy-MSE'])
    if 'Force-MSE' in metrics:
        metrics['Force-RMSE'] = np.sqrt(metrics['Force-MSE'])
    if 'Stress-MSE' in metrics:
        metrics['Stress-RMSE'] = np.sqrt(metrics['Stress-MSE'])


def augment_mat_rmse(metrics: dict):
    metrics['D-RMSE'] = np.sqrt(metrics['D-MSE'])


def update_metrics(avg_metrics: dict, new_metrics: dict, batch_idx: int) -> dict:
    if batch_idx == 0:
        return {key: val.numpy() for key, val in new_metrics.items()}
    else:
        return {metric_name: avg_metrics[metric_name] \
                             + (new_metrics[metric_name].numpy() - avg_metrics[metric_name]) / (batch_idx + 1)
                for metric_name in avg_metrics}


def memory_growth():
    gpus = tf.config.experimental.list_physical_devices('GPU')
    if gpus:
        try:
            # memory growth needs to be the same across GPUs
            for gpu in gpus:
                # uses only as much memory as needed
                tf.config.experimental.set_memory_growth(gpu, True)
        except RuntimeError as e:
            # memory growth must be set before GPUs have been initialized
            print(e)
