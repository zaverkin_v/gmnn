from ase import units
from ase.io import read
from ase.md import MDLogger
from ase.md.verlet import VelocityVerlet
from ase.io.trajectory import Trajectory
from gmnn.calculators import ASECalculator
from ase.md.velocitydistribution import Stationary
from ase.md.velocitydistribution import MaxwellBoltzmannDistribution


if __name__ == '__main__':
    # read the inital structure
    atoms = read("rutile_supercell.xyz") 
    
    # setup calculator
    calc = ASECalculator(atoms=atoms, model_path="/SCR/HADES2_I1/zaverkin/tutorials/tio2/models/example_1",
                         device_number=0, use_all_features=True, wrap_positions=False, compute_stress=False,
                         set_units="kcal/mol to eV", config="pes_training.txt", skin=0.25)
    atoms.set_calculator(calc)

    # define temperature in K
    T = 1000
    # define initial velocities
    MaxwellBoltzmannDistribution(atoms, T * units.kB)
    Stationary(atoms, preserve_temperature=False)

    f = open("tio2.dat", 'a')
    f.write("#! Total energy in meV/atom \n")
    f.close()
    def print_energy(a=atoms):
        # function to print the potential, kinetic, and total energy
        epot = a.get_potential_energy() / len(a) * 10**3
        ekin = a.get_kinetic_energy() / len(a) * 10**3
        f = open("tio2.dat", 'a')
        f.write(f"{(epot + ekin).round(5)}\n")
        f.close()

    # nve dynamics
    dyn = VelocityVerlet(atoms, 1.0 * units.fs)
    dyn_traj = Trajectory("tio2.traj", 'w', atoms)
    dyn.attach(dyn_traj.write, interval=20)
    dyn.attach(MDLogger(dyn, atoms, "tio2.log", header=True, peratom=False, mode="w"), interval=20)
    dyn.attach(print_energy, interval=20)
    # run dynamics for 1.2 ns
    dyn.run(1200000)

