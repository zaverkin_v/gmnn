import numpy as np
import matplotlib.pylab as plt
from ase.io.trajectory import Trajectory

axis_font = { 'size':'12'}


def compute_vacf(v):
    # the convolution is actually being done here
    # meaning from -inf to inf so we only want half the
    # array
    VACF = []
    for i in range(v.shape[1]):
        for j in range(v.shape[2]):
            VACF.append(np.correlate(v[:, i, j], v[:, i,j], mode='full'))
    VACF = np.array(VACF)[:, np.shape(VACF)[-1]//2:]
    VACF = VACF.T 
    
    VACF = np.sum(VACF / VACF[0], 1) / v.shape[1] / v.shape[2]
    return VACF


def fft_acf(acf, dt):
    # FFT of autocorrelation function
    fft_acf = np.fft.rfft(acf)*dt
    return fft_acf


if __name__ == "__main__":
    
    # time step
    dt = 0.5 * 10 ** (-15)
    
    # read trajectory
    traj = Trajectory("ethanol.traj")[10000:]
    
    # read velocities
    v = np.zeros((len(traj),len(traj[0]), 3))
    for i in range(len(traj)):
        atoms = traj[i]
        v[i] = atoms.get_velocities()
    
    # compute velocity autocorrelation function
    VACF = compute_vacf(v)
    
    # compute and plot power spectrum
    vsize = VACF.size
    fft_vacf = fft_acf(VACF, dt)
    freq_v = np.fft.rfftfreq(vsize, d=dt) * 1.0 / ( 2.99792458 * 10**10 ) # cm^-1
        
    fig, ax = plt.subplots()
    ax.plot(freq_v, fft_vacf.real, label="200 K")
    ax.set_yticks([])
    ax.set_xlim(0, 4000)
    ax.set_xlabel(r'Frequency in cm$^{-1}$', **axis_font)
    ax.set_ylabel('Intensity in a.u.', **axis_font)
    ax.legend()
    plt.savefig('fft_spectrum.png', bbox_inches = "tight", dpi=1200)
