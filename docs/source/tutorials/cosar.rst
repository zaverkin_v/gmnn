.. _cosar-tutorial:

============
Learning MAT
============

Preparing Data Det
__________________

The following tutorial will use the `CoSar <https://zenodo.org/record/5172156>`_ data set [1], available free of charge.
No additional preparations are needed and the model can be trained right away.

Training MAT Model
__________________

Preparation
***********

To train the GM-NN model on data that contains :math:`\mathbf{D}` or zero-field splitting (ZFS) tensors, one has
to run the following `train.py` script

.. highlight:: python
.. code-block:: python

    from gmnn.trainer import MATTrainer

    if __name__ == '__main__':

        trainer = MATTrainer(config="mat_training.txt")
        trainer.fit()
        trainer.eval()

In this example, we set up our model and the training procedure by the `mat_training.txt` file

.. highlight:: none
.. code-block:: none

    --data_path=/SCR/HADES2_I1/zaverkin/tutorials/cosar/dataset/CoSar_MAT_17_06_2021.npz
    --model_path=/SCR/HADES2_I1/zaverkin/tutorials/cosar/models
    --model_name=example_1
    --cutoff=8.0
    --n_train=2900
    --n_valid=300
    --batch_size=32
    --valid_batch_size=100
    --data_seed=1
    --capacity=10
    --neighbor_list=1
    --skin=0.0
    --device_number=0
    --n_models=3
    --model_seed=1
    --architecture=512,512
    --weight_init_mode=normal
    --bias_init_mode=zeros
    --activation=swish
    --batch_norm=0
    --bn_momentum=0.9923279462629435
    --norm_scale_factor=0.4
    --init_normalization=ntk
    --weight_factor=1.0
    --bias_factor=0.1
    --pbc=0
    --n_radial=5
    --n_basis=7
    --emb_init=uniform
    --use_all_features=1
    --max_epoch=1000
    --save_epoch=100
    --valid_epoch=1
    --base_lr=0.02
    --scale_lr=0.025
    --shift_lr=0.025
    --emb_lr=0.02
    --forces=0
    --stress=0
    --force_weight=4.0
    --stress_weight=0.5
    --beta_2=0.999
    --use_mae_stopping=1

.. note::
    The presented tutorial trains an ensemble of 3 models similar to :ref:`ethanol-tutorial`. If a single model has to
    be trained, ``n_models=1`` must be selected. All hyper-parameters have been defined in :ref:`parameters`.

Training
********

By running

.. highlight:: bash
.. code-block:: bash

    $ python3 train.py

the training process starts and in ``model_path`` one can find

.. highlight:: bash
.. code-block:: bash

    $ cd <model_path>
    $ ls
    0/  1/  2/  best_stats_ens.pkl  mat_training.txt  train.out

Similar to :ref:`ethanol-tutorial` and :ref:`tio2-tutorial` model checkpoints are saved in `i/logs` folders, where `i`
is the number of the model in the ensemble, while the best models used subsequently are placed in `i/best`.
`train.out` file contains the whole history of the training process with mean absolute error (MAE) and
root-mean-square error (RMSE) of :math:`\mathbf{D}` tensor elements evaluated on training and validation data sets.
Each error metric shows three columns corresponding to the training error, running validation error, and the best validation
error. In this example, we obtain

.. highlight:: none
.. code-block:: none

    Best checkpoints for model 0 can be found in           ............. /SCR/HADES2_I1/zaverkin/tutorials/cosar/models/example_1/0/best
    Checkpoints for restart for model 0 can be found in    ............. /SCR/HADES2_I1/zaverkin/tutorials/cosar/models/example_1/0/logs

    Epoch            D-RMSE (train/valid/best_valid)  D-MAE (train/valid/best_valid)  Time
    ===========================================================================================
    Epoch 1/1000:     9.055/ 5.173/ 5.173              6.257/ 3.852/ 3.852            [ 7.95 s]
    Epoch 2/1000:     5.166/ 4.411/ 4.411              3.807/ 3.306/ 3.306            [ 3.53 s]
    Epoch 3/1000:     4.587/ 4.478/ 4.411              3.391/ 3.408/ 3.306            [ 3.56 s]
    Epoch 4/1000:     4.283/ 4.200/ 4.200              3.167/ 3.039/ 3.039            [ 3.52 s]
    Epoch 5/1000:     4.109/ 4.198/ 4.200              3.015/ 3.203/ 3.039            [ 3.47 s]
    Epoch 6/1000:     3.991/ 3.671/ 3.671              2.916/ 2.774/ 2.774            [ 3.56 s]
    Epoch 7/1000:     3.788/ 3.642/ 3.671              2.778/ 2.797/ 2.774            [ 3.47 s]
    Epoch 8/1000:     3.630/ 3.852/ 3.671              2.658/ 2.830/ 2.774            [ 3.49 s]
    Epoch 9/1000:     3.494/ 3.138/ 3.138              2.553/ 2.302/ 2.302            [ 3.53 s]
    Epoch 10/1000:    3.177/ 3.192/ 3.138              2.332/ 2.434/ 2.302            [ 3.54 s]

    ...

    Epoch 990/1000:   0.017/ 0.621/ 0.620              0.010/ 0.310/ 0.310            [ 3.62 s]
    Epoch 991/1000:   0.016/ 0.621/ 0.621              0.010/ 0.310/ 0.310            [ 3.57 s]
    Epoch 992/1000:   0.016/ 0.621/ 0.621              0.010/ 0.310/ 0.310            [ 3.53 s]
    Epoch 993/1000:   0.016/ 0.621/ 0.621              0.010/ 0.310/ 0.310            [ 3.54 s]
    Epoch 994/1000:   0.016/ 0.621/ 0.621              0.009/ 0.310/ 0.310            [ 3.55 s]
    Epoch 995/1000:   0.016/ 0.621/ 0.621              0.009/ 0.310/ 0.310            [ 3.55 s]
    Epoch 996/1000:   0.016/ 0.621/ 0.621              0.009/ 0.310/ 0.310            [ 3.55 s]
    Epoch 997/1000:   0.016/ 0.621/ 0.621              0.009/ 0.310/ 0.310            [ 3.58 s]
    Epoch 998/1000:   0.015/ 0.621/ 0.621              0.009/ 0.310/ 0.310            [ 3.56 s]
    Epoch 999/1000:   0.015/ 0.621/ 0.621              0.008/ 0.310/ 0.310            [ 3.51 s]
    Epoch 1000/1000:  0.015/ 0.621/ 0.621              0.008/ 0.310/ 0.310            [ 3.56 s]
    ===========================================================================================
    Timing report
    -------------
    Total time                     ............. 3551.7601482868195
    Best model report
    -----------------
    Best epochs from the training  ............. [997, 991, 991]
    ============================================================
    Results of 3 models obtained on the test data:

                        D-RMSE              D-MAE
    ============================================================
    Ensemble:            0.48505             0.24531
    Mean individual:     0.53995             0.28239
    Model 1:             0.54749             0.28425
    Model 2:             0.51785             0.27426
    Model 3:             0.55451             0.28866
    ============================================================

At the end of the `train.out` file, one can find MAE and RMSE evaluated on the test data that have not been seen during
the training procedure and are presented for single models and an ensemble if the latter was employed.

.. note::
    If no test data is available, make sure to exclude the last line in `train.py`, i.e.

    .. highlight:: python
    .. code-block:: python

        trainer.eval()

    Otherwise, an error will arise.

All models in this tutorial were trained on an NVIDIA GeForce RTX 2080Ti 11 GB GPU.

Predicting MAT Tensors
______________________

Here, we show a minimal example of the application of a GM-NN model to the prediction of :math:`\mathbf{D}` tensors
sampled during, e.g., molecular dynamics simulation, see :ref:`ethanol-tutorial` or :ref:`tio2-tutorial`.
The respective calculator is interfaced with Atomic Simulation Environment (`ASE <https://wiki.fysik.dtu.dk/ase/>`_)
package [2] for convenience. An example code is

.. literalinclude:: ../../../examples/cosar_mat.py
   :encoding: latin-1

.. note::

    It is possible to combine the prediction of :math:`\mathbf{D}` tensors with, e.g., molecular dynamics simulation
    employing the models trained on energies and forces; see :ref:`ethanol-tutorial` or :ref:`tio2-tutorial`. Thus, we
    can compute the thermal distribution of the respective property or even power spectra; see results in Ref. 1.

References
__________
* [1] V. Zaverkin, J. Netz, F. Zills, A. Köhn, and J. Kästner, “Thermally Averaged Magnetic Anisotropy Tensors via Machine Learning Based on Gaussian Moments,” J. Chem. Theory Comput. **18**, 1–12 (2022).
* [2] A. H. Larsen `et al.`, “The atomic simulation environment—a python library for working with atoms,” J. Phys. Condens. Matter **29**, 273002 (2017).