import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

# pip install uncertainties, if needed
try:
    import uncertainties.unumpy as unp
    import uncertainties as unc
except:
    try:
        from pip import main as pipmain
    except:
        from pip._internal import main as pipmain
    pipmain(['install','uncertainties'])
    import uncertainties.unumpy as unp
    import uncertainties as unc


def f(x, a, b):
    return a * x + b


def compute_running_average(O,M):
    O_run = np.empty_like(O)
    O_run[:] = 0.0
    for i in range(M//2, len(O)- M//2):
        O_run[i] = O[i-M//2:i+M//2].sum()/M
    return O_run


if __name__ == '__main__':

    data = np.loadtxt("tio2.dat")
    
    energy = (data[10000:] - np.mean(data[10000:]))
    time = np.arange(len(data[10000:])) * 20.0 * 1.0 / 10**6
    
    # linear regression
    popt, pcov = curve_fit(f, time, energy)
    # retrieve parameter values
    a = popt[0]
    b = popt[1]
    print('Optimal Values')
    print('a: {}'.format(a))
    print('b: {}'.format(b))
    
    # calculate parameter confidence interval
    a,b = unc.correlated_values(popt, pcov)
    print('Uncertainty')
    print('a: ' + str(a))
    print('b: ' + str(b))
            
    # calculate regression confidence interval
    px = np.linspace(0, 1.0, 100)
    py = a*px+b
    nom = unp.nominal_values(py)
    std = unp.std_devs(py)
    
    fig, ax = plt.subplots(figsize=(5,4))
    
    ax.tick_params(right=True)
    
    ylocs = [-0.050, -0.025, 0.0, 0.025, 0.050]
    ylabels = ("-0.050", "-0.025", "0.000", "0.025", "0.050")

    ax.set_yticks(ylocs)
    ax.set_yticklabels(ylabels)
    
    ax.set_xlim(-0.0, 1.0)
    ax.set_ylim(-0.05, 0.05)
    
    ax.plot(time, energy, label="M=1")
    ax.plot(time, compute_running_average(energy, 10), label="M=10")
    ax.plot(time, compute_running_average(energy, 100), color="tab:red", label="M=100")

    ax.set_xlabel("Time in ns", fontsize=12)
    ax.set_ylabel("Relative Energy in meV/atom", fontsize=12)
    ax.legend()
    
    plt.savefig("energy_conservation_tio2.png", bbox_inches = "tight", dpi=1200)
