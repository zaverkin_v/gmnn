import os
import sys
import argparse
import numpy as np

class Parameters:
    """Define hyperparameters of the model.

    :param config: File containing hyperparameters

    Note: Examples are shown in `pes_training.txt.default`, `mat_training.txt.default`,
    and `active_pes_training.txt.default`.
    """
    def __init__(self, config: str):
        self.config = config
        self.parser = argparse.ArgumentParser(description='HyperParameters', fromfile_prefix_chars='@')
        self._data_pipeline()
        self._neural_network()
        self._network_trainer()
        self._active_learning()
        self._transfer_learning()

    def _data_pipeline(self):
        self.parser.add_argument("--data_path", type=str, default=None, help="path to the data set")
        self.parser.add_argument("--train_data_path", type=str, default=None, help="path to the train data set")
        self.parser.add_argument("--valid_data_path", type=str, default=None, help="path to the valid data set")
        self.parser.add_argument("--model_path", type=str, default=None,
                                 help="path where trained model should be saved")
        self.parser.add_argument("--model_name", type=str, default=None, help="name of the model")
        self.parser.add_argument("--cutoff", type=float, default=4.0, help="cutoff radius (in Angstrom)")
        self.parser.add_argument("--r_min", type=float, default=0.5, help="minimal radius (in Angstrom)")
        self.parser.add_argument("--n_train", type=int, default=1000, help="number of structures for training")
        self.parser.add_argument("--n_valid", type=int, default=200, help="number of structures for early stopping")
        self.parser.add_argument("--batch_size", type=int, default=32, help="batch size for training")
        self.parser.add_argument("--valid_batch_size", type=int, default=100, help="batch size for validation/test")
        self.parser.add_argument("--data_seed", type=int, default=np.random.randint(0, 10000000),
                                 help="random seed for data splitting")
        self.parser.add_argument("--capacity", type=int, default=10, help="data queue capacity")
        self.parser.add_argument("--neighbor_list", type=int, default=1, help="set 1 to use neighbor list")
        self.parser.add_argument("--skin", type=float, default=0.0, help="skin for neighbor list")
        self.parser.add_argument("--device_number", type=int, default=0, help="GPU index (if GPU is available)")

    def _neural_network(self):
        self.parser.add_argument("--n_models", type=int, default=1, help="number of models in the ensemble")
        self.parser.add_argument("--model_seed", type=int, default=np.random.randint(0, 10000000),
                                 help="random seed for NN training / init")
        self.parser.add_argument("--architecture", type=str, default='512,512',
                                 help="comma-separated hidden layer sizes")
        self.parser.add_argument("--weight_init_mode", type=str, default="normal",
                                 help="weight initialisation distribution")
        self.parser.add_argument("--bias_init_mode", type=str, default="zeros", help="bias initialisation distribution")
        self.parser.add_argument("--activation", type=str, default='swish', help="activation function")
        self.parser.add_argument("--batch_norm", type=int, default=0, help="set 1 for batch normalisation")
        self.parser.add_argument("--bn_momentum", type=float, default=None, help="momentum for batch normalisation")
        self.parser.add_argument("--norm_scale_factor", type=float, default=0.4,
                                 help="rescale output of batch normalisation")
        self.parser.add_argument("--init_normalization", type=str, default="ntk", help="none, ntk, or he")
        self.parser.add_argument("--weight_factor", type=float, default=1.0, help="ntk weight factor")
        self.parser.add_argument("--bias_factor", type=float, default=0.1, help="ntk bias factor")
        self.parser.add_argument("--pbc", type=int, default=0, help="apply periodic boundaries")
        self.parser.add_argument("--n_radial", type=int, default=5, help="number of radial functions")
        self.parser.add_argument("--n_basis", type=int, default=7, help="number of basis functions")
        self.parser.add_argument("--emb_init", type=str, default='uniform', help="choose embeddings initialisation")
        self.parser.add_argument("--use_all_features", type=int, default=1, help="set 1 to use all features")

    def _network_trainer(self):
        self.parser.add_argument("--max_epoch", type=int, default=1000, help="maximal number of training epochs")
        self.parser.add_argument("--save_epoch", type=int, default=100, help="how often to save the model")
        self.parser.add_argument("--valid_epoch", type=int, default=1, help="how often to validate the model")
        self.parser.add_argument("--base_lr", type=float, default=3e-2, help="base learning rate")
        self.parser.add_argument("--scale_lr", type=float, default=1e-3, help="scale learning rate")
        self.parser.add_argument("--shift_lr", type=float, default=5e-2, help="shift learning rate")
        self.parser.add_argument("--emb_lr", type=float, default=2e-2, help="embeddings learning rate")
        self.parser.add_argument("--forces", type=int, default=0, help="set 1 to include forces")
        self.parser.add_argument("--stress", type=int, default=0, help="set 1 to include stresses")
        self.parser.add_argument("--force_weight", type=float, default=4.0, help="rescale force contribution")
        self.parser.add_argument("--stress_weight", type=float, default=0.5, help="rescale stress contribution")
        self.parser.add_argument("--beta_2", type=float, default=0.999, help="optimizer parameter")
        self.parser.add_argument("--use_mae_stopping", type=int, default=1,
                                 help="use RMSE=0 / MAE=1 for early stopping")
        self.parser.add_argument("--loss", type=str, default='molecules',
                                 help="use molecules to train on total energies or structures (vibrations) to rescale "
                                      "the contribution of energies, forces, and (if provided) stresses")

    def _active_learning(self):
        self.parser.add_argument("--max_al_step", type=int, default=10000,
                                 help="maximal number of active learning steps")
        self.parser.add_argument("--n_pool", type=int, default=1000, help="size of the data pool")
        self.parser.add_argument("--max_n_train", type=int, default=6000, help="maximal size of the training set")
        self.parser.add_argument("--kernel", type=str, default='F_inv', help="g, F_inv, qbc-energy, qbc-force,"
                                                                             " ae-energy, ae-force, random")
        self.parser.add_argument("--n_random_features", type=int, default=0,
                                 help="number of random projections of all linear-layer gradients, set to 0 for "
                                      "using last-layer gradients")
        self.parser.add_argument("--selection", type=str, default='max_det_greedy', help="max_diag, max_det_greedy, "
                                                                                         "max_dist_greedy, lcmd_greedy")
        self.parser.add_argument("--al_batch_size", type=int, default=100, help="batch size used for active learning")
        self.parser.add_argument("--max_al_batch_size", type=int, default=100, help="initial maximal size of "
                                                                                    "the selected batch")
        self.parser.add_argument("--al_batch_scaling", type=float, default=1,
                                 help="the maximal size of the batch selected by active learning is increased "
                                      "by this factor in every step")
        self.parser.add_argument("--al_on_cpu", type=int, default=0, help="if set to 1, active learning is "
                                                                          "performed on CPU even if training "
                                                                          "is performed on GPU. "
                                                                          "Otherwise set to 0 (default).")
        self.parser.add_argument("--EperA_errors", type=int, default=0, help="if set to 1, energy errors "
                                                                             "are given per atom. "
                                                                             "Otherwise set to 0 (default).")

    def _transfer_learning(self):
        self.parser.add_argument("--init_path", type=str, default=None,
                                 help="initialize model parameters from pre-trained model")
        self.parser.add_argument("--fine_tune_lrs", type=str, default='1e-2,2e-2,3e-2',
                                 help="comma-separated learning rates for corresponding layers (hidden + output)")

    def get_hyperparameters(self) -> argparse.Namespace:
        """
        :return: Defined hyperparameters.
        """
        if len(sys.argv) == 1:
            if os.path.isfile(self.config):
                args = self.parser.parse_args(['@' + self.config])
            else:
                args = self.parser.parse_args(["--help"])
        else:
            args = self.parser.parse_args()

        return args
