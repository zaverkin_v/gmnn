import tensorflow as tf
from typing import *


class KernelMatrix:
    """
    Abstract base class for representing a (quadratic, symmetric, positive semi-definite) kernel matrix.

    :param num_columns: Number of columns (and therefore also rows) of the kernel matrix.
    """
    def __init__(self, num_columns: int):
        self.num_columns = num_columns

    def get_diag(self) -> tf.Tensor:
        """
        :return: Diagonal of the kernel matrix.
        """
        raise RuntimeError('Not implemented')

    def get_column(self, i: int) -> tf.Tensor:
        """
        :param i: Column index.
        :return: ``i``-th column of the kernel matrix.
        """
        raise RuntimeError('Not implemented')

    def get_num_columns(self) -> int:
        """
        :return: Number of columns (which is also the number of rows).
        """
        return self.num_columns

    def get_sq_dists(self, i: int) -> tf.Tensor:
        """
        :param i: Column index.
        :return: Squared distances in the RKHS of all elements to element i.
                 These are computed as :math:`K_{ii} + K_{jj} - 2K_{ij}` for all :math:`j`.
        """
        diag = self.get_diag()
        return diag[i] + diag - 2 * self.get_column(i)


class DiagonalKernelMatrix(KernelMatrix):
    """
    Represents a diagonal kernel matrix, where get_column() and get_sq_dists() is not implemented.

    :param g: Diagonal of the kernel matrix.
    """
    def __init__(self, g: tf.Tensor):
        super().__init__(g.shape[0])
        self.diag = g

    def get_diag(self) -> tf.Tensor:
        return self.diag


class FeatureKernelMatrix(KernelMatrix):
    """
    Represents a kernel matrix given by :math:`K_{ij} = \\sum_{m, k} g_{mik} g_{mjk}`.

    :param g: Feature tensor with ``shape=(n_models, n_structures, n_features)``.
    """
    def __init__(self, g: tf.Tensor):
        super().__init__(g.shape[1])
        self.g = g
        self.diag = tf.einsum('mbi, mbi -> mb', g, g)

    def get_diag(self) -> tf.Tensor:
        return tf.reduce_mean(self.diag, axis=0)

    def get_column(self, i: int) -> tf.Tensor:
        return tf.reduce_mean(tf.einsum('mbi, mi -> mb', self.g, self.g[:, i, :]), axis=0)


class FeatureCovKernelMatrix(KernelMatrix):
    """Represents a kernel matrix given by :math:`K_{ij} = \\sum_{m, k, l} g_{mik} C_{mkl} g_{mjl}`,
    where :math:`C` is the covariance matrix.

    :param g: Feature tensor with ``shape=(n_models, n_structures, n_features)``.
    :param cov_mat: Parameter covariance matrix with ``shape=(n_models, n_features, n_features)``.
    """
    def __init__(self, g: tf.Tensor, cov_mat: tf.Tensor):
        super().__init__(g.shape[1])
        self.g = g
        self.cov_mat = cov_mat
        self.cov_g = tf.einsum('mij, mbi -> mbj', self.cov_mat, g)
        self.diag = tf.einsum('mbi, mbi -> mb', self.cov_g, g)

    def get_diag(self) -> tf.Tensor:
        return tf.reduce_mean(self.diag, axis=0)

    def get_column(self, i: int) -> tf.Tensor:
        return tf.reduce_mean(tf.einsum('mbi, mi -> mb', self.g, self.cov_g[:, i, :]), axis=0)
