========================================
Active Learning
========================================

Trainer
_______

.. autoclass:: gmnn.trainer.ActivePESTrainer
    :members:

Active Learning
_______________

.. autoclass:: gmnn.active_learning.GenericALMethod
    :members:

Feature Statistics
__________________

.. autoclass:: gmnn.active_learning.FeatureStatistics
    :members:

Random Projections
__________________

.. autoclass:: gmnn.active_learning.RandomProjections
    :members:

Kernels
_______

.. autoclass:: gmnn.al_kernels.KernelMatrix
    :members:

.. autoclass:: gmnn.al_kernels.DiagonalKernelMatrix
    :members:

.. autoclass:: gmnn.al_kernels.FeatureKernelMatrix
    :members:

.. autoclass:: gmnn.al_kernels.FeatureCovKernelMatrix
    :members:

Selection Methods
_________________

Naive Active Learning
*********************

.. autofunction:: gmnn.al_selection.max_diag

Greedy Determinant Maximization (MaxDet)
****************************************

.. autofunction:: gmnn.al_selection.max_det_greedy

Greedy Distance Maximization (MaxDist)
**************************************

.. autofunction:: gmnn.al_selection.max_dist_greedy


Largest Cluster Maximum Distance (LCMD)
***************************************

.. autofunction:: gmnn.al_selection.lcmd_greedy
