import tensorflow as tf
import numpy as np
from typing import *


class Sequential(tf.keras.Model):
    """Groups a sequence of layers into a model.

    :param layers: List of layers.
    """
    def __init__(self, layers: List[tf.keras.layers.Layer]):
        super().__init__()
        self.layers_ = layers

    def call(self, x: Union[tf.Tensor, Tuple[tf.Tensor]], **kwargs) -> tf.Tensor:
        """Applies all layers sequentially to input tensor.

        :param x: Tensor or tuple of tensors.
        :return: Output of last layer.
        """
        for l in self.layers_:
            x = l(x)
        return x


class GaussianMomentsLayer(tf.keras.layers.Layer):
    """Construct Gaussian moment representation of local atomic environments.
    Gaussian moments were defined in `original GM-Paper <https://pubs.acs.org/doi/10.1021/acs.jctc.0c00347>`_
    and improved in our recent Paper.

    :param n_radial: Number of radial functions.
    :param n_contr: Number of tensor contractions.
    :param n_basis: Number of basis functions.
    :param r_max: Cutoff radius. Please define the cutoff in accordance with employed units.
    :param n_species: Maximal number of species in the data set.
    :param r_min: Minimal radius for atomic representation.
    :param seed: Seed for random initialization of the embeddings.
    :param dtype: Set TensorFlow type (e.g. tf.float32)
    :param emb_init: Method for embedding initialization. Defaults to ``emb_init = 'uniform'``.
                     ``emb_init = 'constant'`` is recommended for models trained without force information.
    :param use_all_features: Set ``True`` to select all symmetry non-equivalent features.
                             Set ``False`` to select only the upper triangular elements of tensor contractions.

    Example:

    .. highlight:: python
    .. code-block:: python

        import tensorflow as tf
        from gmnn.data_pipeline import *
        from gmnn.layers import *
        from gmnn.utils import *

        # define data pipeline
        dictionary = np.load('ethanol_1000K.npz')
        dc = DataContainer(dictionary, cutoff=6.5, neighbor_list=True, skin=0.0, n_species=10)
        dcs = dc.split({'train': 1000, 'valid': 200}, seed=1)

        gms = GaussianMomentsLayer(n_radial=5, n_contr=8, n_basis=7, r_max=6.5, n_species=10,
                                   r_min=0.5, dtype=tf.float32, seed=1)
        step = 0
        with tf.device('/CPU:0'):
            coord = tf.train.Coordinator()
            dl = DataLoader(dc=dcs['train'], batch_size=32, shuffle=True, seed=1, device='/CPU:0', capacity=10,
                            coord=coord, daemon=True)
            while not coord.should_stop():
                if step >= 1:
                    coord.request_stop()
                    break
                step += 1
                data = dl.next_batch()
                R, Z, idx_i, idx_j, batch_seg, C, offsets = data["R"], data['Z'], data['idx_i'], data['idx_j'], \
                                                            data['batch_seg'], data['C'], data['offsets']
                R_ij = get_distance_vectors(R, C, idx_i, idx_j, offsets, batch_seg, pbc=False)
                out = gms((R_ij, Z, idx_i, idx_j), unittest=False)

        print(out)
        #tf.Tensor(
        #[[-0.06271161 -0.10780737  0.33595577 ...  0.18490869 -0.8077766
        #  -0.92867994]
        # [-0.05862102 -1.3060669  -0.25606686 ... -0.11938953 -0.01543601
        #   0.56733996]
        # [-0.1124958   0.08881491  0.07140675 ...  0.0827319  -0.8255707
        #  -0.78983796]
        # ...
        # [-0.20675135  0.23728132 -0.02431558 ... -0.00194792 -0.96411914
        #  -0.8291728 ]
        # [ 0.21196912 -0.36008108  1.2524538  ...  0.7034467   0.43757442
        #   0.73513   ]
        # [-0.01549813  0.2235376  -0.36855543 ... -0.06408728 -0.24852037
        #  -0.12861224]], shape=(288, 360), dtype=float32)
    """
    def __init__(self, n_radial: int, n_contr: int, n_basis: int, r_max: float, n_species: int = 119,
                 r_min: float = 0.5, seed: Optional[int] = None, dtype=tf.float32, emb_init: str = 'uniform',
                 use_all_features: bool = True):
        super().__init__()
        self.n_radial = n_radial
        self.n_contr = n_contr
        self.n_basis = n_basis
        self.n_species = n_species
        self.r_max = r_max
        self.r_min = r_min
        self._dtype = dtype
        self.betta = self.n_basis ** 2 / self.r_max ** 2
        self.norm = (2.0 * self.betta / np.pi) ** 0.25
        self.use_all_features = use_all_features

        with tf.name_scope('triang_idxs_2d'):
            self.triang_idxs_2d = []
            for i in range(self.n_radial):
                self.triang_idxs_2d.append([i, i])
                for j in range(i + 1, self.n_radial):
                    self.triang_idxs_2d.append([i, j])
            self.triang_idxs_2d = tf.convert_to_tensor(self.triang_idxs_2d)

        with tf.name_scope('triang_idxs_3d'):
            self.triang_idxs_3d = []
            for i in range(self.n_radial):
                self.triang_idxs_3d.append([i, i, i])
                for j in range(self.n_radial):
                    if j != i:
                        self.triang_idxs_3d.append([i, j, j])
                for j in range(i + 1, self.n_radial):
                    for k in range(j + 1, self.n_radial):
                        self.triang_idxs_3d.append([i, j, k])
            self.triang_idxs_3d = tf.convert_to_tensor(self.triang_idxs_3d)

        # shape of shift_parameter: n_shifts
        with tf.name_scope("radial_shift"):
            self.shift_parameter = tf.convert_to_tensor([self.r_min+i*(self.r_max-self.r_min)/self.n_basis for i in range(self.n_basis)])

        with tf.name_scope("atomic_type_embedding"):
            if emb_init == "uniform":
                self.embeddings = tf.Variable(tf.random.uniform([self.n_species, self.n_species,
                                                                 self.n_radial, self.n_basis], minval=-1.0,
                                                                maxval=1.0, seed=seed, dtype=self._dtype))
            elif emb_init == "constant":
                comb_matrix = tf.convert_to_tensor([[max(0.0, 1.0 - np.abs(i - j * (n_radial - 1) / (n_basis - 1)))
                                                     for j in range(n_basis)] for i in range(n_radial)],
                                                   dtype=self._dtype)
                emb = tf.tile(comb_matrix[None, None, :, :],
                              [self.n_species, self.n_species, 1, 1])
                self.embeddings = tf.Variable(np.sqrt(n_radial) * emb)

    def call(self, x: Tuple[tf.Tensor], unittest: bool = False) -> Union[Tuple[tf.Tensor], tf.Tensor]:
        """Returns a tensor of Gaussian moments representing the local environment of each atom in the
        structure / batch of structures.

        :param x: Tuple of input tensors. Note that the order is important and has to be
                  ``x = (R_ij, Z, idx_i, idx_j)``.
        :return: A tensor containing Gaussian moments.
        """
        R_ij, Z, idx_i, idx_j = x

        # R has shape n_atoms x 3
        # Z has shape n_atoms
        # idx_i has shape n_neighbors
        # idx_j has shape n_neighbors
        # batch_seg has shape n_neighbors

        # calculate species dependent coefficients
        # Z_i, Z_j have shape n_neighbors x 1
        # Z_ij has shape n_neighbors x 2
        Z_i = tf.expand_dims(tf.gather(Z, idx_i), -1)
        Z_j = tf.expand_dims(tf.gather(Z, idx_j), -1)
        Z_ij = tf.concat([Z_i, Z_j], -1)

        # gather embeddings
        # shape: n_neighbors x n_radial x n_basis
        species_pair_coefficients = tf.gather_nd(self.embeddings, Z_ij)
        species_pair_coefficients = (1.0 / np.sqrt(self.n_basis)) * species_pair_coefficients
        
        # calculate radial function
        # R_ij has shape n_neighbors x 3
        R_ij_len = tf.sqrt(tf.reduce_sum(R_ij ** 2, -1) + 1.0e-12)
        R_ij_vec = R_ij / tf.expand_dims(R_ij_len, -1)

        # cos_cutoff has shape n_neighbors
        cos_cutoff = 0.5 * (tf.cos(np.pi * tf.clip_by_value(R_ij_len, clip_value_min=0.,
                                                            clip_value_max=self.r_max) / self.r_max) + 1.0)

        # shape: n_neighbors x n_basis
        basis_values = self.norm * tf.exp(-self.betta * (self.shift_parameter[None, :] - R_ij_len[:, None]) ** 2)

        # shape: n_neighbors x n_models x n_radial  ('nrb,nb->nr')
        radial_function = tf.linalg.matvec(species_pair_coefficients, basis_values)
        # shape: n_neighbors x n_radial
        radial_function = cos_cutoff[:, None] * radial_function

        # calculate Gaussian moments
        # shape: n_neighbors x n_radial x (3)^(moment_number)
        zero_moment = radial_function
        first_moment = zero_moment[:, :, None] * R_ij_vec[:, None, :]
        second_moment = first_moment[:, :, :, None] * R_ij_vec[:, None, None, :]
        third_moment = second_moment[:, :, :, :, None] * R_ij_vec[:, None, None, None, :]

        # shape: n_atoms x n_radial x (3)^(moment_number)
        zero_moment = tf.math.segment_sum(zero_moment, idx_i)
        first_moment = tf.math.segment_sum(first_moment, idx_i)
        second_moment = tf.math.segment_sum(second_moment, idx_i)
        third_moment = tf.math.segment_sum(third_moment, idx_i)

        # calculate contractions
        # convention: a corresponds to n_atoms, m to n_models, r/s/t to n_radial, i/j/k/l to 3
        if self.use_all_features:
            # convention: a corresponds to n_atoms, m to n_models, r/s/t to n_radial, i/j/k/l to 3
            contr_0 = zero_moment
            contr_1 = tf.einsum('ari, asi -> rsa', first_moment, first_moment)
            contr_2 = tf.einsum('arij, asij -> rsa', second_moment, second_moment)
            contr_3 = tf.einsum('arijk, asijk -> rsa', third_moment, third_moment)
            contr_4 = tf.einsum('arij, asik, atjk -> rsta', second_moment, second_moment, second_moment)
            contr_5 = tf.einsum('ari, asj, atij -> rsta', first_moment, first_moment, second_moment)
            contr_6 = tf.einsum('arijk, asijl, atkl -> rsta', third_moment, third_moment, second_moment)
            # directly contract rst to the end since no gather_nd is necessary because the tensor has no symmetries
            #contr_7 = tf.einsum('arijk, asij, atk -> arst', third_moment, second_moment, first_moment)
            contr_7 = tf.einsum('arijk, asij, atk -> rsta', third_moment, second_moment, first_moment)

            n_symm01_features = self.triang_idxs_2d.shape[0] * self.n_radial

            gaussian_moments = [contr_0,
                                tf.transpose(tf.gather_nd(contr_1,
                                                          self.triang_idxs_2d)),
                                tf.transpose(
                                    tf.gather_nd(contr_2,
                                                 self.triang_idxs_2d)),
                                tf.transpose(
                                    tf.gather_nd(contr_3,
                                                 self.triang_idxs_2d)),
                                tf.transpose(tf.gather_nd(contr_4,
                                                          self.triang_idxs_3d)),
                                tf.transpose(tf.reshape(tf.gather_nd(contr_5, self.triang_idxs_2d),
                                                        [n_symm01_features, -1])),
                                tf.transpose(tf.reshape(tf.gather_nd(contr_6, self.triang_idxs_2d),
                                                        [n_symm01_features, -1])),
                                #tf.reshape(contr_7, [-1, self.n_radial ** 3])
                                tf.transpose(tf.reshape(contr_7, [self.n_radial**3, -1]))
                                ]
        else:
            gaussian_moments = [zero_moment,
                                tf.transpose(tf.gather_nd(tf.einsum('ari, asi -> rsa', first_moment, first_moment),
                                                          self.triang_idxs_2d)),
                                tf.transpose(tf.gather_nd(tf.einsum('arij, asij -> rsa', second_moment, second_moment),
                                                          self.triang_idxs_2d)),
                                tf.transpose(tf.gather_nd(tf.einsum('arijk, asijk -> rsa', third_moment, third_moment),
                                                          self.triang_idxs_2d)),
                                tf.transpose(tf.gather_nd(tf.einsum('arij, asi, atj -> rsta',
                                                                    second_moment, first_moment, first_moment),
                                                          self.triang_idxs_3d)),
                                tf.transpose(tf.gather_nd(tf.einsum('arij, asik, atjk -> rsta',
                                                                    second_moment, second_moment, second_moment),
                                                          self.triang_idxs_3d)),
                                tf.transpose(tf.gather_nd(tf.einsum('arijk, asij, atk -> rsta',
                                                                    third_moment, second_moment, first_moment),
                                                          self.triang_idxs_3d)),
                                tf.transpose(tf.gather_nd(tf.einsum('arijk, asijl, atkl -> rsta',
                                                                    third_moment, third_moment, second_moment),
                                                          self.triang_idxs_3d))
                                ]

        gaussian_moments = tf.concat(gaussian_moments[:self.n_contr], axis=-1)
        
        if unittest:
            return R_ij_len, R_ij_vec, gaussian_moments
        else:
            return gaussian_moments


class LinearLayer(tf.keras.layers.Layer):
    """Simple linear layer.

    :param in_features: Number of input features.
    :param out_features: Number of output features.
    :param init_normalization: Set ``init_normalization='he'`` to use He initialization or
                               ``init_normalization='ntk'`` to use NTK parametrization.
    :param weight_init_mode: Set ``weight_init_mode='normal'`` to draw weights from normal distribution.
                             Other options: ``'zeros', 'trunc_normal', 'uniform'`` distributions.
    :param bias_init_mode: Set ``bias_init_mode='zeros'`` to set initial biases to zeros.
                           Other options: ``'normal', 'trunc_normal', 'uniform'`` distributions.
    :param weight_init_gain: Rescaling of weight initialization.
    :param bias_init_gain: Rescaling of bias initialization.
    :param bias_factor: NTK parameter rescaling the biases.
    :param weight_factor: NTK parameter rescaling the weights.
    :param dtype: Set TensorFlow type (e.g. tf.float32).
    :param seed: Set to generate reproducible network initialization, otherwise a random seed is generated.
    """
    def __init__(self, in_features: int, out_features: int, init_normalization: str = 'none',
                 weight_init_mode: str = 'normal', bias_init_mode: str = 'normal', weight_init_gain: float = 1.0,
                 bias_init_gain: float = 1.0, bias_factor: float = 1.0, weight_factor: float = 1.0,
                 dtype=tf.float32, seed: Optional[int] = None):
        # init_normalization should be 'none', 'he' or 'ntk'
        super().__init__()
        self.in_features = in_features
        self.out_features = out_features
        self._dtype = dtype
        self.bias_factor = bias_factor
        self.seed = seed
        self.bias_init_mode = bias_init_mode

        effective_weight_init_gain = weight_init_gain
        if init_normalization == 'he':
            effective_weight_init_gain *= np.sqrt(1.0/in_features)
        self._weights = self.get_random_variable(shape=[self.in_features, self.out_features],
                                                 init_mode=weight_init_mode, init_gain=effective_weight_init_gain)
        self._biases = self.get_random_variable(shape=[self.out_features],
                                                init_mode=bias_init_mode, init_gain=bias_init_gain)

        self.last_input = None
        self.last_output = None

        self.weight_factor = weight_factor * (np.sqrt(1.0/in_features) if init_normalization == 'ntk' else 1.0)

    def call(self, x: tf.Tensor, training: bool) -> tf.Tensor:
        """Returns output of the linear layer.

        :param x: Input batch.
        :return: A tensor containing output of the linear layer.
        """
        self.last_input = x
        self.last_output = self.weight_factor * tf.matmul(x, self._weights) + self.bias_factor * self._biases
        return self.last_output

    def get_random_variable(self, shape: Union[int, List[int]], init_mode: str, init_gain: float) -> tf.Variable:
        """Returns a random variable from the specified distribution.

        :param shape: Shape of the tensor.
        :param init_mode: Distribution.
        :param init_gain: Factor by which the distribution is scaled.
        """
        if init_mode == 'zeros':
            return tf.Variable(init_gain * tf.zeros(shape, dtype=self._dtype), dtype=self._dtype)
        elif init_mode == 'normal':
            return tf.Variable(init_gain * tf.random.normal(shape, dtype=self._dtype, seed=self.seed), dtype=self._dtype)
        elif init_mode == 'trunc_normal':
            return tf.Variable(init_gain * tf.random.truncated_normal(shape, dtype=self._dtype, seed=self.seed), dtype=self._dtype)
        elif init_mode == 'uniform':
            return tf.Variable(init_gain * tf.random.uniform(shape, minval=-1.0, maxval=1.0, dtype=self._dtype, seed=self.seed),
                               dtype=self._dtype)
        else:
            raise ValueError("{} is not a valid initialization distribution".format(init_mode))


class ActivationLayer(tf.keras.layers.Layer):
    """Simple activation layer.

    :param act: Activation function.
    """
    def __init__(self, act: Callable[[tf.Tensor], tf.Tensor]):
        super().__init__()
        self.act = act

    def call(self, x: tf.Tensor) -> tf.Tensor:
        """Returns output of the activation layer.

        :param x: Input tensor.
        :return: A tensor containing output of the activation layer.
        """
        return self.act(x)


class CustomBatchNorm(tf.keras.layers.Layer):
    """Compute a custom version of Batch Normalization. Used only for models trained without force information.

    :param n_features: Number of input features.
    :param norm_scale_factor: Empirically introduced rescaling.
    :param bn_momentum: Momentum of Batch Normalization.
    :param bn_eps: Small number to avoid division by zero.
    """
    def __init__(self, n_features: int, norm_scale_factor: float = 0.4, bn_momentum: float = 0.9,
                 bn_eps: float = 1e-8):
        super().__init__()
        self.n_features = n_features
        self.momentum = bn_momentum
        self.eps = bn_eps
        self.norm_scale_factor = norm_scale_factor

        # don't use bias in this BatchNorm variant
        self.running_mean = tf.Variable(tf.zeros(shape=[n_features]), trainable=False)
        self.running_var = tf.Variable(0.0, trainable=False)
        self.missing_fraction = tf.Variable(1.0, trainable=False)

    def call(self, x: tf.Tensor, training: bool) -> tf.Tensor:
        """Returns output of the normalized layer.

        :param x: Input tensor.
        :param training: Set True during training and False during inference.
        :return: Normalized output.
        """
        if training:
            mean = tf.reduce_mean(x, 0)
            var = tf.reduce_mean((x-mean[None, :])**2)

            # --- update statistics ---
            running_mean = self.momentum * self.running_mean + (1.0 - self.momentum) * mean
            running_var = self.momentum * self.running_var + (1.0 - self.momentum) * var
            self.running_mean.assign(running_mean)
            self.running_var.assign(running_var)
            self.missing_fraction.assign(self.momentum * self.missing_fraction)

        correction_factor = 1.0 / (1.0 - self.missing_fraction + self.eps)
        corrected_running_mean = correction_factor * self.running_mean
        corrected_running_var = correction_factor * (self.running_var + self.eps)

        running_normalized = self.norm_scale_factor * (x - corrected_running_mean) / tf.sqrt(corrected_running_var)

        return running_normalized


class ScaleShiftModel(tf.keras.layers.Layer):
    """Takes a model which begins e.g. with a GaussianMomentsLayer and then rescales and shifts the outputs
    using variables that are computed in :class:`gmnn.data_provider.DataProvider`.

    :param model: Model whose output should be rescaled and shifted.
    :param atomic_energy_regression: Parameters by which atomic energies should be shifted.
    :param atomic_energy_std: Parameters by which atomic energies should be rescaled.
    :param n_species: The maximal atomic number of atomic species.
    :param dtype: Floating point type (e.g. tf.float32).
    """
    def __init__(self, model: tf.keras.layers.Layer, atomic_energy_regression: Union[float, np.ndarray],
                 atomic_energy_std: float, n_species: int = 119, dtype=tf.float32):
        # takes a model which begins e.g. with a GaussianMomentsLayer
        # and then rescales and shifts the outputs using variables
        # that are initialized using atomic_energy_mean and atomic_energy_std
        super().__init__()
        self.model = model

        # shape: n_species x n_models x 1
        self.E_scale = tf.Variable(tf.constant(atomic_energy_std, shape=[n_species, 1], dtype=dtype), name="E_scale")
        reg_tensor = tf.convert_to_tensor(atomic_energy_regression, dtype=dtype)
        # shape: n_species x n_models x 1
        self.E_shift = tf.Variable(reg_tensor[:, None], dtype=dtype, name="E_shift")

        self.E_factor = tf.Variable(atomic_energy_std, dtype=dtype, trainable=False)
        self.E_scale.assign(self.E_scale / self.E_factor)
        self.E_shift.assign(self.E_shift / self.E_factor)

    def call(self, x: tf.Tensor) -> tf.Tensor:
        """
        :param x: Inputs to the model.
        :return: A tensor containing the rescaled and shifted output of the specified model.
        """
        # should be the second element of the tuple
        Z = x[1]
        y_out = self.model(x)
        return self.E_factor * (tf.gather(self.E_scale, Z) * y_out + tf.gather(self.E_shift, Z))


class ScaleShiftTensor(tf.keras.layers.Layer):
    """Takes a model which begins e.g. with a GaussianMomentsLayer and then rescales and shifts the outputs
    using variables that are initialized in :class:`gmnn.data_provider.DataProvider`.

    :param model: Model whose output should be rescaled and shifted.
    :param tensor_mean: Parameters by which tensor elements should be shifted.
    :param tensor_std: Parameters by which tensor elements should be scaled.
    :param n_species: The maximal atomic number of atomic species.
    :param dtype: Floating point type (e.g. tf.float32).
    """
    def __init__(self, model: tf.keras.layers.Layer, tensor_mean: float, tensor_std: float, n_species: int = 119,
                 dtype=tf.float32):
        # takes a model which begins e.g. with a GaussianMomentsLayer
        # and then rescales and shifts the outputs using variables
        # that are initialized using atomic_energy_mean and atomic_energy_std
        super().__init__()
        self.model = model

        # shape: n_species x n_models x 1
        self.MAT_scale = tf.Variable(tf.constant(tensor_std, shape=[n_species, 1], dtype=dtype), name="MAT_scale")
        self.MAT_shift = tf.Variable(tf.constant(tensor_mean, shape=[n_species, 1], dtype=dtype), name="MAT_shift")

    def call(self, x: tf.Tensor) -> tf.Tensor:
        """
        :param x: Inputs to the model.
        :return: A tensor containing the rescaled and shifted output of the specified model.
        """
        Z = x[1]  # should be the second element of the tuple
        y_out = self.model(x)
        return tf.gather(self.MAT_scale, Z) * y_out + tf.gather(self.MAT_shift, Z)



