import numpy as np
from ase.io import read

def atomic_energy(atomic_numbers):
    # computes the sum of atomic energies to obtain cohesive energy

    dict = {
            "8":   -432.503149303,
            "22": -1604.604515075
            }

    out = np.array([dict[str(key)] for key in atomic_numbers]).sum()
    return out


if __name__ == '__main__':
    # parameters
    n_maxat = 95
    n_data = 7815
    dir = "raw_data"
    data_set = "TiO2"

    # numpy arrays
    R = np.zeros((n_data, n_maxat, 3), dtype=np.float64)    # Cartesian coordinates
    F = np.zeros((n_data, n_maxat, 3), dtype=np.float64)    # atomic forces
    E = np.zeros((n_data, ), dtype=np.float64)              # total energies
    C = np.zeros((n_data, 3, 3), dtype=np.float64)          # periodic cell
    Z = np.zeros((n_data, n_maxat), dtype=np.int64)         # atomic numbers
    N = np.zeros((n_data, ), dtype=np.int64)                # number of atoms


    # read xsf data
    i_data = 0
    for i_file in range(n_data):

        filename = "structure{:04d}.xsf".format(i_file+1)
        file = open(dir + "/" + filename, 'r')
        atoms = read(dir + "/" + filename)
        _, _, _, _, ene, _ = file.readline().split()

        # skip the lines
        file.readline()
        file.readline()
        file.readline()
        file.readline()
        file.readline()
        file.readline()
        file.readline()
        file.readline()

        F_at = np.zeros((len(atoms), 3))
        for i_atom in range(len(atoms)):
            _, _, _, _, fx, fy, fz = file.readline().split()
            F_at[i_atom, :] = [float(fx), float(fy), float(fz)]

        R[i_data, :len(atoms), :] = atoms.get_positions()
        F[i_data, :len(atoms), :] = F_at * 23.06    # kcal/mol/Angstrom
        E[i_data] = (float(ene) - atomic_energy(atoms.get_atomic_numbers())) * 23.06    # kcal/mol
        C[i_data] = atoms.get_cell()
        Z[i_data, :len(atoms)] = atoms.get_atomic_numbers()
        N[i_data] = len(atoms)

        i_data += 1

    np.savez(data_set, R=R, F=F, E=E, C=C, Z=Z, N=N)
