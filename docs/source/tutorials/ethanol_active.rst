.. _ethanol_active-tutorial:

========================
Active Learning: Ethanol
========================

Active Learning PES Models
__________________________

Preparation
***********

Here, we again employ the `Ethanol <https://github.com/zaverkin/ethanol_datasets_git>`_ data set
from :ref:`ethanol-tutorial`, but learn our models by actively selecting new training data.
Specifically, we show how to run several experiments to compare different selection methods and uncertainty
estimates similar to the original publication [1].

First, we need to define the combinations of selection methods and kernels (uncertainty estimates) which define specific
active learning approaches; see Ref. [1]. For this purpose, we employ `generate_config.py` script,
which generates most of the methods used in Ref. [1] and writes several configuration files, e.g.,
`ethanol_1000K_F_inv_max_det_greedy_0_10_100_10_0.txt`, into the predefined folder

.. literalinclude:: ../../../examples/generate_config.py
   :encoding: latin-1

The respective configuration files contain only necessary parameters, while the rest is set to default;
see :ref:`parameters`. As an example, we show the content of `ethanol_1000K_F_inv_max_det_greedy_0_10_100_10_0.txt`

.. highlight:: none
.. code-block:: none

    --data_path=ethanol_1000K.npz
    --data_seed=0
    --model_seed=0
    --n_models=1
    --kernel=F_inv
    --selection=max_det_greedy
    --n_random_features=0
    --forces=1
    --cutoff=4.0
    --n_valid=200
    --neighbor_list=0
    --model_path=models/ethanol_1000K_F_inv_max_det_greedy_0_10_100_10_0
    --model_name=model
    --n_pool=4000
    --max_n_train=100
    --n_train=10
    --max_al_batch_size=10
    --batch_size=10

Training
********

Next, for training several models in parallel, we employ multiprocessing_. Particularly, we run the following script

.. highlight:: python
.. code-block:: python

    from gmnn.trainer import *
    from multiprocessing import Pool
    import tensorflow as tf
    import glob
    import numpy as np
    import sys
    import traceback


    def run_config(config_filename):
        print(f'Starting config filename {config_filename}', flush=True)
        try:
            tf.config.threading.set_intra_op_parallelism_threads(12)
            tf.config.threading.set_inter_op_parallelism_threads(12)
            trainer = ActivePESTrainer(config=config_filename)
            trainer.run_al()
        except Exception as e:
            traceback.print_exc()
        print(f'Finished config filename {config_filename}', flush=True)


    if __name__ == '__main__':
        config_files = glob.glob('*.txt')
        print(f'Running on files:')
        print('\n'.join(config_files))
        with Pool(8) as p:
            p.map(run_config, config_files)

The result is written into separate folders with the same name as the respective configuration file.
However, we will also find the respective AL steps. For example, in `ethanol_1000K_F_inv_max_det_greedy_0_10_100_10_0/model`
we find

.. highlight:: bash
.. code-block:: bash

    $ cd ethanol_1000K_F_inv_max_det_greedy_0_10_100_10_0/model
    $ ls
    ethanol_1000K_F_inv_max_det_greedy_0_10_100_10_0.txt  step_0  step_1  step_2  step_3  step_4  step_5  step_6  step_7  step_8  step_9

Each `step_i/` folder has the same structure as the model in :ref:`ethanol-tutorial`, but we delete `i/` folders
for intermediate steps to save the memory needed to store the data. Thus, only the `train.out` results on the
intermediate steps are stored. In general, the `train.out` file contains the same information as in :ref:`ethanol-tutorial`.
However, it has additional lines with the results for the respective AL steps. For example, after the first AL step, `step_1/`
of the `ethanol_1000K_F_inv_max_det_greedy_0_10_100_10_0` experiment, we obtain

.. highlight:: none
.. code-block:: none

    ...

    Epoch 990/1000:   1.322/ 1.002/ 0.995                   1.060/ 0.812/ 0.802                  0.604/ 4.512/ 4.518                  0.459/ 3.123/ 3.127                [ 0.07 s]
    Epoch 991/1000:   1.320/ 1.002/ 0.995                   1.056/ 0.812/ 0.802                  0.604/ 4.512/ 4.518                  0.459/ 3.124/ 3.127                [ 0.07 s]
    Epoch 992/1000:   1.320/ 1.001/ 0.995                   1.055/ 0.811/ 0.802                  0.604/ 4.512/ 4.518                  0.459/ 3.124/ 3.127                [ 0.06 s]
    Epoch 993/1000:   1.318/ 1.000/ 0.995                   1.054/ 0.810/ 0.802                  0.604/ 4.513/ 4.518                  0.459/ 3.124/ 3.127                [ 0.06 s]
    Epoch 994/1000:   1.319/ 1.001/ 0.995                   1.055/ 0.811/ 0.802                  0.604/ 4.513/ 4.518                  0.459/ 3.124/ 3.127                [ 0.06 s]
    Epoch 995/1000:   1.319/ 1.000/ 0.995                   1.055/ 0.810/ 0.802                  0.604/ 4.513/ 4.518                  0.459/ 3.124/ 3.127                [ 0.06 s]
    Epoch 996/1000:   1.318/ 1.000/ 0.995                   1.054/ 0.810/ 0.802                  0.603/ 4.513/ 4.518                  0.459/ 3.124/ 3.127                [ 0.11 s]
    Epoch 997/1000:   1.319/ 1.000/ 0.995                   1.054/ 0.810/ 0.802                  0.603/ 4.513/ 4.518                  0.459/ 3.124/ 3.127                [ 0.09 s]
    Epoch 998/1000:   1.319/ 1.000/ 0.995                   1.055/ 0.810/ 0.802                  0.604/ 4.513/ 4.518                  0.459/ 3.124/ 3.127                [ 0.11 s]
    Epoch 999/1000:   1.319/ 1.000/ 0.995                   1.054/ 0.810/ 0.802                  0.604/ 4.513/ 4.518                  0.459/ 3.124/ 3.127                [ 0.10 s]
    Epoch 1000/1000:  1.317/ 1.000/ 0.995                   1.053/ 0.810/ 0.802                  0.603/ 4.513/ 4.518                  0.459/ 3.124/ 3.127                [ 0.12 s]
    ==============================================================================================================================================================================
    Timing report
    -------------
    Total time                    ............. 112.57757019996643
    Best model report
    -----------------
    Best epochs from the training  ............. [877]
    ====================================================================================================
    Next train and pool data set sizes & mean size of selected structures:

    Mean-N_at           Next-N_train        Next-N_pool         matrix time [s]     selection_time [s]
    ====================================================================================================
     9.00000                 30                3980              2.08653             0.23137
    ====================================================================================================
    ====================================================================================================
    Results of 1 models obtained on the test data:

                        Energy-RMSE         Energy-MAE          Force-RMSE          Force-MAE
    ====================================================================================================
    Ensemble:            1.11179             0.85330             4.43157             3.10381
    Mean individual:     1.11179             0.85330             4.43157             3.10381
    Model 1:             1.11179             0.85330             4.43157             3.10381
    ====================================================================================================
    ========================================================================================================================
    Active learning results obtained on the test data:

                        PearsonR            SpearmanR           MaxError            % < chem. acc.      95% quantile
    ========================================================================================================================
    Energy:              0.06732             0.01277             4.42188             0.65570             2.18984
    Force:               0.16172             0.08507             7.11578             0.00000             4.59601
    ========================================================================================================================

Moreover, we find `idxs.npy` in the respective folder, which contains the indices of selected structures as given for
the original data set (before splitting). These indices are also used for restarting AL experiments. Finally,
we show how to analyze our AL experiments' vast amount of data.

Analyzing AL experiments
________________________

We employ the `ExperimentResults` in `al_analysis.py` to analyse the AL results. However, we need to extract all
relevant data from `train.out` files. For this purpose, we employ the `extract_results.py` script

.. highlight:: python
.. literalinclude:: ../../../examples/extract_results.py
   :encoding: latin-1

Now we analyze the generated `all_results.npz` file by `ExperimentResults` and generate plots employing functions in
`al_plotting.py`

.. highlight:: python
.. code-block:: python

    from al_analysis import *
    from al_plotting import *


    if __name__ == '__main__':
        results = ExperimentResults.load()

        # tables
        save_latex_table_learning_curves_eng(results,
                                             filename='ethanol_1000K_10_100_10_learning_curve.txt',
                                             metric_names=['emae', 'ermse', 'emaxe', 'epr', 'esr', 'eq95',
                                                           'fmae', 'frmse', 'fmaxe', 'fpr', 'fsr', 'fq95',
                                                           'matrix_time', 'selection_time'],
                                             task_name='ethanol_1000K_10_100_10',
                                             alg_names=['random_0_max_diag', 'ae-force_0_max_diag', 'qbc-force_0_max_diag',
                                                        'F_inv_0_max_diag', 'F_inv_512_max_diag', 'F_inv_0_max_det_greedy',
                                                        'F_inv_512_max_det_greedy', 'g_0_max_dist_greedy',
                                                        'g_512_max_dist_greedy', 'g_0_lcmd_greedy', 'g_512_lcmd_greedy'])
        save_latex_table_last_error_eng(results,
                                        filename='ethanol_1000K_10_100_10_last_errors.txt',
                                        metric_names=['emae', 'ermse', 'emaxe', 'epr', 'esr', 'eq95',
                                                      'fmae', 'frmse', 'fmaxe', 'fpr', 'fsr', 'fq95'],
                                        task_name='ethanol_1000K_10_100_10',
                                        alg_names=['random_0_max_diag', 'ae-force_0_max_diag', 'qbc-force_0_max_diag',
                                                        'F_inv_0_max_diag', 'F_inv_512_max_diag', 'F_inv_0_max_det_greedy',
                                                        'F_inv_512_max_det_greedy', 'g_0_max_dist_greedy',
                                                        'g_512_max_dist_greedy', 'g_0_lcmd_greedy', 'g_512_lcmd_greedy'])
        # plots
        plot_multiple_learning_curves(results,
                                      filename='force_errors_ethanol_1000K_10_100_learning_curve.png',
                                      metric_names=['fmae', 'frmse', 'fmaxe'],
                                      task_name='ethanol_1000K_10_100_10',
                                      alg_names=['random_0_max_diag', 'qbc-force_0_max_diag', 'ae-force_0_max_diag',
                                                 'F_inv_0_max_diag', 'F_inv_0_max_det_greedy', 'g_0_max_dist_greedy',
                                                 'g_0_lcmd_greedy'])
        plot_multiple_batch_sizes(results,
                                  filename='force_errors_ethanol_1000K_10_100_batch_size.png',
                                  metric_names=['fmae', 'frmse', 'fmaxe'],
                                  task_names=['ethanol_1000K_10_100_2', 'ethanol_1000K_10_100_5', 'ethanol_1000K_10_100_10'],
                                  alg_names=['random_0_max_diag', 'qbc-force_0_max_diag', 'ae-force_0_max_diag',
                                             'F_inv_0_max_diag', 'F_inv_0_max_det_greedy', 'g_0_max_dist_greedy',
                                             'g_0_lcmd_greedy'])

The following plot shows the learning curves for different methods obtained for the maximal AL batch size of 10

.. _fig-lc:
.. figure:: force_errors_ethanol_1000K_10_100_learning_curve.png
   :width: 1000px
   :align: center

The batch size dependence can be found in the figure below

.. _fig-le:
.. figure:: force_errors_ethanol_1000K_10_100_batch_size.png
   :width: 1000px
   :align: center

The results averaged over learning curves are given in the `ethanol_1000K_10_100_10_learning_curve.txt`, while the results
for the maximal batch size of 10 and the last AL step can be found in `ethanol_1000K_10_100_10_last_errors.txt`. The generated files
can be directly used as an input to a LaTeX_ document.

The `Ethanol <https://github.com/zaverkin/ethanol_datasets_git>`_ data set
is probably not the best example to see the power of AL applied to PES modeling. However, it allows for fast training
and, thus, an introduction to AL modules' basic features in the GM-NN package. Better results are expected for more
diverse data sets or in applications on the fly.

References
__________

* [1] V. Zaverkin, D. Holzmüller, I. Steinwart,  and J. Kästner, “Exploring Chemical and Conformational Spaces by Batch Mode Deep Active Learning,” **submitted** (2022).

.. _multiprocessing: https://docs.python.org/3/library/multiprocessing.html
.. _LaTeX: https://www.latex-project.org/