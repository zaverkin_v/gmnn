.. _data-pipeline:

=============
Data Pipeline
=============

Data Container
______________
.. autoclass:: gmnn.data_pipeline.DataContainer
    :members:
    :special-members: __len__, __getitem__

Data Loader
___________
.. autoclass:: gmnn.data_pipeline.DataLoader
    :members:
    :special-members: __len__, __iter__
