import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import uuid
import shutil
from gmnn.utils import *
from gmnn.layers import *
from gmnn.pes_fit import PESFit
from gmnn.mat_fit import MATFit
from gmnn.data_pipeline import *
from gmnn.parameters import Parameters
from gmnn.active_learning import GenericALMethod


class Trainer:
    def __init__(self, config='configure_training.txt'):
        self.config = config

        # set up memory growth
        gpus = tf.config.experimental.list_physical_devices('GPU')
        if gpus:
            try:
                # memory growth needs to be the same across GPUs
                for gpu in gpus:
                    # uses only as much memory as needed
                    tf.config.experimental.set_memory_growth(gpu, True)
            except RuntimeError as e:
                # memory growth must be set before GPUs have been initialized
                print(e)

        # read parameters
        parameters = Parameters(self.config)
        self.args = parameters.get_hyperparameters()
        if self.args.bn_momentum is None:
            self.args.bn_momentum = 0.5 ** (1 / (self.args.n_train // self.args.batch_size))

        # define model directory
        if not os.path.exists(self.args.model_path):
            os.makedirs(self.args.model_path)
        if self.args.model_name is None:
            # creates an unique id for job
            directory = str(uuid.uuid4())
        else:
            directory = self.args.model_name
        self.model_path = os.path.join(self.args.model_path, directory)
        if not os.path.exists(self.model_path):
            os.makedirs(self.model_path)
        # store the config file
        with open(os.path.join(self.model_path, self.config), 'w') as conf:
            for arg in vars(self.args):
                conf.write("--" + arg + "=" + str(getattr(self.args, arg)) + "\n")

        # define activation function
        if self.args.activation == "softplus":
            self.activation = tf.nn.softplus
        elif self.args.activation == "swish":
            self.activation = lambda x: 1.6765324703310907 * tf.nn.swish(x)
        else:
            raise NotImplementedError()

        # define neural network architecture
        self.args.architecture = [int(item) for item in self.args.architecture.split(',')]
        # compute number of invariant features from n_radial
        if bool(self.args.use_all_features):
            n_features = int(self.args.n_radial + 3 * self.args.n_radial * (self.args.n_radial + 1) / 2 \
                             + self.args.n_radial * (self.args.n_radial + 1) * (
                                     self.args.n_radial + 2) / 6 + self.args.n_radial ** 3 \
                             + 2 * self.args.n_radial ** 2 * (self.args.n_radial + 1) / 2)
        else:
            n_features = int(self.args.n_radial + 3 * self.args.n_radial * (self.args.n_radial + 1) / 2 \
                             + 4 * self.args.n_radial * (self.args.n_radial + 1) * (self.args.n_radial + 2) / 6)
        # include input and output dimensions
        self.args.architecture = [n_features] + self.args.architecture + [1]

        # define learning rate
        self.lr = [self.args.scale_lr, self.args.shift_lr, self.args.emb_lr] + [self.args.base_lr] * (
                len(self.args.architecture) - 1) * 2

        # define learning schedule
        self.lr_sched = lambda x: 1. - x

    def _setup_model(self):
        # tensorflow seed
        tf.random.set_seed(self.args.model_seed)
        # define list of models
        self.models = []
        for i in range(self.args.n_models):
            # define layers
            layers = [GaussianMomentsLayer(n_radial=self.args.n_radial, n_contr=8, n_basis=self.args.n_basis,
                                           r_max=self.args.cutoff, r_min=self.args.r_min, emb_init=self.args.emb_init,
                                           use_all_features=bool(self.args.use_all_features))]
            if bool(self.args.batch_norm):
                layers.append(
                    CustomBatchNorm(n_features=self.args.architecture[0], norm_scale_factor=self.args.norm_scale_factor,
                                    bn_momentum=self.args.bn_momentum))
            for j in range(len(self.args.architecture) - 1):
                layers.append(
                    LinearLayer(in_features=self.args.architecture[j], out_features=self.args.architecture[j + 1],
                                init_normalization=self.args.init_normalization,
                                weight_init_mode=self.args.weight_init_mode, bias_init_mode=self.args.bias_init_mode,
                                bias_factor=self.args.bias_factor))
                if j < len(self.args.architecture) - 2:
                    layers.append(ActivationLayer(self.activation))
            # scale and shift model
            model = self._scale_shift_model(layers)
            # append models
            self.models.append(model)

    def _scale_shift_model(self, layers) -> tf.keras.layers.Layer:
        raise NotImplementedError()


class PESTrainer(Trainer):
    """Manages the training of PES models.

    :param config: Path to a file containing the model parameters.

    Example:

    .. highlight:: python
    .. code-block:: python

        from gmnn.trainer import PESTrainer

        if __name__ == '__main__':

            trainer = PESTrainer(config="pes_training.txt")
            trainer.fit()
            trainer.eval()
    """
    def __init__(self, config: str):
        super().__init__(config=config)
        # set up data containers
        # do not save as self.dc to allow to release the memory of dc after calling split()
        # same for dictionary, which should not be saved as self.dictionary

        if self.args.data_path:
            dictionary = np.load(self.args.data_path)
            dc = DataContainer(dictionary=dictionary, cutoff=self.args.cutoff,
                               neighbor_list=bool(self.args.neighbor_list), skin=self.args.skin)
            self.dcs = dc.split({"train": self.args.n_train, "valid": self.args.n_valid}, seed=self.args.data_seed)
        else:
            train_dictionary = np.load(self.args.train_data_path)
            train_dc = DataContainer(dictionary=train_dictionary, cutoff=self.args.cutoff,
                                     neighbor_list=bool(self.args.neighbor_list), skin=self.args.skin)
            valid_dictionary = np.load(self.args.valid_data_path)
            valid_dc = DataContainer(dictionary=valid_dictionary, cutoff=self.args.cutoff,
                                     neighbor_list=bool(self.args.neighbor_list), skin=self.args.skin)
            self.dcs = {
                "train": train_dc,
                "valid": valid_dc
            }

        # set up model
        self._setup_model()
        # initialize model from the pretrained one, if provided
        if self.args.init_path is not None:
            self._load_pretrained_params()
        # construct trainer
        self.pes_fit = PESFit(self.models, forces=self.args.forces, stress=self.args.stress,
                              force_weight=self.args.force_weight, stress_weight=self.args.stress_weight,
                              pbc=bool(self.args.pbc), beta_2=self.args.beta_2, device_number=self.args.device_number,
                              loss=self.args.loss)

    def _scale_shift_model(self, layers):
        return ScaleShiftModel(Sequential(layers), atomic_energy_std=self.dcs["train"].EperA_stdev,
                               atomic_energy_regression=self.dcs["train"].EperA_regression)

    def _load_pretrained_params(self):
        # loop over models
        for i in range(len(self.models)):
            # load model
            ckpt = tf.train.Checkpoint(epoch=tf.Variable(0), model=self.models[i])
            best_dir = os.path.join(self.args.init_path, str(i), 'best')
            ckpt_manager_best = tf.train.CheckpointManager(ckpt, best_dir, max_to_keep=10)
            ckpt.restore(ckpt_manager_best.latest_checkpoint).expect_partial()
            # reinitialize scale and shift
            self.models[i] = self._scale_shift_model([layer for layer in self.models[i].model.layers_])

        # define learning rate
        self.args.fine_tune_lrs = [float(item) for item in self.args.fine_tune_lrs.split(',')]
        if len(self.args.fine_tune_lrs) != (len(self.args.architecture) - 1):
            raise ValueError(f'The network has {len(self.args.architecture) - 1} layers, '
                             f'but only {len(self.args.fine_tune_lrs)} learning rates are provided.')
        self.lr = [self.args.scale_lr, self.args.shift_lr, self.args.emb_lr]
        for ii in range(len(self.args.fine_tune_lrs)):
            self.lr += [self.args.fine_tune_lrs[ii]] * 2

    def fit(self):
        """Train model on preselected training data.
        """
        self.pes_fit.fit(model_path=self.model_path, train_dc=self.dcs["train"], valid_dc=self.dcs["valid"],
                         train_batch_size=self.args.batch_size, valid_batch_size=self.args.valid_batch_size,
                         capacity=self.args.capacity, lr=self.lr, lr_sched=self.lr_sched, max_epoch=self.args.max_epoch,
                         save_epoch=self.args.save_epoch, validate_epoch=self.args.valid_epoch,
                         mae_stopping=bool(self.args.use_mae_stopping), seed=self.args.data_seed)

    def eval(self, test_data_path: Optional[str] = None, test_batch_size: Optional[int] = None):
        """Evaluate model on preselected test data.
        """
        if "test" not in self.dcs and test_data_path is None:
            raise RuntimeError("Test data is not provided!")
        else:
            self.pes_fit.eval(model_path=self.model_path,
                              test_dc=(self.dcs["test"] if test_data_path is None else
                                       DataContainer(dictionary=np.load(test_data_path), cutoff=self.args.cutoff,
                                                     neighbor_list=bool(self.args.neighbor_list), skin=self.args.skin)),
                              test_batch_size=(self.args.valid_batch_size if test_batch_size is None
                                               else test_batch_size),
                              capacity=self.args.capacity)

    def measure_inference_time(self):
        """Measure inference time of the model.
        """
        if "test" not in self.dcs:
            raise RuntimeError("Test data is not provided!")
        else:
            self.pes_fit.measure_inference_time(model_path=self.model_path, test_dc=self.dcs["test"],
                                                test_batch_size=self.args.valid_batch_size, capacity=self.args.capacity)


class ActivePESTrainer(Trainer):
    """Manages the training of PES models using active learning.

    :param config: Path to a file containing the model parameters.

    Example:

    .. highlight:: python
    .. code-block:: python

        from gmnn.trainer import ActivePESTrainer

        if __name__ == '__main__':

            trainer = ActivePESTrainer(config="active_pes_training.txt")
            trainer.run_al()
    """
    def __init__(self, config: str):
        super().__init__(config=config)
        # set up data containers
        # do not save as self.dc to allow to release the memory of dc after calling split()
        # same for dictionary, which should not be saved as self.dictionary
        dictionary = np.load(self.args.data_path)
        dc = DataContainer(dictionary=dictionary, cutoff=self.args.cutoff,
                                neighbor_list=bool(self.args.neighbor_list), skin=self.args.skin)
        self.dcs = dc.split({"train": self.args.n_train, "valid": self.args.n_valid, "pool": self.args.n_pool},
                                 seed=self.args.data_seed)

    def _scale_shift_model(self, layers):
        return ScaleShiftModel(Sequential(layers), atomic_energy_std=self.dcs["train"].EperA_stdev,
                               atomic_energy_regression=self.dcs["train"].EperA_regression)

    def run_al(self, test_data_path: Optional[str] = None):
        """Run active learning loop.
        """
        if test_data_path is not None:
            self.dcs['test'] = DataContainer(dictionary=np.load(test_data_path), cutoff=self.args.cutoff,
                                             neighbor_list=bool(self.args.neighbor_list), skin=self.args.skin)

        al_step = 0
        while al_step < self.args.max_al_step and len(self.dcs['train']) <= self.args.max_n_train:
            # scale the maximal batch size selected by active learning
            max_al_batch_size = int(self.args.max_al_batch_size * self.args.al_batch_scaling ** al_step)

            # modify model path
            new_model_path = self.model_path + f'/step_{al_step}'

            if os.path.isfile(new_model_path + '/idxs.npy'):
                # restart active learning using saved indices
                idxs = np.load(new_model_path + '/idxs.npy')
                # update training and pool data
                # first, split pool data
                sub_dcs = self.dcs['pool'].split_by_indices(idxs)
                # update pool and train containers
                self.dcs['pool'] = sub_dcs['remaining']
                self.dcs['train'] = self.dcs['train'].join_containers(sub_dcs['selected'])
            else:
                # set up model
                self._setup_model()
                # construct trainer
                pes_fit = PESFit(self.models, forces=self.args.forces, stress=self.args.stress,
                                 force_weight=self.args.force_weight, stress_weight=self.args.stress_weight,
                                 pbc=bool(self.args.pbc), beta_2=self.args.beta_2,
                                 device_number=self.args.device_number, loss=self.args.loss)
                # train model
                pes_fit.fit(model_path=new_model_path, train_dc=self.dcs["train"], valid_dc=self.dcs["valid"],
                            train_batch_size=self.args.batch_size, valid_batch_size=self.args.valid_batch_size,
                            capacity=self.args.capacity, lr=self.lr, lr_sched=self.lr_sched, max_epoch=self.args.max_epoch,
                            save_epoch=self.args.save_epoch, validate_epoch=self.args.valid_epoch,
                            mae_stopping=bool(self.args.use_mae_stopping), seed=self.args.data_seed)

                # determine device
                gpus = tf.config.experimental.list_physical_devices('GPU')
                if gpus and not bool(self.args.al_on_cpu):
                    device = f'/GPU:{self.args.device_number}'
                else:
                    device = '/CPU:0'
                # active learning
                with tf.device(device):
                    al = GenericALMethod(kernel=self.args.kernel, selection=self.args.selection, feature_layer=-2,
                                         pbc=self.args.pbc, force=self.args.forces, device=device,
                                         capacity=self.args.capacity, n_random_features=self.args.n_random_features,
                                         EperA_errors=bool(self.args.EperA_errors))

                    idxs, matrix_time, selection_time, random_projections = al.select(models=self.models, dcs=self.dcs,
                                                                                      batch_size=self.args.al_batch_size,
                                                                                      max_al_batch_size=max_al_batch_size)

                # save indices
                np.save(new_model_path + '/idxs.npy', idxs)

                # update training and pool data
                # first, split pool data
                sub_dcs = self.dcs['pool'].split_by_indices(idxs.numpy())
                # update pool and train containers
                self.dcs['pool'] = sub_dcs['remaining']
                self.dcs['train'] = self.dcs['train'].join_containers(sub_dcs['selected'])
                # write changes in the containers + mean size of selected structures
                test_out = new_model_path + '/train.out'
                write_results = ['Mean-N_at', 'Next-N_train', 'Next-N_pool', 'matrix time [s]', 'selection_time [s]']
                column_widths = [20] * len(write_results)
                f = open(test_out, "a+")
                f.write("".ljust(sum(column_widths), "=") + "\n")
                f.write(f'Next train and pool data set sizes & mean size of selected structures:\n')
                f.write(' \n')
                f.write(padded_str(write_results, column_widths) + '\n')
                f.write("".ljust(sum(column_widths), "=") + "\n")
                results = [np.mean(sub_dcs['selected'].N), len(self.dcs['train']), len(self.dcs['pool'])]
                f.write(padded_str([f'{results[0]:8.5f}'] + [f'{results[1]:7d}'] + [f'{results[2]:7d}'] +
                                   [f'{matrix_time:8.5f}'] + [f'{selection_time:8.5f}'],
                                   column_widths) + '\n')
                f.write("".ljust(sum(column_widths), "=") + "\n")
                f.close()

                if 'test' in self.dcs:
                    # evaluate error metric
                    pes_fit.eval(model_path=new_model_path, test_dc=self.dcs["test"],
                                 test_batch_size=self.args.valid_batch_size, capacity=self.args.capacity)

                    with tf.device(device):
                        # evaluate al statistics
                        al.eval(model_path=new_model_path, models=self.models, dcs=self.dcs,
                                batch_size=self.args.al_batch_size, random_projections=random_projections)

            al_step += 1

        for step_path in [self.model_path + f'/step_{i}' for i in range(1, al_step-1)]:
            # delete all models except first and last
            model_folders = [f for f in os.listdir(step_path) if not os.path.isfile(os.path.join(step_path, f))]
            for model_folder in model_folders:
                shutil.rmtree(step_path + '/' + model_folder)


class MATTrainer(Trainer):
    """Manages the training of MAT models.

    :param config: Path to a file containing the model parameters.

    Example:

    .. highlight:: python
    .. code-block:: python

        from gmnn.trainer import MATTrainer

        if __name__ == '__main__':

            trainer = MATTrainer(config="configure_training.txt")
            trainer.fit()
            trainer.eval()
    """
    def __init__(self, config: str):
        super().__init__(config=config)
        # set up data containers
        # do not save as self.dc to allow to release the memory of dc after calling split()
        # same for dictionary, which should not be saved as self.dictionary
        dictionary = np.load(self.args.data_path)
        dc = DataContainer(dictionary=dictionary, cutoff=self.args.cutoff,
                                neighbor_list=bool(self.args.neighbor_list), skin=self.args.skin)
        self.dcs = dc.split({"train": self.args.n_train, "valid": self.args.n_valid}, seed=self.args.data_seed)
        # set up model
        self._setup_model()
        # construct trainer
        self.mat_fit = MATFit(self.models, pbc=bool(self.args.pbc), beta_2=self.args.beta_2,
                              device_number=self.args.device_number)

    def _scale_shift_model(self, layers):
        return ScaleShiftTensor(Sequential(layers), tensor_std=self.dcs["train"].MATperA_stdev,
                                tensor_mean=self.dcs["train"].MATperA_mean)

    def fit(self):
        """Train model on preselected training data.
        """
        self.mat_fit.fit(model_path=self.model_path, train_dc=self.dcs["train"], valid_dc=self.dcs["valid"],
                         train_batch_size=self.args.batch_size, valid_batch_size=self.args.valid_batch_size,
                         capacity=self.args.capacity, lr=self.lr, lr_sched=self.lr_sched, max_epoch=self.args.max_epoch,
                         save_epoch=self.args.save_epoch, validate_epoch=self.args.valid_epoch,
                         mae_stopping=bool(self.args.use_mae_stopping), seed=self.args.data_seed)

    def eval(self):
        """Evaluate model on preselected test data.
        """
        if "test" not in self.dcs:
            raise RuntimeError("Test data is not provided!")
        else:
            self.mat_fit.eval(model_path=self.model_path, test_dc=self.dcs["test"],
                              test_batch_size=self.args.valid_batch_size, capacity=self.args.capacity)
