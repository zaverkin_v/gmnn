.. _ethanol-tutorial:

====================
Learning Ethanol PES
====================

Preparing Data Set
__________________

Before training the GM-NN models, the data set containing Cartesian coordinates, total energies, and atomic forces must
be prepared. This tutorial will work with the `Ethanol <https://github.com/zaverkin/ethanol_datasets_git>`_ data set
`ethanol_1000K.npz` recently published by us [1], available free of charge.

.. note::
    The shape of total energies contained in this data set has to be changed to fit the current version of the code, i.e.
    one has to run

    .. highlight:: python
    .. code-block:: python

        import numpy as np

        # load the data set
        dictionary = np.load("ethanol_1000K.npz")

        # save the data set prepared for GM-NN
        np.savez("ethanol_1000K.npz", E=dictionary['E'][:, 0], R=dictionary['R'], Z=dictionary['Z'],
                 N=dictionary['N'], F=dictionary['F'])

.. note::
    Here we use the data set written in `.npz` format. However, the raw ab-initio data is typically written as, e.g.,
    `.xsf` or `.xyz` files. An example of the data preparation for such a case can be found in :ref:`tio2-tutorial`.

Training PES Model
__________________

Preparation
***********

Once the ab-initio data is written in a suitable format (the :ref:`data-pipeline` implemented in this software package
uses a dictionary as an input), we can begin with training our PES model. In this tutorial, we use
:ref:`pes-trainer` which manages the building and training of atomistic NNs. However, it is possible to construct
custom models using :ref:`layers`. Using the :ref:`pes-trainer`, one needs to run only this minimal script to train
the predefined GM-NN model

.. highlight:: python
.. code-block:: python

    from gmnn.trainer import PESTrainer

    if __name__ == '__main__':

        trainer = PESTrainer(config="pes_training.txt")
        trainer.fit()
        trainer.eval()

The training process can be easily customized by changing the `pes_training.txt` file, which contains all the necessary
:ref:`parameters`. In this example, we employ

.. highlight:: none
.. code-block:: none

    --data_path=/SCR/HADES2_I1/zaverkin/tutorials/ethanol/dataset/ethanol_1000K.npz
    --model_path=/SCR/HADES2_I1/zaverkin/tutorials/ethanol/models/
    --model_name=example_1
    --cutoff=4.0
    --n_train=1000
    --n_valid=200
    --batch_size=32
    --valid_batch_size=100
    --data_seed=1
    --capacity=10
    --neighbor_list=0
    --skin=0.0
    --device_number=0
    --n_models=3
    --model_seed=1
    --architecture=512,512
    --weight_init_mode=normal
    --bias_init_mode=zeros
    --activation=swish
    --batch_norm=0
    --bn_momentum=0.9778885363354327
    --norm_scale_factor=0.4
    --init_normalization=ntk
    --weight_factor=1.0
    --bias_factor=0.1
    --pbc=0
    --n_radial=5
    --n_basis=7
    --emb_init=uniform
    --use_all_features=1
    --max_epoch=1000
    --save_epoch=100
    --valid_epoch=1
    --base_lr=0.03
    --scale_lr=0.001
    --shift_lr=0.05
    --emb_lr=0.02
    --forces=1
    --stress=0
    --force_weight=4.0
    --stress_weight=1.0
    --beta_2=0.999
    --use_mae_stopping=1

.. note::
    In this tutorial, we train an ensemble of 3 models. If a single model has to be trained, ``n_models=1`` must be
    specified. When training with energies and forces, the combined loss function

    .. math::

        \mathcal{L}\left(\boldsymbol{\theta}\right) = \sum_{k=1}^{N_\mathrm{Train}} \Bigg[\lambda_E
        \lVert E_k^\mathrm{ref} - \hat{E}(S_k, \boldsymbol{\theta})\rVert_2^2 +  \lambda_F
        \sum_{i=1}^{N_\mathrm{at}^{(k)}} \lVert \mathbf{F}_{i,k}^\mathrm{ref} - \hat{\mathbf{F}}_i\left(S_k,
        \boldsymbol{\theta}\right)\rVert_2^2\Bigg]~,

    is minimized, where :math:`\lambda_F` and :math:`\lambda_E` are the force and energy weights. :math:`\lambda_E`
    is set to 1 a.u. and only the force weight is changed in the practical implementation. If the model has to be
    trained with energies only, then ``forces=0`` has to be specified.

.. note::
    In this and the following tutorials, we select the layer-wise learning rates ``base_lr``, ``emb_lr``, ``scale_lr``,
    and ``shift_lr`` for `config_file`, which has been found to be the best for the certain task. However, different
    tradeoffs can be observed. Especially, for data sets containing only molecules in equilibrium, see QM9 [3,4], we recommend
    to use ``base_lr=0.005``, ``emb_lr=0.0025``, ``scale_lr=0.001``, and ``shift_lr=0.05``, while training only for
    ``max_epoch=500`` epochs.

Training
********

By running

.. highlight:: bash
.. code-block:: bash

    $ python3 train.py

the training process starts and following folders and files are created in the ``model_path``

.. highlight:: bash
.. code-block:: bash

    $ cd <model_path>
    $ ls
    0/  1/  2/  best_stats_ens.pkl  pes_training.txt  train.out

Model checkpoints used to restart training are saved in `i/logs` folders, where `i` is the number of the model in
the ensemble, while the best models used during inference are placed in `i`/best. The text file `train.out` contains
the whole history of the training process with mean absolute error (MAE) and root-mean-square error (RMSE) of total
energies and atomic forces, which are evaluated on training and validation data sets. Each error metric shows three
columns corresponding to the training error, running validation error, and the best validation error.
In this example, we obtain

.. highlight:: none
.. code-block:: none

    Best checkpoints for model 0 can be found in           ............. /SCR/HADES2_I1/zaverkin/tutorials/ethanol/models/example_1/0/best
    Checkpoints for restart for model 0 can be found in    ............. /SCR/HADES2_I1/zaverkin/tutorials/ethanol/models/example_1/0/logs

    Epoch            Energy-RMSE (train/valid/best_valid)  Energy-MAE (train/valid/best_valid)  Force-RMSE (train/valid/best_valid)  Force-MAE (train/valid/best_valid)  Time
    ==============================================================================================================================================================================
    Epoch 1/1000:    24.644/ 6.846/ 6.846                  17.132/ 6.503/ 6.503                 23.525/ 7.811/ 7.811                 15.175/ 5.576/ 5.576                [11.18 s]
    Epoch 2/1000:     5.174/ 2.351/ 2.351                   4.306/ 1.911/ 1.911                  6.353/ 5.383/ 5.383                  4.547/ 3.905/ 3.905                [ 1.26 s]
    Epoch 3/1000:     2.531/ 2.170/ 2.170                   1.996/ 1.811/ 1.811                  4.820/ 4.386/ 4.386                  3.472/ 3.166/ 3.166                [ 1.22 s]
    Epoch 4/1000:     2.131/ 1.597/ 1.597                   1.712/ 1.315/ 1.315                  4.151/ 3.787/ 3.787                  2.981/ 2.729/ 2.729                [ 1.21 s]
    Epoch 5/1000:     2.397/ 1.779/ 1.779                   1.928/ 1.378/ 1.378                  3.795/ 3.661/ 3.661                  2.720/ 2.642/ 2.642                [ 1.22 s]
    Epoch 6/1000:     2.327/ 1.317/ 1.317                   1.794/ 1.040/ 1.040                  3.571/ 3.323/ 3.323                  2.585/ 2.443/ 2.443                [ 1.22 s]
    Epoch 7/1000:     2.022/ 2.345/ 1.317                   1.621/ 2.092/ 1.040                  3.255/ 3.185/ 3.323                  2.353/ 2.339/ 2.443                [ 1.16 s]
    Epoch 8/1000:     1.871/ 1.239/ 1.239                   1.513/ 1.020/ 1.020                  3.188/ 2.981/ 2.981                  2.297/ 2.194/ 2.194                [ 1.19 s]
    Epoch 9/1000:     1.711/ 1.822/ 1.239                   1.386/ 1.598/ 1.020                  3.033/ 3.062/ 2.981                  2.205/ 2.251/ 2.194                [ 1.16 s]
    Epoch 10/1000:    1.697/ 1.133/ 1.133                   1.366/ 0.906/ 0.906                  2.857/ 2.676/ 2.676                  2.069/ 1.978/ 1.978                [ 1.22 s]

    ...

    Epoch 990/1000:   0.083/ 0.108/ 0.099                   0.066/ 0.077/ 0.071                  0.260/ 0.477/ 0.475                  0.198/ 0.323/ 0.322                [ 1.20 s]
    Epoch 991/1000:   0.084/ 0.105/ 0.099                   0.067/ 0.076/ 0.071                  0.260/ 0.476/ 0.475                  0.198/ 0.322/ 0.322                [ 1.17 s]
    Epoch 992/1000:   0.083/ 0.106/ 0.099                   0.065/ 0.075/ 0.071                  0.260/ 0.476/ 0.475                  0.198/ 0.322/ 0.322                [ 1.22 s]
    Epoch 993/1000:   0.082/ 0.101/ 0.099                   0.065/ 0.073/ 0.071                  0.259/ 0.476/ 0.475                  0.198/ 0.322/ 0.322                [ 1.21 s]
    Epoch 994/1000:   0.083/ 0.100/ 0.099                   0.065/ 0.072/ 0.071                  0.259/ 0.475/ 0.475                  0.198/ 0.322/ 0.322                [ 1.20 s]
    Epoch 995/1000:   0.083/ 0.111/ 0.099                   0.066/ 0.079/ 0.071                  0.259/ 0.475/ 0.475                  0.198/ 0.322/ 0.322                [ 1.20 s]
    Epoch 996/1000:   0.084/ 0.100/ 0.099                   0.066/ 0.073/ 0.071                  0.259/ 0.475/ 0.475                  0.198/ 0.322/ 0.322                [ 1.16 s]
    Epoch 997/1000:   0.082/ 0.101/ 0.099                   0.065/ 0.072/ 0.071                  0.258/ 0.475/ 0.475                  0.197/ 0.322/ 0.322                [ 1.20 s]
    Epoch 998/1000:   0.082/ 0.101/ 0.099                   0.064/ 0.072/ 0.071                  0.258/ 0.474/ 0.475                  0.197/ 0.321/ 0.322                [ 1.22 s]
    Epoch 999/1000:   0.082/ 0.100/ 0.099                   0.064/ 0.072/ 0.071                  0.258/ 0.474/ 0.475                  0.197/ 0.322/ 0.322                [ 1.18 s]
    Epoch 1000/1000:  0.082/ 0.101/ 0.099                   0.065/ 0.072/ 0.071                  0.258/ 0.474/ 0.475                  0.197/ 0.321/ 0.322                [ 1.24 s]
    ==============================================================================================================================================================================
    Timing report
    -------------
    Total time                    ............. 1186.234213590622
    Best model report
    -----------------
    Best epochs from the training  ............. [987, 999, 998]
    ====================================================================================================
    Results of 3 models obtained on the test data:

                        Energy-RMSE         Energy-MAE          Force-RMSE          Force-MAE
    ====================================================================================================
    Ensemble:            0.10588             0.07919             0.45645             0.31880
    Mean individual:     0.12112             0.09048             0.55938             0.38830
    Model 1:             0.10838             0.08180             0.51339             0.36123
    Model 2:             0.13819             0.10222             0.63681             0.43161
    Model 3:             0.11679             0.08742             0.52794             0.37206
    ====================================================================================================


At the end of the `train.out` file, one can find MAE and RMSE evaluated on the test data that have not been seen during
training procedure and are presented for single models and an ensemble if the latter was employed.

.. note::
    If no test data is available, make sure to exclude the last line in `train.py`, i.e.

    .. highlight:: python
    .. code-block:: python

        trainer.eval()

    Otherwise, an error will arise.

All models in this tutorial were trained on an NVIDIA GeForce RTX 2080Ti 11 GB GPU.

Running Molecular Dynamics with ASE
___________________________________

Here, we show a minimal example of the application of a GM-NN model for molecular dynamics (MD) simulations employing
the Atomic Simulation Environment (`ASE <https://wiki.fysik.dtu.dk/ase/>`_) package [2]. Note that other molecular
simulations, such as geometry optimization, vibrational analysis, etc., are possible and can be conducted analogous
to this example, see `ASE <https://wiki.fysik.dtu.dk/ase/>`_. To perform an MD simulation in a canonical (NVT)
statistical ensemble, we will use the `ethanol_md.py` script

.. literalinclude:: ../../../examples/ethanol_md.py
   :encoding: latin-1

Using the MD path contained in `ethanol.traj`, one can compute, e.g., the vibrational power spectrum by computing the
Fourier transform of the velocity autocorrelation function

.. math::

   C_{\mathbf{v}\mathbf{v}} \left( t \right) = \frac{1}{3 N_\mathrm{at}} \sum_{i=1}^{N_\mathrm{at}}\sum_{j =1}^3 \frac{\langle v_{ij}\left(0\right) v_{ij}\left(t\right)\rangle}{\langle v_{ij}\left(0\right) v_{ij}\left(0\right)\rangle}

An example of the respective computation is shown in `ethanol_spectrum.py`

.. literalinclude:: ../../../examples/ethanol_spectrum.py
   :encoding: latin-1

The obtained power spectrum is shown below

.. _fig-power-spectrum:
.. figure:: fft_spectrum.png
   :width: 600px
   :align: center

.. note::
    The presented results are only an example calculation, and, certainly, more smooth spectra can be obtained by
    applying, e.g., a half-Gauss window to the autocorrelation function.

References
__________

* [1] V.  Zaverkin  and  J.  Kästner,  “Gaussian  moments  as  physically  inspired molecular  descriptors  for accurate  and  scalable  machine  learning  potentials,” J. Chem. Theory Comput. **16**, 5410–5421 (2020).
* [2] A. H. Larsen `et al.`, “The atomic simulation environment—a python library for working with atoms,” J. Phys. Condens. Matter **29**, 273002 (2017).