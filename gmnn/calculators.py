import os
import numpy as np

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import argparse
from gmnn.utils import *
from gmnn.layers import *
from gmnn.neighborlist import *
from ase import units


class NNCalculator:
    def __init__(self, model_path: str, config: str, use_all_features: bool = True, device_number: int = 0):
        # parameters
        self.model_path = model_path
        self.config = config
        self.use_all_features = use_all_features
        self.device_number = device_number

        # limit memory growth
        memory_growth()

        # load parameters
        self.args = self._load_parameters()
        self.args.use_all_features = use_all_features

        # read hidden architecture
        self.args.architecture = [int(item) for item in self.args.architecture.split(',')]

        # compute number of invariant features from n_radial
        if bool(self.args.use_all_features):
            n_features = int(self.args.n_radial + 3 * self.args.n_radial * (self.args.n_radial + 1) / 2 \
                             + self.args.n_radial * (self.args.n_radial + 1) * (
                                     self.args.n_radial + 2) / 6 + self.args.n_radial ** 3 \
                             + 2 * self.args.n_radial ** 2 * (self.args.n_radial + 1) / 2)
        else:
            n_features = int(self.args.n_radial + 3 * self.args.n_radial * (self.args.n_radial + 1) / 2 \
                             + 4 * self.args.n_radial * (self.args.n_radial + 1) * (self.args.n_radial + 2) / 6)
        # define architecture
        self.args.architecture = [n_features] + self.args.architecture + [1]

        # define activation function
        if self.args.activation == "softplus":
            self.activation = tf.nn.softplus
        elif self.args.activation == "swish":
            self.activation = lambda x: 1.6765324703310907 * tf.nn.swish(x)
        else:
            raise NotImplementedError()

        # define list of models
        self.models = []
        for i in range(self.args.n_models):
            # define layers
            layers = [GaussianMomentsLayer(n_radial=self.args.n_radial, n_contr=8, n_basis=self.args.n_basis,
                                           r_max=self.args.cutoff, r_min=self.args.r_min, emb_init=self.args.emb_init,
                                           use_all_features=bool(self.args.use_all_features))]
            if bool(self.args.batch_norm):
                layers.append(
                    CustomBatchNorm(n_features=self.args.architecture[0],
                                    norm_scale_factor=self.args.norm_scale_factor,
                                    bn_momentum=self.args.bn_momentum))
            for j in range(len(self.args.architecture) - 1):
                layers.append(
                    LinearLayer(in_features=self.args.architecture[j], out_features=self.args.architecture[j + 1],
                                init_normalization=self.args.init_normalization,
                                weight_init_mode=self.args.weight_init_mode,
                                bias_init_mode=self.args.bias_init_mode,
                                bias_factor=self.args.bias_factor))
                if j < len(self.args.architecture) - 2:
                    layers.append(ActivationLayer(self.activation))
            # scale and shift model
            model = self._scale_shift_model(layers)
            # append models
            self.models.append(model)

        # load best models
        for i in range(len(self.models)):
            ckpt = tf.train.Checkpoint(epoch=tf.Variable(0), model=self.models[i])
            best_dir = os.path.join(self.model_path, str(i), 'best')
            ckpt_manager_best = tf.train.CheckpointManager(ckpt, best_dir, max_to_keep=10)
            ckpt.restore(ckpt_manager_best.latest_checkpoint).expect_partial()

    def _load_parameters(self) -> argparse.Namespace:
        parser = argparse.ArgumentParser(description='Parameters', fromfile_prefix_chars='@')

        parser.add_argument("--cutoff", type=float)
        parser.add_argument("--r_min", type=float)
        parser.add_argument("--n_models", type=int)
        parser.add_argument("--architecture", type=str)
        parser.add_argument("--weight_init_mode", type=str)
        parser.add_argument("--bias_init_mode", type=str)
        parser.add_argument("--activation", type=str)
        parser.add_argument("--batch_norm", type=int)
        parser.add_argument("--bn_momentum", type=float)
        parser.add_argument("--norm_scale_factor", type=float)
        parser.add_argument("--init_normalization", type=str)
        parser.add_argument("--weight_factor", type=float)
        parser.add_argument("--bias_factor", type=float)
        parser.add_argument("--n_radial", type=int)
        parser.add_argument("--n_basis", type=int)
        parser.add_argument("--emb_init", type=str)
        parser.add_argument("--use_all_features", type=int)

        config_md = self.model_path + '/' + self.config
        args, unknown = parser.parse_known_args(['@' + config_md])
        return args

    def _scale_shift_model(self, layers):
        raise NotImplementedError()

    def get_potential_energy(self, atoms: Atoms, force_consistent: bool = False) -> float:
        raise NotImplementedError()

    def get_forces(self, atoms: Atoms) -> np.ndarray:
        raise NotImplementedError()


class ASECalculator(NNCalculator):
    """Calculator class for computing and updating energies, forces, and virial stresses of the simulated system.

    :param atoms: Atomistic system.
    :param model_path: Trained model.
    :param config: Configure file.
    :param use_all_features: Set ``True`` to use all features. Set ``False`` to use only upper-triangular features.
    :param wrap_positions: Set ``True`` if positions of atoms need to be wrapped back to the cell.
    :param compute_stress: Set ``True`` if virial stresses have to be computed.
    :param skin: If ``neighbor_list=True``, compute all neighbors within ``cutoff+skin``.
    :param device_number: GPU index (if GPU is available).
    :param set_units: Specify units: ``'kcal/mol to eV'``, ``'Hartree to eV'``, ``'eV to kcal/mol'``,
                      ``'eV to Hartree'``, ``'kcal/mol to Hartree'``, ``'eV to eV'``.
    :param dtype: Set floating point type (e.g. tf.float32).

    Example:

    .. highlight:: python
    .. code-block:: python

        from ase.io import read
        from gmnn.calculators import ASECalculator


        if __name__ == '__main__':

            # read input file
            atoms = read("ethanol.xyz")

            # setup calculator
            calc = ASECalculator(atoms=atoms, model_path="models/ethanol/test_5", device_number=0, use_all_features=True,
                                 wrap_positions=False, compute_stress=False, set_units="kcal/mol to eV",
                                  config="pes_training.txt")

            atoms.set_calculator(calc)

            print(atoms.get_potential_energy())
            print(atoms.get_forces())
            print(calc.get_frequencies(atoms))
    """
    def __init__(self, atoms: Atoms, model_path: str, config: str = "pes_training.txt", use_all_features: bool = True,
                 device_number: int = 0, wrap_positions: bool = False, skin: float = 0.0, compute_stress: bool = False,
                 set_units: str = 'kcal/mol -> eV', dtype=tf.float32):
        super().__init__(model_path=model_path, config=config, use_all_features=use_all_features,
                         device_number=device_number)
        self.skin = skin
        self.wrap_positions = wrap_positions
        self.nl = NeighborListTF(cutoff=self.args.cutoff + self.skin, wrap_positions=self.wrap_positions)
        self.compute_stress = compute_stress
        self.pbc = any(atoms.get_pbc())
        self._set_units(set_units)
        self.config = config
        self.dtype = dtype

        self.last_atoms = atoms.copy()
        self.update_nl = atoms.copy()   # used to update the neighborlist employing skin radius
        self._build_neighbors(atoms)
        self._calculate_all_properties(atoms)

    def _scale_shift_model(self, layers):
        return ScaleShiftModel(Sequential(layers), atomic_energy_std=1.0, atomic_energy_regression=np.zeros(119))

    def _set_units(self, set_units: str):
        if set_units == 'kcal/mol to eV':
            self.rescale_units = units.kcal / units.mol
        elif set_units == 'Hartree to eV':
            self.rescale_units = units.Hartree
        elif set_units == 'eV to kcal/mol':
            self.rescale_units = units.mol / units.kcal
        elif set_units == 'eV to Hartree':
            self.rescale_units = 1.0 / units.Hartree
        elif set_units == 'kcal/mol to Hartree':
            self.rescale_units = units.kcal / units.mol / units.Hartree
        elif set_units == 'eV to eV':
            self.rescale_units = units.eV
        else:
            raise NotImplementedError()

    def _calculation_required(self, atoms, quantities=None):
        return atoms != self.last_atoms

    @tf.function(input_signature=[
        tf.TensorSpec([None, ], dtype=tf.int32),                # data['Z']
        tf.TensorSpec([None, None], dtype=tf.float32),          # data['R']
        tf.TensorSpec([None, None, None], dtype=tf.float32),    # data['C']
        tf.TensorSpec([None, ], dtype=tf.int32),                # data['idx_i']
        tf.TensorSpec([None, ], dtype=tf.int32),                # data['idx_j']
        tf.TensorSpec([None, None], dtype=tf.float32),          # data['offsets']
        tf.TensorSpec([None, ], dtype=tf.int32)                 # data['batch_seg']
    ])
    def _inference(self, Z, R, C, idx_i, idx_j, offsets, batch_seg):
        E_bp_pred = []
        F_at_pred = []
        W_at_pred = []
        for model in self.models:
            with tf.GradientTape(watch_accessed_variables=False) as force_tape:
                force_tape.watch(R)
                R_ij = get_distance_vectors(R, C, idx_i, idx_j, offsets, batch_seg, pbc=self.pbc)
                if self.pbc and self.compute_stress:
                    with tf.GradientTape(watch_accessed_variables=False) as stress_tape:
                        stress_tape.watch(R_ij)
                        x = (R_ij, Z, idx_i, idx_j)
                        E_at_pred = tf.cast(model(x, training=False), dtype=self.dtype)
                        E_bp_pred.append(tf.reduce_sum(E_at_pred))
                        E_total = tf.reduce_sum(E_at_pred)
                    F_ij_pred = tf.convert_to_tensor(stress_tape.gradient(E_total, R_ij))
                    W_at_pred.append(- 1.0 * tf.reduce_sum(F_ij_pred[:, None, :] * R_ij[:, :, None], 0)
                                     / tf.abs(tf.linalg.det(C)))
                else:
                    x = (R_ij, Z, idx_i, idx_j)
                    E_at_pred = tf.cast(model(x, training=False), dtype=self.dtype)
                    E_bp_pred.append(tf.reduce_sum(E_at_pred))
                    E_total = tf.reduce_sum(E_at_pred)
            F_at_pred.append(-tf.convert_to_tensor(force_tape.gradient(E_total, R)))

        # mean force used to propagate the structure
        F_at_ens = sum(F_at_pred) / len(self.models)
        E_bp_ens = sum(E_bp_pred) / len(self.models)
        if self.pbc:
            W_at_ens = sum(W_at_pred) / len(self.models)

        # force variance to check the accuracy of the potential
        F_at_var = tf.math.reduce_std(F_at_pred, axis=0) ** 2
        E_bp_var = tf.math.reduce_std(E_bp_pred, axis=0) ** 2
        if self.pbc:
            W_at_var = tf.math.reduce_std(W_at_pred, axis=0) ** 2
        
        if self.pbc and self.compute_stress:
            results = {"energy_mean": E_bp_ens,
                       "energy_var": E_bp_var,
                       "force_mean": F_at_ens,
                       "force_var": F_at_var,
                       "stress_mean": W_at_ens,
                       "stress_var": W_at_var}
        else:
            results = {"energy_mean": E_bp_ens,
                       "energy_var": E_bp_var,
                       "force_mean": F_at_ens,
                       "force_var": F_at_var}

        return results

    @tf.function(input_signature=[
        tf.TensorSpec([None, ], dtype=tf.int32),                # data['Z']
        tf.TensorSpec([None, None], dtype=tf.float32),          # data['R']
        tf.TensorSpec([None, None, None], dtype=tf.float32),    # data['C']
        tf.TensorSpec([None, ], dtype=tf.int32),                # data['idx_i']
        tf.TensorSpec([None, ], dtype=tf.int32),                # data['idx_j']
        tf.TensorSpec([None, None], dtype=tf.float32),          # data['offsets']
        tf.TensorSpec([None, ], dtype=tf.int32)                 # data['batch_seg']
    ])
    def _hessian(self, Z, R, C, idx_i, idx_j, offsets, batch_seg):
        H_pred = []
        for model in self.models:
            with tf.GradientTape(watch_accessed_variables=False, persistent=True) as hessian_tape:
                hessian_tape.watch(R)
                with tf.GradientTape(watch_accessed_variables=False) as force_tape:
                    force_tape.watch(R)
                    R_ij = get_distance_vectors(R, C, idx_i, idx_j, offsets, batch_seg, pbc=self.pbc)
                    x = (R_ij, Z, idx_i, idx_j)
                    E_at_pred = tf.cast(model(x, training=False), dtype=self.dtype)
                    E_total = tf.reduce_sum(E_at_pred)
                G_pred = tf.convert_to_tensor(force_tape.gradient(E_total, R))
            H_pred.append(tf.convert_to_tensor(hessian_tape.jacobian(G_pred, R, experimental_use_pfor=False)))

        # mean hessian
        H_ens = sum(H_pred) / len(self.models)

        # hessian variance to check the accuracy
        H_var = tf.math.reduce_std(H_pred, axis=0) ** 2

        results = {"hessian_mean": H_ens,
                   "hessian_var": H_var}

        return results

    def _update_neighbors(self, atoms: Atoms):
        if ((self.update_nl.get_pbc() != atoms.get_pbc()).any() or
                (self.update_nl.get_cell() != atoms.get_cell()).any() or
                (((self.update_nl.get_positions(wrap=self.wrap_positions)
                  - atoms.get_positions(wrap=self.wrap_positions)) ** 2).sum(1)).max() > self.skin ** 2 / 4.0):
            self._build_neighbors(atoms)
            self.update_nl = atoms.copy()

    def _build_neighbors(self, atoms: Atoms):
        self.idx_i, self.idx_j, self.offsets = self.nl.neighbor_list(atoms)

    def _calculate_all_properties(self, atoms: Atoms):
        self._update_neighbors(atoms)

        batch_seg = np.zeros(len(atoms), dtype=np.int64)
        # move everything to TensorFlow
        Z = tf.convert_to_tensor(atoms.get_atomic_numbers(), dtype=tf.int32)
        R = tf.convert_to_tensor(atoms.get_positions(wrap=self.wrap_positions), dtype=tf.float32)
        C = tf.convert_to_tensor(atoms.get_cell()[None, :, :], dtype=tf.float32)
        idx_i = tf.convert_to_tensor(self.idx_i, dtype=tf.int32)
        idx_j = tf.convert_to_tensor(self.idx_j, dtype=tf.int32)
        offsets = tf.convert_to_tensor(self.offsets, dtype=tf.float32)
        batch_seg = tf.convert_to_tensor(batch_seg, dtype=tf.int32)

        self.results = self._inference(Z, R, C, idx_i, idx_j, offsets, batch_seg)
        self.last_atoms = atoms.copy()

    def _calculate_hessian(self, atoms: Atoms):
        self._update_neighbors(atoms)

        batch_seg = np.zeros(len(atoms), dtype=np.int64)
        # move everything to TensorFlow
        Z = tf.convert_to_tensor(atoms.get_atomic_numbers(), dtype=tf.int32)
        R = tf.convert_to_tensor(atoms.get_positions(wrap=self.wrap_positions), dtype=tf.float32)
        C = tf.convert_to_tensor(atoms.get_cell()[None, :, :], dtype=tf.float32)
        idx_i = tf.convert_to_tensor(self.idx_i, dtype=tf.int32)
        idx_j = tf.convert_to_tensor(self.idx_j, dtype=tf.int32)
        offsets = tf.convert_to_tensor(self.offsets, dtype=tf.float32)
        batch_seg = tf.convert_to_tensor(batch_seg, dtype=tf.int32)

        self.results_hessian = self._hessian(Z, R, C, idx_i, idx_j, offsets, batch_seg)
        self.last_atoms = atoms.copy()

    def get_potential_energy(self, atoms: Atoms, force_consistent: bool = False) -> float:
        """
        :param atoms: Atomistic system.
        :return: Potential energy of the simulated system.
        """
        if self._calculation_required(atoms):
            self._calculate_all_properties(atoms)
        return self.results["energy_mean"].numpy() * self.rescale_units

    def get_forces(self, atoms: Atoms) -> np.ndarray:
        """
        :param atoms: Atomistic system.
        :return: Atomic forces of the simulated system.
        """
        if self._calculation_required(atoms):
            self._calculate_all_properties(atoms)
        return self.results["force_mean"].numpy() * self.rescale_units
    
    def get_stress(self, atoms: Atoms) -> np.ndarray:
        """
        :param atoms: Atomistic system.
        :return: Virial stress of the simulated system.
        """
        if self._calculation_required(atoms):
            self._calculate_all_properties(atoms)
        return - 1.0 * self.results["stress_mean"].numpy() * self.rescale_units

    def get_hessian(self, atoms: Atoms) -> np.ndarray:
        """
        :param atoms: Atomistic system.
        :return: Hessian matrix of the simulated system.
        """
        n_params = tf.reduce_prod(atoms.get_positions().shape)
        self._calculate_hessian(atoms)
        return tf.reshape(self.results_hessian["hessian_mean"], [n_params, n_params]).numpy() * self.rescale_units
    
    def get_frequencies(self, atoms: Atoms) -> Tuple[np.ndarray, np.ndarray]:
        """
        :param atoms: Atomistic system.
        :return: Harmonic vibrational frequencies and modes of the simulated system.
        """
        # masses has shape n_at x 3
        masses = tf.tile(tf.cast(atoms.get_masses(), dtype=self.dtype)[:, None], [1, 3])
        # reshape and tensorprod results in shape 3 * n_at x 3 * n_at
        masses = tf.reshape(masses, -1)[:, None] * tf.reshape(masses, -1)[None, :]
        # hessian has shape 3 * n_at x 3 * n_at
        hessian = self.get_hessian(atoms)
        # divide hessian by masses
        weighted_hessian = hessian / tf.sqrt(masses)
        # compute eigenvalues
        w, v = tf.linalg.eigh( weighted_hessian )
        # conversion factor to inverse centimeters
        s = units._hbar * 1e10 / np.sqrt(units._e * units._amu) / units.invcm
        return tf.sqrt(tf.cast(w, dtype=tf.complex128)).numpy() * s, v.numpy()
    
    def get_energy_variance(self, atoms: Atoms) -> float:
        """
        :param atoms: Atomistic system.
        :return: Variance of potential energy of the simulated system.
        """
        if self._calculation_required(atoms):
            self._calculate_all_properties(atoms)
        return self.results["energy_var"].numpy() * self.rescale_units ** 2

    def get_force_variance(self, atoms: Atoms) -> np.ndarray:
        """
        :param atoms: Atomistic system.
        :return: Variance of atomic forces of the simulated system.
        """
        if self._calculation_required(atoms):
            self._calculate_all_properties(atoms)
        return self.results["force_var"].numpy() * self.rescale_units ** 2

    def get_stress_variance(self, atoms: Atoms) -> np.ndarray:
        """
        :param atoms: Atomistic system.
        :return: Variance of virial stress of the simulated system.
        """
        if self._calculation_required(atoms):
            self._calculate_all_properties(atoms)
        return self.results["stress_var"].numpy() * self.rescale_units ** 2

    def get_hessian_variance(self, atoms: Atoms) -> np.ndarray:
        """
        :param atoms: Atomistic system.
        :return: Variance of Hessian matrix of the simulated system.
        """
        n_params = tf.reduce_prod(atoms.get_positions().shape)
        self._calculate_hessian(atoms)
        return tf.reshape(self.results_hessian["hessian_var"], [n_params, n_params]).numpy() * self.rescale_units ** 2


class MATCalculator(NNCalculator):
    """Calculator class for computing and updating magnetic anisotropy tensors of the simulated system.

    :param atoms: Atomistic system.
    :param model_path: Trained model.
    :param config: Configure file.
    :param use_all_features: Set ``True`` to use all features. Set ``False`` to use only upper-triangular features.
    :param device_number: GPU index (if GPU is available).

    Example:

    .. highlight:: python
    .. code-block:: python

        from ase.io import read, write
        from gmnn.calculators import MATCalculator


        if __name__ == '__main__':

            # read input file
            atoms = read("strct.xyz")

            # setup calculator
            calc = MATCalculator(atoms=atoms, model_path="models/cosar/test_2", config="mat_training.txt",
                                 device_number=0, use_all_features=True)

            print(calc.get_mat(atoms))
    """
    def __init__(self, atoms: Atoms, model_path: str, config: str, use_all_features: bool = True,
                 device_number: int = 0):
        super().__init__(model_path=model_path, config=config, use_all_features=use_all_features,
                         device_number=device_number)
        self.pbc = any(atoms.get_pbc())
        self.nl = NeighborListTF(cutoff=self.args.cutoff)

        # indices for symmetric & traceless tensor
        self.indices = np.stack(np.triu_indices(3, 0, 3), 0)
        self.indices = tf.constant(
            [[self.indices[0, i], self.indices[1, i]] for i in range(len(self.indices[0]) - 1)], dtype=tf.int32)

        self.last_atoms = atoms.copy()
        self._build_neighbors(atoms)
        self._calculate_all_properties(atoms)

    def _scale_shift_model(self, layers):
        return ScaleShiftTensor(Sequential(layers), tensor_std=1.0, tensor_mean=0.0)

    def _calculation_required(self, atoms, quantities=None):
        return atoms != self.last_atoms

    def _build_neighbors(self, atoms: Atoms):
        self.idx_i, self.idx_j, self.offsets = self.nl.neighbor_list(atoms)

    @tf.function(input_signature=[
        tf.TensorSpec([None, ], dtype=tf.int32),                # data['Z']
        tf.TensorSpec([None, None], dtype=tf.float32),          # data['R']
        tf.TensorSpec([None, None, None], dtype=tf.float32),    # data['C']
        tf.TensorSpec([None, ], dtype=tf.int32),                # data['idx_i']
        tf.TensorSpec([None, ], dtype=tf.int32),                # data['idx_j']
        tf.TensorSpec([None, None], dtype=tf.float32),          # data['offsets']
        tf.TensorSpec([None, ], dtype=tf.int32)                 # data['batch_seg']
    ])
    def _inference(self, Z, R, C, idx_i, idx_j, offsets, batch_seg):
        # precompute the symmetrized part of the tensor
        # R_RT has shape n_atoms x 3 x 3
        R_c = R - tf.gather(tf.math.segment_mean(R, batch_seg), batch_seg)
        R_RT = R_c[:, :, None] * R_c[:, None, :]
        # normalize
        R_RT = R_RT / (tf.reduce_sum(R_c ** 2, -1)[:, None, None] + 1e-12)
        # R_sq has shape n_atoms x 3 x 3
        R_sq = tf.eye(3)
        # S_ij = 3 * R_IJ (x) R_IJ - R^2 * delta_IJ
        # sym_values has shape n_atoms x 5
        sym_values = tf.transpose(tf.gather_nd(tf.transpose(3.0 * R_RT - R_sq, perm=(1, 2, 0)), self.indices))

        MAT_pred = []
        MAT_grad = []
        for model in self.models:
            with tf.GradientTape(watch_accessed_variables=False) as grad_tape:
                grad_tape.watch(R)
                R_ij = get_distance_vectors(R, C, idx_i, idx_j, offsets, batch_seg, pbc=self.pbc)
                x = (R_ij, Z, idx_i, idx_j)
                atomic_property = model(x, training=False)
                MAT_total = tf.math.segment_sum(atomic_property * sym_values, batch_seg)
                MAT_pred.append(MAT_total)
            MAT_grad.append(tf.convert_to_tensor(grad_tape.jacobian(MAT_total, R, experimental_use_pfor=False)))

        MAT_ens = sum(MAT_pred) / len(self.models)
        MAT_var = tf.math.reduce_std(MAT_pred, axis=0) ** 2

        MAT_grad_ens = sum(MAT_grad) / len(self.models)
        MAT_grad_var = tf.math.reduce_std(MAT_grad, axis=0) ** 2

        results = {"MAT_mean": MAT_ens,
                   "MAT_var": MAT_var,
                   "MAT_grad_mean": MAT_grad_ens,
                   "MAT_grad_var": MAT_grad_var}

        return results

    def _calculate_all_properties(self, atoms: Atoms):
        self._build_neighbors(atoms)

        batch_seg = np.zeros(len(atoms), dtype=np.int64)
        # move everything to TensorFlow
        Z = tf.convert_to_tensor(atoms.get_atomic_numbers(), dtype=tf.int32)
        R = tf.convert_to_tensor(atoms.get_positions(), dtype=tf.float32)
        C = tf.convert_to_tensor(atoms.get_cell()[None, :, :], dtype=tf.float32)
        idx_i = tf.convert_to_tensor(self.idx_i, dtype=tf.int32)
        idx_j = tf.convert_to_tensor(self.idx_j, dtype=tf.int32)
        offsets = tf.convert_to_tensor(self.offsets, dtype=tf.float32)
        batch_seg = tf.convert_to_tensor(batch_seg, dtype=tf.int32)

        self.results = self._inference(Z, R, C, idx_i, idx_j, offsets, batch_seg)
        self.last_atoms = atoms.copy()

    def get_mat(self, atoms: Atoms) -> np.ndarray:
        """
        :param atoms: Atomistic system.
        :return: Magnetic anisotropy tensor of the simulated system.
        """
        if self._calculation_required(atoms):
            self._calculate_all_properties(atoms)
        return self.results["MAT_mean"].numpy()[0]

    def get_mat_grad(self, atoms: Atoms) -> np.ndarray:
        """
        :param atoms: Atomistic system.
        :return: Magnetic anisotropy tensor of simulated system.
        """
        if self._calculation_required(atoms):
            self._calculate_all_properties(atoms)
        return self.results["MAT_grad_mean"].numpy()[0]

    def get_mat_variance(self, atoms: Atoms) -> np.ndarray:
        """
        :param atoms: Atomistic system.
        :return: Variance of the magnetic anisotropy tensor of the simulated system.
        """
        if self._calculation_required(atoms):
            self._calculate_all_properties(atoms)
        return self.results["MAT_var"].numpy()[0]

    def get_mat_grad_variance(self, atoms: Atoms) -> np.ndarray:
        """
        :param atoms: Atomistic system.
        :return: Variance of magnetic anisotropy tensor of simulated system.
        """
        if self._calculation_required(atoms):
            self._calculate_all_properties(atoms)
        return self.results["MAT_grad_var"].numpy()[0]


class TICalculator:
    def __init__(self, atoms: Atoms, nn_calc: NNCalculator, hessian: str, lam: float,
                 wrap_positions: bool = False):
        self.wrap_positions = wrap_positions
        self.pbc = any(atoms.get_pbc())
        self.nn_calc = nn_calc
        self.hessian = np.loadtxt(hessian) * units.Hartree / 0.529177 ** 2

        self.eq_positions = atoms.get_positions()
        self.lam = lam

        self.nn_force = None
        self.nn_energy = None

        self.ref_force = None
        self.ref_energy = None

        self.last_atoms = atoms.copy()
        self._calculate_all_properties(atoms)

    def _calculation_required(self, atoms, quantities=None):
        return atoms != self.last_atoms

    def _get_displacement(self, atoms: Atoms):
        # compute distance
        u = atoms.get_positions(wrap=self.wrap_positions) - self.eq_positions
        # get diagonal part of the cell
        cell = tf.linalg.diag_part(atoms.get_cell())
        # apply the minimum image convention
        u -= tf.math.rint(u / cell) * cell
        return u

    def _calculate_all_properties(self, atoms: Atoms):
        # compute network output
        self.nn_force = self.nn_calc.get_forces(atoms)
        self.nn_energy = self.nn_calc.get_potential_energy(atoms)
        # compute harmonic output
        u = tf.reshape(self._get_displacement(atoms), shape=-1)
        self.ref_force = - tf.reshape(tf.linalg.matvec(self.hessian, u), shape=(len(atoms), -1))
        self.ref_energy = 0.5 * tf.reduce_sum(u * tf.linalg.matvec(self.hessian, u))

        self.last_atoms = atoms.copy()

    def get_potential_energy(self, atoms: Atoms, force_consistent: bool = False) -> float:
        if self._calculation_required(atoms):
            self._calculate_all_properties(atoms)
        return self.lam * self.nn_energy + (1. - self.lam) * self.ref_energy.numpy()

    def get_forces(self, atoms: Atoms) -> np.ndarray:
        if self._calculation_required(atoms):
            self._calculate_all_properties(atoms)
        return self.lam * self.nn_force + (1. - self.lam) * self.ref_force.numpy()

    def write_energy(self, atoms: Atoms) -> Tuple[float, float]:
        if self._calculation_required(atoms):
            self._calculate_all_properties(atoms)
        return self.nn_energy, self.ref_energy.numpy()




