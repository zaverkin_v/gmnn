.. _transfer_learning-tutorial:

=================
Transfer learning
=================

Fine-tuning from DFT to CCSD(T) level using molecular dynamics data sets
________________________________________________________________________

Data set preparation
********************

In this tutorial, we will show how to apply transfer learning to improve the performance of
our models on the target task, e.g., CCSD(T) labels, by pre-training on the source task, e.g.,
DFT labels. Thus, we will require two different data sets. As the source data set we will use the
`DFT ethanol data set <https://github.com/zaverkin/ethanol_datasets_git>`_ previously described in
:ref:`ethanol-tutorial`. As the target data set we will use the `CCSD(T) ethanol data set
<http://www.sgdml.org/#datasets>`_. Please download the corresponding data and store it in the `<data>`
directory. Before we start with training our models, we have to slightly change
the keys and the respective values in downloaded .npz files. For this purpose, run the following script:

.. literalinclude:: ../../../examples/convert_ethanol_data.py
   :encoding: latin-1

As an output you will obtained converted .npz data files which we can use in the following sections.

Pre-training and fine-tuning models
***********************************

In this section, we will define two config files, `pre-training.txt` and `fine-tuning.txt`, respectively.
The former defines the hyper-parameters for the models, the respective training parameters, and
reads, e.g.:

.. highlight:: none
.. code-block:: none

    --data_path=data/ethanol_datasets_git/ethanol_1000K-converted.npz
    --model_path=models/transfer-learning
    --model_name=pre-training
    --cutoff=4.0
    --n_train=1024
    --n_valid=200
    --data_seed=0
    --model_seed=0
    --forces=1
    --neighbor_list=0
    --n_models=3

The config file for fine-tuning our models on target data has two additional parameters `init_path`
and `fine_tune_lrs`. The former defines the model path used to initialize the parameters. The latter
defines the learning rates of the respective hidden layers, such that they are fine-tuned to a different
extend. The respective approach is referred to as discriminative fine-tuning and is frequently
used in the natural langauge processing domain [1]. We also fix the embedding learning rate, which we found
to perform better. The scale and shift learning rates can also be adjusted depending on the specific task. An
example for a `fine-tuning.txt` could be:

.. highlight:: none
.. code-block:: none

    --data_path=data/ethanol_ccsd_t/ethanol_ccsd_t-train-converted.npz
    --model_path=models/transfer-learning
    --model_name=fine-tuning
    --cutoff=4.0
    --n_train=128
    --n_valid=100
    --data_seed=0
    --model_seed=0
    --forces=1
    --neighbor_list=0
    --n_models=3
    --init_path=models/transfer-learning/pre-training
    --fine_tune_lrs=4e-4,2e-3,1e-2
    --scale_lr=0.001
    --shift_lr=0.05
    --emb_lr=0.0

To be able to see the improved sample efficiency of fine-tuned models, we train a model from
scratch, i.e., train it by employing CCSD(T) labels. The corresponding config file would be:

.. highlight:: none
.. code-block:: none

    --data_path=data/ethanol_ccsd_t/ethanol_ccsd_t-train-converted.npz
    --model_path=models/transfer-learning
    --model_name=from-scratch
    --cutoff=4.0
    --n_train=950
    --n_valid=50
    --valid_batch_size=50
    --data_seed=0
    --model_seed=0
    --forces=1
    --neighbor_list=0
    --n_models=3

Now we are ready to run our experiments. For this purpose we can use the following script
(`transfer-learning.py`):

.. highlight:: python
.. code-block:: python

    from gmnn.trainer import PESTrainer


    if __name__ == '__main__':
        # pre-train model on the source data set
        pre_training = PESTrainer(config='pre-training.txt')
        pre_training.fit()
        # uncomment if you wish to evaluate your model on the source test data
        #pre_training.eval()
        # or on the target test data
        pre_training.eval(test_data_path='data/ethanol_ccsd_t/ethanol_ccsd_t-test-converted.npz')

        # fine-tune parameters obtained in the previous step on target data set
        fine_tuning = PESTrainer(config='fine-tuning.txt')
        fine_tuning.fit()
        fine_tuning.eval(test_data_path='data/ethanol_ccsd_t/ethanol_ccsd_t-test-converted.npz')

        # train new models from scratch to compare with fine-tuned models
        from_scratch = PESTrainer(config='from-scratch.txt')
        from_scratch.fit()
        from_scratch.eval(test_data_path='data/ethanol_ccsd_t/ethanol_ccsd_t-test-converted.npz')

The pre-training results suggest that the DFT fails to predict CCSD(T) labels:

.. highlight:: none
.. code-block:: none

    Results of 3 models obtained on the test data:

                        Energy-RMSE         Energy-MAE          Force-RMSE          Force-MAE
    ====================================================================================================
    Ensemble:           60.30967            60.28936             8.32597             6.74574
    Mean individual:    60.30716            60.28683             8.32935             6.74832
    Model 1:            60.30161            60.28131             8.33228             6.75254
    Model 2:            60.30974            60.28948             8.32552             6.74663
    Model 3:            60.31011            60.28970             8.33024             6.74578
    ====================================================================================================

However, fine-tuning on 128 CCSD(T) energy and force labels leads to an accurate model:

.. highlight:: none
.. code-block:: none

    Results of 3 models obtained on the test data:

                        Energy-RMSE         Energy-MAE          Force-RMSE          Force-MAE
    ====================================================================================================
    Ensemble:            0.10881             0.08038             0.37554             0.26538
    Mean individual:     0.12581             0.09216             0.48198             0.33733
    Model 1:             0.11734             0.08699             0.46811             0.32621
    Model 2:             0.11160             0.08237             0.47826             0.33339
    Model 3:             0.14849             0.10710             0.49959             0.35240
    ====================================================================================================

with MAE and RMSE values close to the models trained on 950 structures from scratch:

.. highlight:: none
.. code-block:: none

    Results of 3 models obtained on the test data:

                        Energy-RMSE         Energy-MAE          Force-RMSE          Force-MAE
    ====================================================================================================
    Ensemble:            0.09521             0.06800             0.36389             0.25354
    Mean individual:     0.10563             0.07623             0.43285             0.29976
    Model 1:             0.10682             0.07520             0.44054             0.29915
    Model 2:             0.10669             0.08010             0.45240             0.31874
    Model 3:             0.10339             0.07339             0.40561             0.28137
    ====================================================================================================

Varying the respective config files different experiments can be performed, including those discussed
in the original publication [2].

Fine-tuning using general purpose ANI potentials
__________________________________________________

For this experiment we will need the pre-trained ANI general purpose potentials from
`here <https://doi.org/10.18419/darus-3299>`_.
Essentially, we may re-use all config files and Python scripts from the previous section, but
instead of pre-training on the DFT ethanol data set, we use the models pre-trained on ANI-1x data set.
For example, three of the pre-trained ANI models can be combined to a single ensemble model:

.. highlight:: none
.. code-block:: bash

    cp -r pre-training/seed_0_n_train_4194304/0 ensemble/0
    cp -r pre-training/seed_1_n_train_4194304/0 ensemble/1
    cp -r pre-training/seed_2_n_train_4194304/0 ensemble/2
    cp pre-training/seed_0_n_train_4194304/config.txt ensemble/

Please, change `n_models=1` to `n_models=3` in the above config file.

As an example, we perform fine-tuning using only energy labels and compare to the results obtained for
energy-only training from scratch. Note that one could use energy and force labels for fine-tuning, too.
Here, we have to define `fine-tuning-ani-train.txt` (with `forces=0`):

.. highlight:: none
.. code-block:: none

    --data_path=data/ethanol_ccsd_t/ethanol_ccsd_t-train-converted.npz
    --model_path=models/transfer-learning-ani
    --model_name=fine-tuning
    --cutoff=4.0
    --n_train=950
    --n_valid=50
    --valid_batch_size=50
    --data_seed=0
    --model_seed=0
    --forces=0
    --neighbor_list=0
    --n_models=3
    --init_path=models/transfer-learning-ani/pre-training/ensemble
    --fine_tune_lrs=4e-4,2e-3,1e-2
    --scale_lr=0.001
    --shift_lr=0.05
    --emb_lr=0.0

and `fine-tuning-ani-test.txt` (with `forces=1`):

.. highlight:: none
.. code-block:: none

    --data_path=data/ethanol_ccsd_t/ethanol_ccsd_t-train-converted.npz
    --model_path=models/transfer-learning-ani
    --model_name=fine-tuning
    --cutoff=4.0
    --n_train=950
    --n_valid=50
    --valid_batch_size=50
    --data_seed=0
    --model_seed=0
    --forces=1
    --neighbor_list=0
    --n_models=3
    --init_path=models/transfer-learning-ani/pre-training/ensemble
    --fine_tune_lrs=4e-4,2e-3,1e-2
    --scale_lr=0.001
    --shift_lr=0.05
    --emb_lr=0.0

The models trained from-scratch require two config files, too.
The `from-scratch-energy-train.txt` reads

.. highlight:: none
.. code-block:: none

    --data_path=data/ethanol_ccsd_t/ethanol_ccsd_t-train-converted.npz
    --model_path=models/transfer-learning-ani
    --model_name=from-scratch
    --cutoff=4.0
    --n_train=950
    --n_valid=50
    --valid_batch_size=50
    --data_seed=0
    --model_seed=0
    --forces=0
    --neighbor_list=0
    --n_models=3

and the `from-scratch-energy-test.txt` is

.. highlight:: none
.. code-block:: none

    --data_path=data/ethanol_ccsd_t/ethanol_ccsd_t-train-converted.npz
    --model_path=models/transfer-learning-ani
    --model_name=from-scratch
    --cutoff=4.0
    --n_train=950
    --n_valid=50
    --valid_batch_size=50
    --data_seed=0
    --model_seed=0
    --forces=1
    --neighbor_list=0
    --n_models=3

The corresponding Python script `transfer-learning-ani.py` reads

.. highlight:: none
.. code-block:: python

    from gmnn.trainer import PESTrainer


    if __name__ == '__main__':

        # fine-tune parameters obtained in the pre-training step on target data set
        fine_tuning_train = PESTrainer(config='fine-tuning-ani-train.txt')
        fine_tuning_train.fit()

        # run the test with forces=1
        fine_tuning_test = PESTrainer(config='fine-tuning-ani-test.txt')
        fine_tuning_test.eval(test_data_path='data/ethanol_ccsd_t/ethanol_ccsd_t-test-converted.npz')

        # train new models from scratch to compare with fine-tuned models
        from_scratch_train = PESTrainer(config='from-scratch-energy-train.txt')
        from_scratch_train.fit()

        # run the test with forces=1
        from_scratch_test = PESTrainer(config='from-scratch-energy-test.txt')
        from_scratch_test.eval(test_data_path='data/ethanol_ccsd_t/ethanol_ccsd_t-test-converted.npz')

The results obtained by fine-tuning are

.. highlight:: none
.. code-block:: none

    Results of 3 models obtained on the test data:

                        Energy-RMSE         Energy-MAE          Force-RMSE          Force-MAE
    ====================================================================================================
    Ensemble:            0.43888             0.33517             2.29408             1.61308
    Mean individual:     0.46668             0.35519             2.50130             1.74724
    Model 1:             0.44034             0.33932             2.42101             1.68499
    Model 2:             0.51447             0.38798             2.65599             1.87485
    Model 3:             0.44524             0.33827             2.42691             1.68189
    ====================================================================================================

and those obtained by training from scratch are shown below:

.. highlight:: none
.. code-block:: none

    Results of 3 models obtained on the test data:

                        Energy-RMSE         Energy-MAE          Force-RMSE          Force-MAE
    ====================================================================================================
    Ensemble:            0.40821             0.30567             2.99477             2.08762
    Mean individual:     0.53181             0.40058             4.08679             2.83257
    Model 1:             0.55260             0.41686             4.38935             3.05035
    Model 2:             0.53672             0.40499             4.10185             2.82622
    Model 3:             0.50612             0.37989             3.76918             2.62113
    ====================================================================================================

You can see that fine-tuning leads to a better performance on atomic forces. The pre-trained and fine-tuned
models can be used in an atomistic simulation similar to previous tutorials.

References
__________

* [1] J. Howard and S. Ruder,  "Universal Language Model Fine-tuning for Text Classification", ACL (2018).
* [2] V. Zaverkin, D. Holzmüller, L. Bonfirraro,  and J. Kästner, “Transfer learning for chemically accurate interatomic neural network potentials,” **submitted** (2022).
