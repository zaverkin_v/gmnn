.. _tio2-tutorial:

.. role:: raw-html(raw)
   :format: html

=============================================
Learning Bulk :raw-html:`TiO<sub>2</sub>` PES
=============================================

Preparing Data Set
__________________

Here, we describe the generation of `.npz` files which can be used as an input to our models. However, different from
:ref:`ethanol-tutorial`, we employ the raw :raw-html:`TiO<sub>2</sub>` `ab-initio Data <http://ann.atomistic.net/download/>`_
written in `.xsf` format [1]. The data set is available free of charge. To generate a `.npz` file containing all
necessary data to train an atomistic neural network, one may run the following code

.. literalinclude:: ../../../examples/data_preparation.py
   :encoding: latin-1

The generated `TiO2.npz` file contains a dictionary with total energies, atomic forces, Cartesian coordinates,
cell vectors, and atomic numbers of 7815 configurations and can be read by running e.g.

.. highlight:: python
.. code-block:: python

    import numpy as np

    data = np.load("TiO2.npz")

    data["E"]
    # array([-4699.05664589, -4699.82998005, -4701.27927338, ...,
    #   -9260.96770039, -9260.80156808, -9261.25728925])

    ...

Training PES Model
__________________

Preparation
***********

Now, the GM-NN model can be trained, similar to :ref:`ethanol-tutorial` by running

.. highlight:: python
.. code-block:: python

    from gmnn.trainer import PESTrainer

    if __name__ == '__main__':

        trainer = PESTrainer(config="pes_training.txt")
        trainer.fit()
        trainer.eval()

However, in this tutorial, we use periodic boundary conditions, which can be switched on by selecting the parameter
``pbc=1``. The corresponding `configure_file` is used to set up the models and the training process and reads

.. highlight:: none
.. code-block:: none

    --data_path=/SCR/HADES2_I1/zaverkin/tutorials/tio2/dataset/TiO2.npz
    --model_path=/SCR/HADES2_I1/zaverkin/tutorials/tio2/models/
    --model_name=example_1
    --cutoff=6.5
    --n_train=5000
    --n_valid=500
    --batch_size=32
    --valid_batch_size=100
    --data_seed=1
    --capacity=10
    --neighbor_list=1
    --skin=0.0
    --device_number=0
    --n_models=1
    --model_seed=1
    --architecture=512,512
    --weight_init_mode=normal
    --bias_init_mode=zeros
    --activation=swish
    --batch_norm=0
    --bn_momentum=0.9955666054790808
    --norm_scale_factor=0.4
    --init_normalization=ntk
    --weight_factor=1.0
    --bias_factor=0.1
    --pbc=1
    --n_radial=5
    --n_basis=7
    --emb_init=uniform
    --use_all_features=1
    --max_epoch=1000
    --save_epoch=100
    --valid_epoch=1
    --base_lr=0.03
    --scale_lr=0.001
    --shift_lr=0.05
    --emb_lr=0.02
    --forces=1
    --stress=0
    --force_weight=4.0
    --stress_weight=0.1
    --beta_2=0.999
    --use_mae_stopping=1

.. note::
    If stress tensors are available, they can be included in the training process by setting ``stress=1`` and employing
    the stress weight ``stress_weight=0.1`` to scale its contribution to the total loss function

    .. math::

        \mathcal{L}\left(\boldsymbol{\theta}\right) = \sum_{k=1}^{N_\mathrm{Train}} \Bigg[\lambda_E \lVert
        E_k^\mathrm{ref} - \hat{E}(S_k, \boldsymbol{\theta})\rVert_2^2 + \lambda_F
        \sum_{i=1}^{N_\mathrm{at}^{(k)}} \lVert \mathbf{F}_{i,k}^\mathrm{ref} - \hat{\mathbf{F}}_i\left(S_k,
        \boldsymbol{\theta}\right)\rVert_2^2 + \lambda_W \sum_{i=1}^{3} \sum_{j=1}^{3} \lVert W_{i,j,k}^\mathrm{ref} -
        \hat{W}_{i,j}\left(S_k, \boldsymbol{\theta}\right)\rVert_2^2\Bigg]~,

    where :math:`\lambda_W` is the stress weight and :math:`\mathbf{W}` is currently computed as

    .. math::

        \mathbf{W} = - \frac{1}{V_\mathrm{Cell}} \sum_{i=1}^{N_\mathrm{at}} \sum_{j \in R_\mathrm{c}}
        \mathbf{R}_{ij} \otimes \boldsymbol{\nabla}_{\mathbf{R}_{ij}} \hat{E} (S_k, \boldsymbol{\theta})~,

    with :math:`V_\mathrm{Cell}` being the volume of the cell and is omitted during training, i.e. we train on
    :math:`\mathbf{W}V_\mathrm{Cell}` and the data has to be prepared accordingly.

Training
********

By running

.. highlight:: bash
.. code-block:: bash

    $ python3 train.py

the training procedure starts and in the specified directory (``model_path``), the following folders and files are created

.. highlight:: bash
.. code-block:: bash

    $ cd <model_path>
    $ ls
    0/  best_stats_ens.pkl  pes_training.txt  train.out

Model checkpoints, which will be used in case the training procedure has to be restarted, are saved in `0/logs`,
while the best models, which are used subsequently during inference, are placed in `0/best`. The text file `train.out` contains
the whole history of the training process with mean absolute error (MAE) and root-mean-square errors (RMSE) of
total energy and atomic forces force. The aforementioned error metrics are evaluated on training and validation data sets.
For each error measure, three columns can be found and correspond to the training error, running validation error,
and the best validation error. In this tutorial, we obtain

.. highlight:: none
.. code-block:: none

    Best checkpoints for model 0 can be found in           ............. /SCR/HADES2_I1/zaverkin/tutorials/tio2/models/example_1/0/best
    Checkpoints for restart for model 0 can be found in    ............. /SCR/HADES2_I1/zaverkin/tutorials/tio2/models/example_1/0/logs

    Epoch            Energy-RMSE (train/valid/best_valid)  Energy-MAE (train/valid/best_valid)  Force-RMSE (train/valid/best_valid)  Force-MAE (train/valid/best_valid)  Time
    ==============================================================================================================================================================================
    Epoch 1/1000:    331.977/119.040/119.040               178.696/80.478/80.478                48.564/17.345/17.345                 16.994/10.958/10.958                [10.23 s]
    Epoch 2/1000:    143.117/149.825/119.040               96.214/110.203/80.478                25.155/13.655/17.345                 10.931/ 8.617/10.958                [ 5.10 s]
    Epoch 3/1000:    125.919/91.187/91.187                 81.841/56.266/56.266                 23.082/13.175/13.175                  9.926/ 7.609/ 7.609                [ 5.08 s]
    Epoch 4/1000:    89.976/84.873/84.873                  57.175/52.738/52.738                 19.260/12.745/12.745                  8.320/ 8.985/ 8.985                [ 5.15 s]
    Epoch 5/1000:    72.374/81.384/84.873                  47.662/64.319/52.738                 15.914/10.127/12.745                  7.588/ 6.345/ 8.985                [ 5.11 s]
    Epoch 6/1000:    79.224/54.690/54.690                  50.807/36.959/36.959                 16.199/10.098/10.098                  7.271/ 5.934/ 5.934                [ 5.07 s]
    Epoch 7/1000:    50.017/42.173/42.173                  31.723/23.894/23.894                 12.841/ 8.396/ 8.396                  6.221/ 5.316/ 5.316                [ 5.12 s]
    Epoch 8/1000:    59.486/50.726/42.173                  38.836/34.465/23.894                 12.178/ 9.396/ 8.396                  5.994/ 5.064/ 5.316                [ 5.08 s]
    Epoch 9/1000:    71.492/167.836/42.173                 41.198/125.564/23.894                14.758/29.797/ 8.396                  5.954/10.181/ 5.316                [ 5.04 s]
    Epoch 10/1000:   90.846/56.674/42.173                  51.292/42.397/23.894                 13.083/ 8.058/ 8.396                  6.583/ 5.334/ 5.316                [ 4.99 s]

    ...

    Epoch 990/1000:   1.557/ 1.439/ 1.439                   1.059/ 0.938/ 0.938                  1.368/ 1.659/ 1.659                  0.840/ 0.950/ 0.950                [ 5.10 s]
    Epoch 991/1000:   1.569/ 1.421/ 1.439                   1.068/ 0.945/ 0.938                  1.369/ 1.660/ 1.659                  0.841/ 0.950/ 0.950                [ 5.11 s]
    Epoch 992/1000:   1.573/ 1.605/ 1.439                   1.052/ 1.068/ 0.938                  1.370/ 1.657/ 1.659                  0.841/ 0.949/ 0.950                [ 5.09 s]
    Epoch 993/1000:   1.512/ 1.425/ 1.439                   1.027/ 0.967/ 0.938                  1.368/ 1.657/ 1.659                  0.840/ 0.949/ 0.950                [ 5.11 s]
    Epoch 994/1000:   1.524/ 1.538/ 1.439                   1.022/ 0.983/ 0.938                  1.367/ 1.659/ 1.659                  0.839/ 0.949/ 0.950                [ 5.14 s]
    Epoch 995/1000:   1.577/ 1.476/ 1.439                   1.063/ 0.943/ 0.938                  1.367/ 1.658/ 1.659                  0.838/ 0.949/ 0.950                [ 5.10 s]
    Epoch 996/1000:   1.503/ 1.445/ 1.439                   0.987/ 0.963/ 0.938                  1.365/ 1.657/ 1.659                  0.836/ 0.948/ 0.950                [ 5.07 s]
    Epoch 997/1000:   1.468/ 1.435/ 1.435                   0.959/ 0.933/ 0.933                  1.366/ 1.657/ 1.657                  0.837/ 0.947/ 0.947                [ 5.14 s]
    Epoch 998/1000:   1.450/ 1.489/ 1.435                   0.937/ 1.038/ 0.933                  1.363/ 1.656/ 1.657                  0.835/ 0.948/ 0.947                [ 5.12 s]
    Epoch 999/1000:   1.444/ 1.432/ 1.435                   0.935/ 0.937/ 0.933                  1.366/ 1.657/ 1.657                  0.837/ 0.947/ 0.947                [ 5.08 s]
    Epoch 1000/1000:  1.432/ 1.427/ 1.427                   0.916/ 0.925/ 0.925                  1.365/ 1.657/ 1.657                  0.836/ 0.947/ 0.947                [ 5.14 s]
    ==============================================================================================================================================================================
    Timing report
    -------------
    Total time                    ............. 5092.245361804962
    Best model report
    -----------------
    Best epochs from the training  ............. [1000]
    ====================================================================================================
    Results of 1 models obtained on the test data:

                        Energy-RMSE         Energy-MAE          Force-RMSE          Force-MAE
    ====================================================================================================
    Ensemble:            3.09517             1.08827             4.82976             1.20924
    Mean individual:     3.09517             1.08827             4.82976             1.20924
    Model 1:             3.09517             1.08827             4.82976             1.20924
    ====================================================================================================

At the end of the `train.out` file, one can find MAE and RMSE evaluated on the test data that have not been seen during
training procedure and are presented for single models and an ensemble if the latter was employed.

.. note::
    If no test data is available, make sure to exclude the last line in `train.py`, i.e.

    .. highlight:: python
    .. code-block:: python

        trainer.eval()

    Otherwise, an error will arise.

All models in this tutorial were trained on an NVIDIA GeForce RTX 2080Ti 11 GB GPU.

Running Molecular Dynamics with ASE
___________________________________

Here, we show a minimal example of the application of a GM-NN model for molecular dynamics (MD) simulations employing the
Atomic Simulation Environment (`ASE <https://wiki.fysik.dtu.dk/ase/>`_) package [2]. Note that other molecular
simulations such as geometry optimization, cell optimization (enabled by setting ``compute_stress=True``), etc.,
are possible and can be conducted analogously to this example. To perform an MD simulation in a microcanonical (NVE)
statistical ensemble, we will use the `tio2_md.py` script

.. literalinclude:: ../../../examples/tio2_md.py
   :encoding: latin-1

.. note:: In this example, we have used a skin of :math:`\mathrm{skin}=0.25` Ang. which makes the simulation somewhat
          faster and can be employed in other simulations for the same purpose. However, by using a larger cutoff
          :math:`R_\mathrm{c} + \mathrm{skin}` one has to take into account the tradeoff between, on average, the faster
          construction of the neighbor list but a slower construction of the atomic representation due to the increased
          number of neighbors.

We assess the energy conservation during the microcanonical (NVE) statistical ensemble in this minimal example. To plot the
total energy over time we use

.. literalinclude:: ../../../examples/energy_conservation.py
   :encoding: latin-1

The corresponding graph is shown below

.. _fig-energy-conservation:
.. figure:: energy_conservation_tio2.png
   :width: 600px
   :align: center

The figure shows that the total energy predicted via our GM-NN potential is certainly conserved with fluctuations
below 0.025 meV/atom. To be able to see the mean value, one can compute the running average of the total energy defined as

.. math::

   \hat{O}_i = \frac{1}{M+1} \sum_{j=i - M/2}^{i + M/2} O_j~,

where :math:`M` is the window size of the running average and :math:`O_i` is an observable (total energy in our case).
Finally, we observe only a slight energy drift of :math:`(0.00106 \pm 0.00006)` meV/atom/ns, which is another indicator of
the robustness and accuracy of the created machine-learned interatomic potential.

References
__________

* [1] N. Artrith and A. Urban, “An implementation of artificial neural-network potentials for atomistic materials simulations: Performance for :raw-html:`TiO<sub>2</sub>`,” Comput. Mater. Sci. **114**, 135–150 (2016).
* [2] A. H. Larsen `et al.`, “The atomic simulation environment—a python library for working with atoms,” J. Phys. Condens. Matter **29**, 273002 (2017).