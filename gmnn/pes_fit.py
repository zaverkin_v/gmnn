import os
import time
from typing import *
from gmnn.adam import *
from gmnn.utils import *
from gmnn.data_pipeline import *


class PESFit:
    """Train and evaluate the specified model on predefined training, validation, and test data.

    :param models: List of models.
    :param forces: Set ``True`` for training with forces.
    :param force_weight: Rescale force contribution in the combined loss.
    :param stress: Set ``True`` for training with atomic stresses (this option is under development,
                   use it at your own risk).
    :param stress_weight: Rescale stress contribution in the combined loss.
    :param pbc: Set ``True`` if periodic boundary conditions has to be implied.
    :param beta_2: beta_2 parameter of Adam (momentum for second-order statistics).
    :param device_number: GPU index (if GPU is available).

    Example:

    .. highlight:: python
    .. code-block:: python

        ...

        dictionary = np.load('data_path')
        dc = DataContainer(dictionary=dictionary, cutoff=4.0, neighbor_list=True, skin=0.0)
        dcs = self.dc.split({"train": 1000, "valid": 200}, seed=1)

        ...

        from gmnn.pes_fit import PESFit

        # set learning rate and learning schedule
        lr = [0.001, 0.05, 0.02] + [0.03] * (len(architecture) - 1) * 2

        lr_sched = lambda x: 1. - x

        pes_fit = PESFit(models, forces=True, stress=False, force_weight=4.0, stress_weight=0.1, pbc=True,
                         beta_2=0.999, device_number=0)

        # train model
        pes_fit.fit(model_path='model_path', train_dc=dcs["train"], valid_dc=dcs["valid"], train_batch_size=32,
                    valid_batch_size=100, capacity=5, lr=lr, lr_sched=lr_sched, max_epoch=1000, save_epoch=100,
                    validate_epoch=1, mae_stopping=True, seed=1)

        # test model
        pes_fit.eval(model_path=model_path, test_dc=dcs["test"], test_batch_size=100, capacity=5)
    """
    def __init__(self, models: List[tf.keras.layers.Layer], forces: bool = False, force_weight: float = 4.0,
                 stress: bool = False, stress_weight: float = 0.1, pbc: bool = False, beta_2: float = 0.95,
                 device_number: int = 0, loss: str = 'molecules'):
        self.models = models
        self.forces = forces
        self.force_weight = tf.Variable(force_weight, trainable=False)
        self.stress = stress
        self.stress_weight = tf.Variable(stress_weight, trainable=False)
        self.pbc = pbc
        self.loss = loss

        self.print_metrics = ['Energy-RMSE', 'Energy-MAE']
        if self.forces:
            self.print_metrics.extend(['Force-RMSE', 'Force-MAE'])
        if self.stress:
            self.print_metrics.extend(['Stress-RMSE', 'Stress-MAE'])

        # determine device
        gpus = tf.config.experimental.list_physical_devices('GPU')
        if gpus:
            self.device = f'/GPU:{device_number}'
        else:
            self.device = '/CPU:0'

        # determine optimizer
        self.optimizers = [Adam(model.trainable_variables, beta_1=0.9, beta_2=beta_2) for model in self.models]

    @tf.function(input_signature=[
        tf.TensorSpec([None, ], dtype=tf.float32),              # data['N']
        tf.TensorSpec([None, ], dtype=tf.float32),              # data['E']
        tf.TensorSpec([None, None], dtype=tf.float32),          # data['F']
        tf.TensorSpec([None, ], dtype=tf.int32),                # data['Z']
        tf.TensorSpec([None, None], dtype=tf.float32),          # data['D']
        tf.TensorSpec([None, ], dtype=tf.float32),              # data['Q']
        tf.TensorSpec([None, None], dtype=tf.float32),          # data['R']
        tf.TensorSpec([None, None, None], dtype=tf.float32),    # data['C']
        tf.TensorSpec([None, None, None], dtype=tf.float32),    # data['W']
        tf.TensorSpec([None, ], dtype=tf.int32),                # data['idx_i']
        tf.TensorSpec([None, ], dtype=tf.int32),                # data['idx_j']
        tf.TensorSpec([None, None], dtype=tf.float32),          # data['offsets']
        tf.TensorSpec([None, ], dtype=tf.int32)                 # data['batch_seg']
    ])
    def _inference(self, N, E, F, Z, D, Q, R, C, W, idx_i, idx_j, offsets, batch_seg):
        if self.forces and self.stress:
            E_bp_pred = []
            F_at_pred = []
            W_at_pred = []
            for model in self.models:
                with tf.GradientTape(watch_accessed_variables=False) as force_tape:
                    force_tape.watch(R)
                    R_ij = get_distance_vectors(R, C, idx_i, idx_j, offsets, batch_seg, pbc=self.pbc)
                    with tf.GradientTape(watch_accessed_variables=False) as stress_tape:
                        stress_tape.watch(R_ij)
                        x = (R_ij, Z, idx_i, idx_j)
                        E_at_pred = model(x, training=False)
                        # sum up atomic contributions E_tot = \sum_i E_i
                        E_bp_pred.append(tf.math.segment_sum(E_at_pred, batch_seg))
                        E_total = tf.reduce_sum(E_bp_pred[-1])
                    # compute forces F_ij = dE_tot/dR_ij
                    F_ij_pred = tf.convert_to_tensor(stress_tape.gradient(E_total, R_ij))
                    # compute stress W = - \sum_i\sum_j R_ij \otimes F_ij
                    W_at_pred.append(- 1.0 * tf.math.segment_sum(
                        tf.math.segment_sum(F_ij_pred[:, None, :] * R_ij[:, :, None], idx_i),
                        batch_seg))
                # compute atomic forces F_i = -dE_tot/dR_i
                F_at_pred.append(-tf.convert_to_tensor(force_tape.gradient(E_total, R)))

            return E_bp_pred, F_at_pred, W_at_pred

        if self.forces and not self.stress:
            E_bp_pred = []
            F_at_pred = []
            for model in self.models:
                with tf.GradientTape(watch_accessed_variables=False) as force_tape:
                    force_tape.watch(R)
                    R_ij = get_distance_vectors(R, C, idx_i, idx_j, offsets, batch_seg, pbc=self.pbc)
                    x = (R_ij, Z, idx_i, idx_j)
                    E_at_pred = model(x, training=False)
                    # sum up atomic contributions E_tot = \sum_i E_i
                    E_bp_pred.append(tf.math.segment_sum(E_at_pred, batch_seg))
                    E_total = tf.reduce_sum(E_bp_pred[-1])
                # compute atomic forces F_i = -dE_tot/dR_i
                F_at_pred.append(-tf.convert_to_tensor(force_tape.gradient(E_total, R)))

            return E_bp_pred, F_at_pred

        else:
            E_bp_pred = []
            for model in self.models:
                R_ij = get_distance_vectors(R, C, idx_i, idx_j, offsets, batch_seg, pbc=self.pbc)
                x = (R_ij, Z, idx_i, idx_j)
                E_at_pred = model(x, training=False)
                # sum up atomic contributions E_tot = \sum_i E_i
                E_bp_pred.append(tf.math.segment_sum(E_at_pred, batch_seg))

            return E_bp_pred

    def _eval_with_loss(self, N, E, F, Z, D, Q, R, C, W, idx_i, idx_j, offsets, batch_seg, training):
        individual_metrics_list = []
        if self.forces and self.stress:
            E_bp_pred = []
            F_at_pred = []
            W_at_pred = []
            for model in self.models:
                with tf.GradientTape(watch_accessed_variables=False) as force_tape:
                    force_tape.watch(R)
                    R_ij = get_distance_vectors(R, C, idx_i, idx_j, offsets, batch_seg, pbc=self.pbc)
                    with tf.GradientTape(watch_accessed_variables=False) as stress_tape:
                        stress_tape.watch(R_ij)
                        x = (R_ij, Z, idx_i, idx_j)
                        E_at_pred = model(x, training=training)
                        # sum up atomic contributions E_tot = \sum_i E_i
                        E_bp_pred.append(tf.math.segment_sum(E_at_pred, batch_seg))
                        E_total = tf.reduce_sum(E_bp_pred[-1])
                    # compute forces F_ij = dE_tot/dR_ij
                    F_ij_pred = tf.convert_to_tensor(stress_tape.gradient(E_total, R_ij))
                    # compute stress W = - \sum_i\sum_j R_ij \otimes F_ij
                    W_at_pred.append(- 1.0 * tf.math.segment_sum(
                        tf.math.segment_sum(F_ij_pred[:, None, :] * R_ij[:, :, None], idx_i),
                        batch_seg))
                # compute atomic forces F_i = -dE_tot/dR_i
                F_at_pred.append(-tf.convert_to_tensor(force_tape.gradient(E_total, R)))
            
            # todo: need to take it into account during building the data set
            W *= tf.abs(tf.linalg.det(C))[:, None, None]

            for i in range(len(self.models)):
                # names are given according to the mlip package (https://iopscience.iop.org/article/10.1088/2632-2153/abc9fe/meta)
                # from which it was also taken how to rescale energy, force, and stress losses according to the specific task
                if self.loss == 'molecules':
                    # sum over structures, atoms, and elements
                    F_loss = tf.reduce_sum((F - F_at_pred[i]) ** 2)
                    # sum over structures
                    E_loss = tf.reduce_sum((E[:, None] - E_bp_pred[i]) ** 2)
                    # sum over structures, elements
                    W_loss = tf.reduce_sum((W - W_at_pred[i]) ** 2)
                elif self.loss == 'structures':
                    # sum over structures, atoms, and elements
                    F_loss = tf.reduce_sum(tf.math.segment_mean((F - F_at_pred[i]) ** 2, batch_seg))
                    # sum over structures
                    E_loss = tf.reduce_sum((E[:, None] - E_bp_pred[i]) ** 2 / N[:, None] ** 2)
                    # sum over structures, elements
                    W_loss = tf.reduce_sum((W - W_at_pred[i]) ** 2 / N[:, None, None] ** 2)
                elif self.loss == 'vibrations':
                    # sum over structures, atoms, and elements
                    F_loss = tf.reduce_sum((F - F_at_pred[i]) ** 2)
                    # sum over structures
                    E_loss = tf.reduce_sum((E[:, None] - E_bp_pred[i]) ** 2 / N[:, None])
                    # sum over structures, elements
                    W_loss = tf.reduce_sum((W - W_at_pred[i]) ** 2 / N[:, None, None])
                else:
                    raise NotImplementedError(f'{self.loss} loss is not implemented')

                individual_metrics_list.append(
                    {'Loss': E_loss + self.force_weight * F_loss + self.stress_weight * W_loss,
                     'Energy-MSE': (tf.reduce_mean((E[:, None] - E_bp_pred[i]) ** 2 / N[:, None] ** 2) if (self.loss == 'structures' or self.loss == 'vibrations')
                                    else tf.reduce_mean((E[:, None] - E_bp_pred[i]) ** 2)),
                     'Energy-MAE': (tf.reduce_mean(tf.abs(E[:, None] - E_bp_pred[i]) / N[:, None]) if (self.loss == 'structures' or self.loss == 'vibrations')
                                    else tf.reduce_mean(tf.abs(E[:, None] - E_bp_pred[i]))),
                     'Force-MSE': tf.reduce_mean((F - F_at_pred[i]) ** 2),
                     'Force-MAE': tf.reduce_mean(tf.abs(F - F_at_pred[i])),
                     'Stress-MSE': tf.reduce_mean((W - W_at_pred[i]) ** 2),
                     'Stress-MAE': tf.reduce_mean(tf.abs(W - W_at_pred[i]))})

            F_at_ens = sum(F_at_pred) / len(self.models)
            E_bp_ens = sum(E_bp_pred) / len(self.models)
            W_ens = sum(W_at_pred) / len(self.models)

            ens_metrics = {'Energy-MSE': (tf.reduce_mean((E[:, None] - E_bp_ens) ** 2 / N[:, None] ** 2) if (self.loss == 'structures' or self.loss == 'vibrations')
                                          else tf.reduce_mean((E[:, None] - E_bp_ens) ** 2)),
                           'Energy-MAE': (tf.reduce_mean(tf.abs(E[:, None] - E_bp_ens) / N[:, None]) if (self.loss == 'structures' or self.loss == 'vibrations')
                                          else tf.reduce_mean(tf.abs(E[:, None] - E_bp_ens))),
                           'Force-MSE': tf.reduce_mean((F - F_at_ens) ** 2),
                           'Force-MAE': tf.reduce_mean(tf.abs(F - F_at_ens)),
                           'Stress-MSE': tf.reduce_mean((W - W_ens) ** 2),
                           'Stress-MAE': tf.reduce_mean(tf.abs(W - W_ens))}

            return individual_metrics_list, ens_metrics

        elif self.forces and not self.stress:
            E_bp_pred = []
            F_at_pred = []
            for model in self.models:
                with tf.GradientTape(watch_accessed_variables=False) as force_tape:
                    force_tape.watch(R)
                    R_ij = get_distance_vectors(R, C, idx_i, idx_j, offsets, batch_seg, pbc=self.pbc)
                    x = (R_ij, Z, idx_i, idx_j)
                    E_at_pred = model(x, training=training)
                    # sum up atomic contributions E_tot = \sum_i E_i
                    E_bp_pred.append(tf.math.segment_sum(E_at_pred, batch_seg))
                    E_total = tf.reduce_sum(E_bp_pred[-1])
                # compute atomic forces F_i = -dE_tot/dR_i
                F_at_pred.append(-tf.convert_to_tensor(force_tape.gradient(E_total, R)))

            for i in range(len(self.models)):
                # names are given according to the mlip package (https://iopscience.iop.org/article/10.1088/2632-2153/abc9fe/meta)
                # from which it was also taken how to rescale energy, force, and stress losses according to the specific task
                if self.loss == 'molecules':
                    # sum over structures, atoms, and elements
                    F_loss = tf.reduce_sum((F - F_at_pred[i]) ** 2)
                    # sum over structures
                    E_loss = tf.reduce_sum((E[:, None] - E_bp_pred[i]) ** 2)
                elif self.loss == 'structures':
                    # sum over structures, atoms, and elements
                    F_loss = tf.reduce_sum(tf.math.segment_mean((F - F_at_pred[i]) ** 2, batch_seg))
                    # sum over structures
                    E_loss = tf.reduce_sum((E[:, None] - E_bp_pred[i]) ** 2 / N[:, None] ** 2)
                elif self.loss == 'vibrations':
                    # sum over structures, atoms, and elements
                    F_loss = tf.reduce_sum((F - F_at_pred[i]) ** 2)
                    # sum over structures
                    E_loss = tf.reduce_sum((E[:, None] - E_bp_pred[i]) ** 2 / N[:, None])
                else:
                    raise NotImplementedError(f'{self.loss} loss is not implemented')

                individual_metrics_list.append(
                    {'Loss': E_loss + self.force_weight * F_loss,
                     'Energy-MSE': (tf.reduce_mean((E[:, None] - E_bp_pred[i])**2 / N[:, None] ** 2) if (self.loss == 'structures' or self.loss == 'vibrations')
                                    else tf.reduce_mean((E[:, None] - E_bp_pred[i])**2)),
                     'Energy-MAE': (tf.reduce_mean(tf.abs(E[:, None] - E_bp_pred[i]) / N[:, None]) if (self.loss == 'structures' or self.loss == 'vibrations')
                                    else tf.reduce_mean(tf.abs(E[:, None] - E_bp_pred[i]))),
                     'Force-MSE': tf.reduce_mean((F - F_at_pred[i])**2),
                     'Force-MAE': tf.reduce_mean(tf.abs(F - F_at_pred[i]))})

            F_at_ens = sum(F_at_pred) / len(self.models)
            E_bp_ens = sum(E_bp_pred) / len(self.models)
            ens_metrics = {'Energy-MSE': (tf.reduce_mean((E[:, None] - E_bp_ens)**2 / N[:, None] ** 2) if (self.loss == 'structures' or self.loss == 'vibrations')
                                          else tf.reduce_mean((E[:, None] - E_bp_ens)**2)),
                           'Energy-MAE': (tf.reduce_mean(tf.abs(E[:, None] - E_bp_ens) / N[:, None]) if (self.loss == 'structures' or self.loss == 'vibrations')
                                          else tf.reduce_mean(tf.abs(E[:, None] - E_bp_ens))),
                           'Force-MSE': tf.reduce_mean((F - F_at_ens)**2),
                           'Force-MAE': tf.reduce_mean(tf.abs(F - F_at_ens))}

            return individual_metrics_list, ens_metrics

        else:
            E_bp_pred = []
            for model in self.models:
                R_ij = get_distance_vectors(R, C, idx_i, idx_j, offsets, batch_seg, pbc=self.pbc)
                x = (R_ij, Z, idx_i, idx_j)
                E_at_pred = model(x, training=training)
                # sum up atomic contributions E_tot = \sum_i E_i
                E_bp_pred.append(tf.math.segment_sum(E_at_pred, batch_seg))

                # names are given according to the mlip package (https://iopscience.iop.org/article/10.1088/2632-2153/abc9fe/meta)
                # from which it was also taken how to rescale energy, force, and stress losses according to the specific task
                if self.loss == 'molecules':
                    # sum over structures
                    E_loss = tf.reduce_sum((E[:, None] - E_bp_pred[-1]) ** 2)
                elif self.loss == 'structures':
                    # sum over structures
                    E_loss = tf.reduce_sum((E[:, None] - E_bp_pred[-1]) ** 2 / N[:, None] ** 2)
                elif self.loss == 'vibrations':
                    # sum over structures
                    E_loss = tf.reduce_sum((E[:, None] - E_bp_pred[-1]) ** 2 / N[:, None])
                else:
                    raise NotImplementedError(f'{self.loss} loss is not implemented')

                individual_metrics_list.append(
                    {'Loss': E_loss,
                     'Energy-MSE': (tf.reduce_mean((E[:, None] - E_bp_pred[-1]) ** 2 / N[:, None] ** 2) if (self.loss == 'structures' or self.loss == 'vibrations')
                                    else tf.reduce_mean((E[:, None] - E_bp_pred[-1]) ** 2)),
                     'Energy-MAE': (tf.reduce_mean(tf.abs(E[:, None] - E_bp_pred[-1]) / N[:, None]) if (self.loss == 'structures' or self.loss == 'vibrations')
                                    else tf.reduce_mean(tf.abs(E[:, None] - E_bp_pred[-1])))})

            E_bp_ens = sum(E_bp_pred) / len(self.models)
            ens_metrics = {'Energy-MSE': (tf.reduce_mean((E[:, None] - E_bp_ens) ** 2 / N[:, None] ** 2) if (self.loss == 'structures' or self.loss == 'vibrations')
                                          else tf.reduce_mean((E[:, None] - E_bp_ens) ** 2)),
                           'Energy-MAE': (tf.reduce_mean(tf.abs(E[:, None] - E_bp_ens) / N[:, None]) if (self.loss == 'structures' or self.loss == 'vibrations')
                                          else tf.reduce_mean(tf.abs(E[:, None] - E_bp_ens)))}

            return individual_metrics_list, ens_metrics

    @tf.function(input_signature=[
        tf.TensorSpec([None, ], dtype=tf.float32),              # data['N']
        tf.TensorSpec([None, ], dtype=tf.float32),              # data['E']
        tf.TensorSpec([None, None], dtype=tf.float32),          # data['F']
        tf.TensorSpec([None, ], dtype=tf.int32),                # data['Z']
        tf.TensorSpec([None, None], dtype=tf.float32),          # data['D']
        tf.TensorSpec([None, ], dtype=tf.float32),              # data['Q']
        tf.TensorSpec([None, None], dtype=tf.float32),          # data['R']
        tf.TensorSpec([None, None, None], dtype=tf.float32),    # data['C']
        tf.TensorSpec([None, None, None], dtype=tf.float32),    # data['W']
        tf.TensorSpec([None, ], dtype=tf.int32),                # data['idx_i']
        tf.TensorSpec([None, ], dtype=tf.int32),                # data['idx_j']
        tf.TensorSpec([None, None], dtype=tf.float32),          # data['offsets']
        tf.TensorSpec([None, ], dtype=tf.int32)                 # data['batch_seg']
    ])
    def _train_step(self, N, E, F, Z, D, Q, R, C, W, idx_i, idx_j, offsets, batch_seg):
        with tf.GradientTape() as loss_tape:
            individual_metrics_list, ens_metrics = self._eval_with_loss(N, E, F, Z, D, Q, R, C, W, idx_i, idx_j,
                                                                        offsets, batch_seg, training=True)
            total_loss = sum(individual_metrics_list[i]['Loss'] for i in range(len(self.models)))

        # compute loss gradients
        gradients = loss_tape.gradient(total_loss,
                                       [self.models[i].trainable_variables for i in range(len(self.models))])

        for i in range(len(self.models)):
            self.optimizers[i].step(gradients[i])

        return individual_metrics_list, ens_metrics

    @tf.function(input_signature=[
        tf.TensorSpec([None, ], dtype=tf.float32),              # data['N']
        tf.TensorSpec([None, ], dtype=tf.float32),              # data['E']
        tf.TensorSpec([None, None], dtype=tf.float32),          # data['F']
        tf.TensorSpec([None, ], dtype=tf.int32),                # data['Z']
        tf.TensorSpec([None, None], dtype=tf.float32),          # data['D']
        tf.TensorSpec([None, ], dtype=tf.float32),              # data['Q']
        tf.TensorSpec([None, None], dtype=tf.float32),          # data['R']
        tf.TensorSpec([None, None, None], dtype=tf.float32),    # data['C']
        tf.TensorSpec([None, None, None], dtype=tf.float32),    # data['W']
        tf.TensorSpec([None, ], dtype=tf.int32),                # data['idx_i']
        tf.TensorSpec([None, ], dtype=tf.int32),                # data['idx_j']
        tf.TensorSpec([None, None], dtype=tf.float32),          # data['offsets']
        tf.TensorSpec([None, ], dtype=tf.int32)                 # data['batch_seg']
    ])
    def _valid_step(self, N, E, F, Z, D, Q, R, C, W, idx_i, idx_j, offsets, batch_seg):
        return self._eval_with_loss(N, E, F, Z, D, Q, R, C, W, idx_i, idx_j, offsets, batch_seg, training=False)

    def fit(self, model_path: str, train_dc: DataContainer, valid_dc: DataContainer, lr: Union[float, List[float]],
            lr_sched: Callable[[float], float], train_batch_size: int = 32, valid_batch_size: int = 100, capacity: int = 5,
            max_epoch: int = 1000, save_epoch: int = 100, validate_epoch: int = 1, max_to_keep: int = 10,
            mae_stopping: bool = True, seed: Optional[int] = None):
        """Train the specified model.

        :param model_path: Path to the model.
        :param train_dc: ``DataContainer`` providing training data.
        :param valid_dc: ``DataContainer`` providing validation data.
        :param train_batch_size: Mini-batch size for training.
        :param valid_batch_size: Mini-batch size for validation
        :param capacity: Capacity of data queue.
        :param lr: Learning rate.
        :param lr_sched: Learning rate schedule.
        :param max_epoch: Number of training epochs.
        :param save_epoch: Save a checkpoint after ``save_epoch`` epochs.
        :param validate_epoch: Validate model after ``validate_epoch`` epochs.
        :param max_to_keep: Maximal number of stored checkpoints.
        :param mae_stopping: Set ``True`` if mean absolute error has to be used for early stopping.
        :param seed: Used to generate certain permutations of training data.

        .. note::
            For restart provide ``model_path`` with `logs` directory containing previous checkpoints.
        """

        # create best and log directories to save/restore training progress
        if not os.path.exists(model_path):
            os.makedirs(model_path)
        log_dirs = [os.path.join(model_path, str(i), 'logs') for i in range(len(self.models))]
        best_dirs = [os.path.join(model_path, str(i), 'best') for i in range(len(self.models))]
        for dir in log_dirs + best_dirs:
            if not os.path.exists(dir):
                os.makedirs(dir)

        # define files to save the progress of training
        train_out = os.path.join(model_path, 'train.out')
        best_stats_files = [os.path.join(dir, 'best_stats.pkl') for dir in best_dirs]
        best_stats_ens_file = os.path.join(model_path, 'best_stats_ens.pkl')

        # all variables in ckpt will be stored in the checkpoint
        # they are automatically loaded by the CheckpointManager if there is already a checkpoint in log_dir
        ckpts = [tf.train.Checkpoint(epoch=tf.Variable(0), optimizer=self.optimizers[i], model=self.models[i])
                 for i in range(len(self.models))]
        ckpt_managers_log = [tf.train.CheckpointManager(ckpts[i], log_dirs[i], max_to_keep=max_to_keep)
                             for i in range(len(self.models))]
        ckpt_managers_best = [tf.train.CheckpointManager(ckpts[i], best_dirs[i], max_to_keep=max_to_keep)
                             for i in range(len(self.models))]

        # start TensorFlow session
        with tf.device(self.device):
            for i in range(len(self.models)):
                ckpts[i].restore(ckpt_managers_log[i].latest_checkpoint)
            epoch = int(ckpts[0].epoch)
            # start timing
            start_session = time.time()

            # start data queues
            coord = tf.train.Coordinator()

            # generate data queues for efficient training
            train_dl = DataLoader(dc=train_dc, batch_size=train_batch_size, shuffle=True, seed=seed,
                                     device=self.device, capacity=capacity, coord=coord, daemon=True)
            valid_dl = DataLoader(dc=valid_dc, batch_size=valid_batch_size, shuffle=False,
                                     device=self.device, capacity=capacity, coord=coord, daemon=True)

            steps_per_epoch = len(train_dl)
            max_step = max_epoch * steps_per_epoch

            if epoch > 0:
                # restored checkpoint
                f = open(train_out, "a+")
                f.write('============= \n')
                f.write('Training is restarted from epoch {} \n'.format(epoch))

            else:
                # start new session
                create_header(train_out)
                f = open(train_out, "a+")

            headings = [metric + ' (train/valid/best_valid)' for metric in self.print_metrics]
            # last column does not need to be whitespace-padded because it does not matter visually
            column_widths = [17] + [len(heading) + 2 for heading in headings] + [0]
            headings = ['Epoch'] + headings + ['Time']

            f.write('Best checkpoints for model 0 can be found in           ............. {} \n'.format(best_dirs[0]))
            f.write('Checkpoints for restart for model 0 can be found in    ............. {} \n'.format(log_dirs[0]))
            f.write(' \n')
            f.write(padded_str(headings, column_widths) + '\n')
            f.write("".ljust(sum(column_widths) + 9, "=") + "\n")
            f.close()

            best_valid_metrics = [None] * len(self.models)
            best_epochs = [None] * len(self.models)
            best_valid_ens_metrics = None

            if epoch > 0:
                best_valid_ens_metrics = load_object(best_stats_ens_file)
                for i in range(len(self.models)):
                    m, e = load_object(best_stats_files[i])
                    best_valid_metrics[i] = m
                    best_epochs[i] = e

            if mae_stopping:
                best_metric_names = ['Energy-MAE']
                if self.forces:
                    best_metric_names.append('Force-MAE')
                # stress seems to have way to strong influence on the selected model, try to use it during training
                # but not during the early stopping
                #if self.stress:
                #    best_metric_names.append('Stress-MAE')
            else:
                best_metric_names = ['Energy-RMSE']
                if self.forces:
                    best_metric_names.append('Force-RMSE')
                # stress seems to have way to strong influence on the selected model, try to use it during training
                # but not during the early stopping
                #if self.stress:
                #    best_metric_names.append('Stress-RMSE')

            while epoch < max_epoch and not coord.should_stop():
                start_epoch = time.time()
                for ckpt in ckpts:
                    ckpt.epoch.assign_add(1)
                epoch += 1

                train_ens_metrics = None

                for batch_idx, data in enumerate(train_dl):
                    step = steps_per_epoch * (epoch-1) + batch_idx

                    lrs = tf.convert_to_tensor(np.asarray(lr) * lr_sched(step/max_step), dtype=tf.float32)
                    for opt in self.optimizers:
                        opt.set_lr(lrs)
                    _, ens_metrics = self._train_step(data['N'], data['E'], data['F'], data['Z'], data['D'], data['Q'],
                                                      data['R'], data['C'], data['W'], data['idx_i'], data['idx_j'],
                                                      data['offsets'], data['batch_seg'])
                    train_ens_metrics = update_metrics(train_ens_metrics, ens_metrics, batch_idx)

                augment_rmse(train_ens_metrics)

                if epoch % save_epoch == 0:
                    # save progress for restoring
                    for i in range(len(self.models)):
                        ckpt_managers_log[i].save()

                if epoch % validate_epoch == 0 or epoch == max_epoch:
                    # check performance on validation step
                    valid_ens_metrics = None
                    valid_indiv_metrics = [None] * len(self.models)

                    for batch_idx, data in enumerate(valid_dl):
                        indiv_metrics, ens_metrics = self._valid_step(data['N'], data['E'], data['F'], data['Z'],
                                                                      data['D'], data['Q'], data['R'], data['C'],
                                                                      data['W'], data['idx_i'], data['idx_j'],
                                                                      data['offsets'], data['batch_seg'])
                        valid_ens_metrics = update_metrics(valid_ens_metrics, ens_metrics, batch_idx)
                        for i in range(len(self.models)):
                            valid_indiv_metrics[i] = update_metrics(valid_indiv_metrics[i], indiv_metrics[i],
                                                                         batch_idx)

                    # process ensemble metrics
                    augment_rmse(valid_ens_metrics)
                    valid_ens_score = sum([valid_ens_metrics[name] for name in best_metric_names])
                    best_valid_ens_score = np.Inf if best_valid_ens_metrics is None \
                        else sum([best_valid_ens_metrics[name] for name in best_metric_names])
                    if valid_ens_score < best_valid_ens_score:
                        best_valid_ens_metrics = valid_ens_metrics
                        save_object(best_stats_ens_file, best_valid_ens_metrics)

                    # process individual model metrics
                    for i in range(len(self.models)):
                        augment_rmse(valid_indiv_metrics[i])
                        valid_score = sum([valid_indiv_metrics[i][name] for name in best_metric_names])
                        best_valid_score = np.Inf if best_valid_metrics[i] is None \
                                                  else sum([best_valid_metrics[i][name] for name in best_metric_names])
                        if valid_score < best_valid_score:
                            best_valid_metrics[i] = valid_indiv_metrics[i]
                            best_epochs[i] = epoch
                            save_object(best_stats_files[i], (best_valid_metrics[i], best_epochs[i]))
                            ckpt_managers_best[i].save()

                    end_epoch = time.time()

                    f = open(train_out, "a+")
                    strs = [f'Epoch {epoch}/{max_epoch}: ']
                    for metric_name in self.print_metrics:
                        vals = [train_ens_metrics[metric_name], valid_ens_metrics[metric_name],
                                best_valid_ens_metrics[metric_name]]
                        strs.append('/'.join([f'{val:6.3f}' for val in vals]) + ' ')
                    strs.append(f'[{end_epoch - start_epoch:5.2f} s]')
                    f.write(padded_str(strs, column_widths) + '\n')
                    f.close()

            coord.request_stop()

            end_session = time.time()
            f = open(train_out, "a+")
            f.write("".ljust(sum(column_widths) + 9, "=") + "\n")
            f.write('Timing report \n')
            f.write("".ljust(13, "-") + "\n")
            f.write("Total time                    ............. {} \n".format(end_session - start_session))
            f.write('Best model report \n')
            f.write("".ljust(17, "-") + "\n")
            f.write('Best epochs from the training  ............. {} \n'.format(best_epochs))
            f.close()

    def eval(self, model_path: str, test_dc: DataContainer, test_batch_size: int = 32, capacity: int = 5,
             max_to_keep: int = 10):
        """Test the specified model.

        :param model_path: Path to the model with `best` directory.
        :param test_dc: ``DataContainer`` providing test data.
        :param test_batch_size: Mini-batch size for testing.
        :param capacity: Capacity of data queue.
        :param max_to_keep: Maximal number of stored checkpoints.
        """
        # open the best directory to load the best model
        if not os.path.exists(model_path):
            os.makedirs(model_path)

        # define files to save the results on the test data
        test_out = os.path.join(model_path, 'train.out')
        column_widths = [20] + [20] * len(self.print_metrics)
        f = open(test_out, "a+")
        f.write("".ljust(sum(column_widths), "=") + "\n")
        f.write(f'Results of {len(self.models)} models obtained on the test data:\n')
        f.write(' \n')
        f.write(padded_str([''] + self.print_metrics, column_widths) + '\n')
        f.write("".ljust(sum(column_widths), "=") + "\n")
        f.close()

        # all variables in ckpt will be stored in the checkpoint
        # they are automatically loaded by the CheckpointManager if there is already a checkpoint in log_dir

        for i in range(len(self.models)):
            ckpt = tf.train.Checkpoint(epoch=tf.Variable(0), optimizer=self.optimizers[i], model=self.models[i])
            best_dir = os.path.join(model_path, str(i), 'best')
            ckpt_manager_best = tf.train.CheckpointManager(ckpt, best_dir, max_to_keep=max_to_keep)
            ckpt.restore(ckpt_manager_best.latest_checkpoint)

        # start TensorFlow session
        with tf.device(self.device):

            # start data queues
            coord = tf.train.Coordinator()

            # generate data queues
            test_dl = DataLoader(dc=test_dc, batch_size=test_batch_size, shuffle=False,
                                  device=self.device, capacity=capacity, coord=coord, daemon=True)

            test_ens_metrics = None
            test_indiv_metrics = [None] * len(self.models)

            for batch_idx, data in enumerate(test_dl):
                indiv_metrics, ens_metrics = self._valid_step(data['N'], data['E'], data['F'], data['Z'], data['D'],
                                                              data['Q'], data['R'], data['C'], data['W'], data['idx_i'],
                                                              data['idx_j'], data['offsets'], data['batch_seg'])
                test_ens_metrics = update_metrics(test_ens_metrics, ens_metrics, batch_idx)
                for i in range(len(self.models)):
                    test_indiv_metrics[i] = update_metrics(test_indiv_metrics[i], indiv_metrics[i], batch_idx)

            augment_rmse(test_ens_metrics)
            for i in range(len(self.models)):
                augment_rmse(test_indiv_metrics[i])

            f = open(test_out, "a+")
            ens_results = [test_ens_metrics[name] for name in self.print_metrics]
            mean_results = [sum(test_indiv_metrics[i][name] for i in range(len(self.models))) / len(self.models)
                            for name in self.print_metrics]
            f.write(padded_str(['Ensemble:'] + [f'{result:8.5f}' for result in ens_results], column_widths) + '\n')
            f.write(padded_str(['Mean individual:'] + [f'{result:8.5f}' for result in mean_results], column_widths)
                    + '\n')
            for i in range(len(self.models)):
                f.write(padded_str([f'Model {i+1}:'] + [f'{test_indiv_metrics[i][name]:8.5f}'
                                                        for name in self.print_metrics], column_widths)
                        + '\n')
            f.write("".ljust(sum(column_widths), "=") + "\n")
            f.close()

            coord.request_stop()

    def measure_inference_time(self, model_path: str, test_dc: DataContainer, test_batch_size: int = 32,
                               capacity: int = 5, max_to_keep: int = 10):
        """Measure inference time for the specified model.

        :param model_path: Path to the model with `best` directory.
        :param test_dc: ``DataContainer`` providing test data.
        :param test_batch_size: Mini-batch size for testing.
        :param capacity: Capacity of data queue.
        :param max_to_keep: Maximal number of stored checkpoints.
        """
        # open the best directory to load the best model
        if not os.path.exists(model_path):
            os.makedirs(model_path)

        # define files to save the results on the test data
        test_out = os.path.join(model_path, 'train.out')

        # all variables in ckpt will be stored in the checkpoint
        # they are automatically loaded by the CheckpointManager if there is already a checkpoint in log_dir

        for i in range(len(self.models)):
            ckpt = tf.train.Checkpoint(epoch=tf.Variable(0), optimizer=self.optimizers[i], model=self.models[i])
            best_dir = os.path.join(model_path, str(i), 'best')
            ckpt_manager_best = tf.train.CheckpointManager(ckpt, best_dir, max_to_keep=max_to_keep)
            ckpt.restore(ckpt_manager_best.latest_checkpoint)

        # start TensorFlow session
        with tf.device(self.device):

            # start data queues
            coord = tf.train.Coordinator()

            # generate data queues
            test_dl = DataLoader(dc=test_dc, batch_size=test_batch_size, shuffle=False,
                                 device=self.device, capacity=capacity, coord=coord, daemon=True)

            data = test_dl.next_batch()

            # trigger graph compilation for tf.function()
            self._inference(data['N'], data['E'], data['F'], data['Z'], data['D'], data['Q'], data['R'], data['C'],
                            data['W'], data['idx_i'], data['idx_j'], data['offsets'], data['batch_seg'])

            n_rep = 1000

            time.sleep(1)

            # run inference time measurement
            start_time = time.time()

            for i in range(n_rep):
                result = self._inference(data['N'], data['E'], data['F'], data['Z'], data['D'], data['Q'], data['R'],
                                         data['C'], data['W'], data['idx_i'], data['idx_j'], data['offsets'],
                                         data['batch_seg'])

            end_time = time.time()

            f = open(test_out, "a+")
            f.write(f'Average inference time per batch: {(end_time - start_time) / n_rep:g} seconds\n')
            f.close()

            coord.request_stop()
