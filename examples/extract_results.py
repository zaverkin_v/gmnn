import os
import numpy as np


def search_string_in_file(file_name, string_to_search, idx):
    # search for the given string in file and return lines containing that string
    line_found = 0.0
    with open(file_name, 'r') as read_obj:
        for line in read_obj:
            if string_to_search in line:
                line_split = np.asarray(line.split()[idx:], dtype=np.float64)
                line_found=line_split
                break
    return line_found


def search_next_line_by_string(file_name, string_to_search, idx):
    # search for the given string in file and return lines containing that string
    line_found = 0.0
    with open(file_name, 'r') as read_obj:
        for line in read_obj:
            if string_to_search in line:
                line = read_obj.readline()
                line = read_obj.readline()
                line_split = np.asarray(line.split()[idx:], dtype=np.float64)
                line_found = line_split
                break
    return line_found


if __name__ == "__main__":
    # results[task_name][kernel_name][selection_name]
    results = {'ethanol_1000K': {}}
    
    all_metric_keys = ['ermse_ens', 'emae_ens', 'frmse_ens', 'fmae_ens', 'ermse', 'emae', 'frmse', 'fmae', 'epr',
                       'esr', 'emaxe', 'echem_acc', 'eq95', 'fpr', 'fsr', 'fmaxe', 'fchem_acc', 'fq95', 'n_at',
                       'n_train', 'n_pool', 'matrix_time', 'selection_time']
    
    for ds in ["ethanol_1000K"]:
        for kernel, selection in [("random", "max_diag"), ("qbc-force", "max_diag"), ("ae-force", "max_diag"),
                                  ("F_inv", "max_diag"), ("F_inv", "max_det_greedy"), ("g", "max_dist_greedy"),
                                  ("g", "lcmd_greedy")]:
            
            n_models = 3 if kernel == "qbc-energy" or kernel == "qbc-force" else 1
            if kernel not in results[ds]:
                results[ds].update({kernel: {}})
            results[ds][kernel].update({selection: []})

            for n_random_projections in [0, 512] if kernel in ["F_inv", "g"] else [0]:
                # ethanol: 10 -> 100 [2, 5, 10] (total 5000 structures, 4000 reserved as the pool data)
                step_configs = [(10, 100, 4000, [2, 5, 10])]

                for n_train, max_n_train, n_pool, max_al_batch_sizes in step_configs:
                    for max_al_batch_size in max_al_batch_sizes:
                        file = open('current_get_results.dat', 'w')
                        file.write(f'{ds} {kernel} {selection} {n_random_projections} {n_train} {max_n_train} {max_al_batch_size}')
                        file.close()
                        results[ds][kernel][selection].append([])
                        results[ds][kernel][selection][-1] = {'n_rp': n_random_projections, 'min_n_train': n_train, 'max_n_train': max_n_train, 'max_al_batch_size': max_al_batch_size}
                        for key in all_metric_keys:
                            results[ds][kernel][selection][-1].update({key: []})

                        for data_seed in range(5):
                            for key in all_metric_keys:
                                results[ds][kernel][selection][-1][key].append([])
                            for step in range((max_n_train - n_train) // max_al_batch_size + 1):
                                rootdir=f'models/{ds}_{kernel}_{selection}_{n_random_projections}_{n_train}_{max_n_train}_{max_al_batch_size}_{data_seed}/model/step_{step}'
                                local_results = search_string_in_file(rootdir + "/train.out", "Ensemble:", 1).tolist()
                                local_results.extend(search_string_in_file(rootdir + "/train.out", "Mean individual:", 2).tolist())
                                local_results.extend(search_string_in_file(rootdir + "/train.out", "Energy:", 1).tolist())
                                local_results.extend(search_string_in_file(rootdir + "/train.out", "Force:", 1).tolist())
                                local_results.extend(search_next_line_by_string(rootdir + "/train.out", "Mean-N_at", 0).tolist())

                                for ii in range(len(all_metric_keys)):
                                    results[ds][kernel][selection][-1][all_metric_keys[ii]][-1].append(local_results[ii])
    
    np.savez('all_results.npz', **results)
