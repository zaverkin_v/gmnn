from gmnn.layers import LinearLayer
from gmnn.utils import *
from gmnn.al_kernels import *
from gmnn.al_selection import *
from gmnn.data_pipeline import *
from scipy.stats.stats import pearsonr, spearmanr


class RandomProjections:
    """Stores random projections (if configured to use random projections)
    to allow multiple FeatureStatistics objects to use the same projections.

    :param models: List of models.
    :param n_random_features: If ``n_random_features = 0``, use last-layer gradients.
                              Otherwise, use random projections of all linear-layer gradients.
    """

    def __init__(self, models: List[tf.keras.layers.Layer], n_random_features: int = 0):
        self.linear_layers = [[l for l in model.model.layers_ if isinstance(l, LinearLayer)] for model in models]
        self.n_random_features = n_random_features
        if n_random_features > 0:
            self.input_projections = [tf.random.normal(shape=(l.in_features + 1, n_random_features))
                                      for l in self.linear_layers[0]]
            self.grad_output_projections = [tf.random.normal(shape=(l.out_features, n_random_features))
                                            for l in self.linear_layers[0]]


class FeatureStatistics:
    """Computes and stores feature statistics for converged models, such as (random projections of) gradients or
    variances.

    :param models: List of models.
    :param dc: ``DataContainer`` providing data.
    :param random_projections: Configuration for used random projections or last-layer gradients.
    :param batch_size: Mini-batch size to compute feature statistics.
    :param capacity: Capacity of pre-loading in DataLoader.
    :param pbc: Set ``True`` if periodic boundary conditions has to be implied.
    :param device: GPU index (if GPU is available).
    :param feature_layer: Use ``feature_layer`` gradients. Set ``feature_layer = -2`` to use last-layer gradients.
    :param force: Set ``True`` for evaluating with forces.
    """
    def __init__(self, models: List[tf.keras.layers.Layer], dc: DataContainer, random_projections: RandomProjections,
                 batch_size: int = 100, capacity: int = 5, pbc: bool = False, device: str = '/CPU:0',
                 feature_layer: int = -2, force: bool = False, EperA_errors: bool = False):
        self.models = models
        self.dc = dc
        self.random_projections = random_projections
        self.batch_size = batch_size
        self.capacity = capacity
        self.pbc = pbc
        self.device = device
        self.feature_layer = feature_layer
        self.force = force
        self.EperA_errors = EperA_errors

        self.ens_stats = None
        self.g = None
        self.F = None
        self.F_reg_inv = None
        self.inputs = None
        self.grad_outputs = None

    @tf.function(input_signature=[
        tf.TensorSpec([None, ], dtype=tf.int32),                # data['Z']
        tf.TensorSpec([None, None], dtype=tf.float32),          # data['R']
        tf.TensorSpec([None, None, None], dtype=tf.float32),    # data['C']
        tf.TensorSpec([None, ], dtype=tf.int32),                # data['idx_i']
        tf.TensorSpec([None, ], dtype=tf.int32),                # data['idx_j']
        tf.TensorSpec([None, None], dtype=tf.float32),          # data['offsets']
        tf.TensorSpec([None, ], dtype=tf.int32)                 # data['batch_seg']
    ])
    def _eval_force(self, Z, R, C, idx_i, idx_j, offsets, batch_seg):
        F_at_pred = []
        E_bp_pred = []
        for model in self.models:
            with tf.GradientTape(watch_accessed_variables=False) as force_tape:
                force_tape.watch(R)
                R_ij = get_distance_vectors(R, C, idx_i, idx_j, offsets, batch_seg, pbc=self.pbc)
                x = (R_ij, Z, idx_i, idx_j)
                E_at_pred = model(x, training=False)
                E_bp_pred.append(tf.math.segment_sum(E_at_pred, batch_seg))     # sum up atomic contributions
                E_total = tf.reduce_sum(E_bp_pred[-1])
            F_at_pred.append(-tf.convert_to_tensor(force_tape.gradient(E_total, R)))
        return E_bp_pred, F_at_pred

    @tf.function(input_signature=[
        tf.TensorSpec([None, ], dtype=tf.int32),                # data['Z']
        tf.TensorSpec([None, None], dtype=tf.float32),          # data['R']
        tf.TensorSpec([None, None, None], dtype=tf.float32),    # data['C']
        tf.TensorSpec([None, ], dtype=tf.int32),                # data['idx_i']
        tf.TensorSpec([None, ], dtype=tf.int32),                # data['idx_j']
        tf.TensorSpec([None, None], dtype=tf.float32),          # data['offsets']
        tf.TensorSpec([None, ], dtype=tf.int32)                 # data['batch_seg']
    ])
    def _eval_energy(self, Z, R, C, idx_i, idx_j, offsets, batch_seg):
        E_bp_pred = []
        for model in self.models:
            R_ij = get_distance_vectors(R, C, idx_i, idx_j, offsets, batch_seg, pbc=self.pbc)
            x = (R_ij, Z, idx_i, idx_j)
            E_at_pred = model(x, training=False)
            E_bp_pred.append(tf.math.segment_sum(E_at_pred, batch_seg))     # sum up atomic contributions
        return E_bp_pred

    def _compute_ens_stats(self, N, E, F, Z, D, Q, R, C, W, idx_i, idx_j, offsets, batch_seg, labeled_data: bool = False):
        if self.force:
            E_bp_pred, F_at_pred = self._eval_force(Z, R, C, idx_i, idx_j, offsets, batch_seg)
            E_bp_var = tf.math.reduce_std(E_bp_pred, axis=0)[:, 0] ** 2
            F_at_var = tf.math.reduce_std(F_at_pred, axis=0) ** 2
            if labeled_data:
                ens_stats = {'Energy-Var': E_bp_var,
                             'Force-Var': tf.math.segment_mean(tf.reduce_mean(F_at_var, axis=-1), batch_seg),
                             'Energy-AE': (sum(tf.abs(E[None, :, None] - E_bp_pred) / N[None, :, None])[:, 0] / len(self.models) if self.EperA_errors
                                           else sum(tf.abs(E[None, :, None] - E_bp_pred))[:, 0] / len(self.models)),
                             'Force-AE': tf.math.segment_mean(tf.reduce_mean(sum(tf.abs(F[None] - F_at_pred)) / len(self.models),
                                                                             axis=-1), batch_seg)}
            else:
                ens_stats = {'Energy-Var': E_bp_var,
                             'Force-Var': tf.math.segment_mean(tf.reduce_mean(F_at_var, axis=-1), batch_seg)}
            return ens_stats
        else:
            E_bp_pred = self._eval_energy(Z, R, C, idx_i, idx_j, offsets, batch_seg)
            E_bp_var = tf.math.reduce_std(E_bp_pred, axis=0)[:, 0] ** 2
            if labeled_data:
                ens_stats = {'Energy-Var': E_bp_var,
                             'Energy-AE': (sum(tf.abs(E[None, :, None] - E_bp_pred) / N[None, :, None])[:, 0] / len(self.models) if self.EperA_errors
                                           else sum(tf.abs(E[None, :, None] - E_bp_pred))[:, 0] / len(self.models))}
            else:
                ens_stats = {'Energy-Var': E_bp_var}
            return ens_stats

    def _compute_fisher(self, g):
        # m corresponds to n_models, c to n_candidates, i and j to n_weights
        return tf.einsum('mci, mcj -> mij', g, g)

    @tf.function(input_signature=[
        tf.TensorSpec([None, ], dtype=tf.int32),                # data['Z']
        tf.TensorSpec([None, None], dtype=tf.float32),          # data['R']
        tf.TensorSpec([None, None, None], dtype=tf.float32),    # data['C']
        tf.TensorSpec([None, ], dtype=tf.int32),                # data['idx_i']
        tf.TensorSpec([None, ], dtype=tf.int32),                # data['idx_j']
        tf.TensorSpec([None, None], dtype=tf.float32),          # data['offsets']
        tf.TensorSpec([None, ], dtype=tf.int32)                 # data['batch_seg']
    ])
    def _compute_fm(self, Z, R, C, idx_i, idx_j, offsets, batch_seg):
        if self.random_projections.n_random_features == 0:
            fm = []     # fm has shape n_models x n_candidates x n_weights
            for model, linear_layers in zip(self.models, self.random_projections.linear_layers):
                last_linear_layer = linear_layers[-1]
                with tf.GradientTape(watch_accessed_variables=False) as fm_tape:
                    fm_tape.watch(model.trainable_variables[self.feature_layer])
                    R_ij = get_distance_vectors(R, C, idx_i, idx_j, offsets, batch_seg, pbc=self.pbc)
                    x = (R_ij, Z, idx_i, idx_j)
                    E_at = model(x, training=False)[:, 0]
                    E_bp = tf.math.segment_sum(E_at, batch_seg)     # sum up atomic contributions
                weight_deriv = fm_tape.jacobian(E_bp, model.trainable_variables[self.feature_layer])
                fm.append(tf.concat([last_linear_layer.weight_factor * weight_deriv[:, :, 0],
                                     last_linear_layer.bias_factor * tf.ones_like(weight_deriv[:, 0:1, 0])], axis=1))
            return tf.convert_to_tensor(fm)
        else:
            fm = []
            for model, linear_layers in zip(self.models, self.random_projections.linear_layers):
                with tf.GradientTape(watch_accessed_variables=True, persistent=True) as fm_tape:
                    R_ij = get_distance_vectors(R, C, idx_i, idx_j, offsets, batch_seg, pbc=self.pbc)
                    x = (R_ij, Z, idx_i, idx_j)
                    E_at = model(x, training=False)[:, 0]
                    inputs = [tf.concat([l.weight_factor * l.last_input,
                                         l.bias_factor * tf.ones_like(l.last_input[:, 0:1])], axis=1)
                              for l in linear_layers]
                    outputs = [l.last_output for l in linear_layers]
                    E_total = tf.math.reduce_sum(E_at, axis=0)      # sum up atomic contributions
                grad_outputs = [tf.convert_to_tensor(fm_tape.gradient(E_total, out)) for out in outputs]
                # matmul sums over features, the result after segment_sum is n_structures x n_random_features
                # then sum over layers
                fm.append(sum([tf.math.segment_sum(tf.matmul(inp, inp_proj) * tf.matmul(go, go_proj), batch_seg)
                               for inp, inp_proj, go, go_proj
                               in zip(inputs, self.random_projections.input_projections, grad_outputs,
                                      self.random_projections.grad_output_projections)]))
            return tf.convert_to_tensor(fm)

    def get_ens_stats(self, labeled_data: bool = False):
        """

        :param labeled_data: Set ``True`` to get absolute errors for energy and force.
        :return: Dictionary with Ensembling (Query-by-Committee, QbC) statistics.
        """
        if self.ens_stats is None:
            with tf.device(self.device):
                # start data queues
                coord = tf.train.Coordinator()
                # data queue
                dl = DataLoader(dc=self.dc, batch_size=self.batch_size, shuffle=False, drop_last=False,
                                device=self.device, capacity=self.capacity, coord=coord, daemon=True)
                ens_stats = [self._compute_ens_stats(data['N'], data['E'], data['F'], data['Z'], data['D'], data['Q'],
                                                     data['R'], data['C'], data['W'], data['idx_i'], data['idx_j'],
                                                     data['offsets'], data['batch_seg'],
                                                     labeled_data=labeled_data) for data in dl]
                self.ens_stats = {key: tf.concat([es[key] for es in ens_stats], axis=0) for key in ens_stats[0]}
                coord.request_stop()
        return self.ens_stats

    def get_g(self):
        """

        :return: Feature vector of ``shape=(n_models, n_structures, n_features)``.
        """
        if self.g is None:
            with tf.device(self.device):
                # start data queues
                coord = tf.train.Coordinator()
                # data queue
                dl = DataLoader(dc=self.dc, batch_size=self.batch_size, shuffle=False, drop_last=False,
                                device=self.device, capacity=self.capacity, coord=coord, daemon=True)
                self.g = [self._compute_fm(data['Z'], data['R'], data['C'], data['idx_i'], data['idx_j'],
                                           data['offsets'], data['batch_seg']) for data in dl]
                self.g = tf.concat(self.g, axis=1)
                coord.request_stop()
        return self.g

    def get_F(self):
        """

        :return: Fisher matrix of ``shape=(n_models, n_features, n_features)``.
        """
        if self.F is None:
            self.F = self._compute_fisher(self.get_g())
        return self.F

    def get_F_reg_inv(self):
        """

        :return: Regularized inverse of Fisher matrix of ``shape=(n_models, n_features, n_features)``.
        """
        if self.F_reg_inv is None:
            F = self.get_F()
            g = self.get_g()
            # empirical regularisation
            lam = tf.linalg.trace(F) / (g.shape[1] * g.shape[2])
            self.F_train_reg_inv = tf.linalg.inv(F + lam * tf.eye(F.shape[1]))
        return self.F_train_reg_inv


class GenericALMethod:
    """Provides methods for selecting batches during active learning and evaluating model statistics.

    :param kernel: Name of the kernel, e.g. ``g``, ``F_inv``, ``qbc-energy``, ``qbc-force``.
                   ``random`` produces random selection and ``ae-energy`` and ``ae-force`` select by absolute errors
                   on the pool data, which is only possible if the pool data is already labeled.
    :param selection: Selection method, one of ``max_dist_greedy``, ``lcmd_greedy``, ``max_det_greedy`` or ``max_diag``.
    :param feature_layer: Use ``feature_layer`` gradients. Set ``feature_layer = -2`` to use last-layer gradients.
    :param pbc: Set ``True`` if periodic boundary conditions has to be implied.
    :param force: Set ``True`` for evaluating with forces.
    :param capacity: Capacity of data queue.
    :param device_number: GPU index (if GPU is available).
    :param n_random_features: If ``n_random_features = 0``, use last-layer gradients (according to feature_layer).
                              Otherwise, use random projections of all linear-layer gradients.
    """
    def __init__(self, kernel: str, selection: str, feature_layer: int = -2, pbc: bool = False, force: bool = False,
                 capacity: int = 5, device: str = '/CPU:0', n_random_features: int = 0, EperA_errors: bool = False):
        self.kernel = kernel
        self.selection = selection
        self.feature_layer = feature_layer
        self.force = force
        self.pbc = pbc
        self.capacity = capacity
        self.device = device
        self.n_random_features = n_random_features
        self.EperA_errors = EperA_errors

    def select(self, models: List[tf.keras.layers.Layer], dcs: Dict[str, DataContainer], batch_size: int = 100,
               max_al_batch_size: int = 100) -> Tuple[tf.Tensor, float, float, RandomProjections]:
        """Selects a new batch of active learning samples.

        :param models: List of models.
        :param dcs: List of ``DataContainer`` providing data.
        :param batch_size: Mini-batch size to compute feature statistics.
        :param max_al_batch_size: Maximal size of the selected AL batch.
        :return: Indices of the selected structures from the data pool, time for computing the ``KernelMatrix``,
                 time for performing the selection, and the ``RandomProjections`` used.
        """
        random_projections = RandomProjections(models, self.n_random_features)

        stats = {key: FeatureStatistics(models=models, dc=dc, random_projections=random_projections,
                                        batch_size=batch_size, capacity=self.capacity,
                                        pbc=self.pbc, device=self.device, feature_layer=self.feature_layer,
                                        force=self.force)
                 for key, dc in dcs.items()}

        if (self.kernel == 'qbc-energy' or self.kernel == 'qbc-force' or self.kernel == 'ae-energy' or
            self.kernel == 'ae-force' or self.kernel == 'random') and self.selection != 'max_diag':
            raise RuntimeError(f'{self.kernel} kernel can only be used with max_diag selection method,'
                               f' not with {self.selection}!')

        stats_list = [stats['pool'], stats['train']] if self.selection == 'max_dist_greedy' or \
                                                        self.selection == 'lcmd_greedy' else [stats['pool']]
        start_time = time.time()
        matrix = self._get_kernel_matrix(stats_list, stats['train'])
        matrix_time = time.time() - start_time

        start_time = time.time()
        if self.selection == 'max_dist_greedy':
            idxs = max_dist_greedy(matrix=matrix, batch_size=max_al_batch_size, n_train=len(dcs['train']))
        elif self.selection == 'max_diag':
            idxs = max_diag(matrix=matrix, batch_size=max_al_batch_size)
        elif self.selection == 'max_det_greedy':
            idxs = max_det_greedy(matrix=matrix, batch_size=max_al_batch_size)
        elif self.selection == 'lcmd_greedy':
            idxs = lcmd_greedy(matrix=matrix, batch_size=max_al_batch_size, n_train=len(dcs['train']))
        else:
            raise NotImplementedError(f"Unknown selection method '{self.selection}' for active learning!")
        selection_time = time.time() - start_time

        return idxs, matrix_time, selection_time, random_projections

    def eval(self, model_path: str, models: List[tf.keras.layers.Layer], dcs: Dict[str, DataContainer],
             batch_size: int = 100, random_projections: Optional[RandomProjections] = None):
        """Evaluates uncertainty metrics using labeled test data and writes results to a train.out file in
        ``model_path``.

        :param model_path: Path to the model.
        :param models: List of models.
        :param dcs: List of ``DataContainer`` providing data.
        :param batch_size: Mini-batch size to compute feature statistics.
        :param random_projections: Use ``RandomProjections`` from the selection step, if provided.
        """
        if random_projections is None:
            random_projections = RandomProjections(models, self.n_random_features)

        stats = {key: FeatureStatistics(models=models, dc=dc, random_projections=random_projections,
                                        batch_size=batch_size, capacity=self.capacity,
                                        pbc=self.pbc, device=self.device, feature_layer=self.feature_layer,
                                        force=self.force, EperA_errors=self.EperA_errors) for key, dc in dcs.items()}

        test_error = stats["test"].get_ens_stats(labeled_data=True)

        if self.selection == 'max_dist_greedy' or self.selection == 'lcmd_greedy':
            matrix = self._get_kernel_matrix([stats['test'], stats['train']], stats['train'])
            n_test, n_train = len(dcs["test"]), len(dcs["train"])
            variance = matrix.get_sq_dists(n_test)
            for i in range(n_test + 1, n_test + n_train):
                variance = tf.math.minimum(variance, matrix.get_sq_dists(i))
            variance = variance[:-n_train]
        elif self.selection == 'max_diag' or self.selection == 'max_det_greedy':
            matrix = self._get_kernel_matrix([stats['test']], stats['train'])
            variance = matrix.get_diag()
        else:
            raise NotImplementedError(f"Unknown selection method '{self.selection}' for active learning!")

        # define files to save the results on the test data
        test_out = model_path + '/train.out'
        write_results = ['PearsonR', 'SpearmanR', 'MaxError', '% < chem. acc.', '95% quantile']

        column_widths = [20] + [20] * len(write_results)
        f = open(test_out, "a+")
        f.write("".ljust(sum(column_widths), "=") + "\n")
        f.write(f'Active learning results obtained on the test data:\n')
        f.write(' \n')
        f.write(padded_str([''] + write_results, column_widths) + '\n')
        f.write("".ljust(sum(column_widths), "=") + "\n")

        energy_results = [pearsonr(variance, test_error['Energy-AE'])[0],
                          spearmanr(variance, test_error['Energy-AE'])[0],
                          np.max(test_error['Energy-AE']),
                          np.mean(tf.cast(test_error['Energy-AE'] < 1.0, tf.int32)),
                          np.quantile(test_error['Energy-AE'], 0.95)]

        f.write(padded_str(['Energy:'] + [f'{result:8.5f}' for result in energy_results], column_widths) + '\n')
        if self.force:
            force_results = [pearsonr(variance, test_error['Force-AE'])[0],
                             spearmanr(variance, test_error['Force-AE'])[0],
                             np.max(test_error['Force-AE']),
                             np.mean(tf.cast(test_error['Force-AE'] < 1.0, tf.int32)),
                             np.quantile(test_error['Force-AE'], 0.95)]
            f.write(padded_str(['Force:'] + [f'{result:8.5f}' for result in force_results], column_widths)
                    + '\n')
        f.write("".ljust(sum(column_widths), "=") + "\n")
        f.close()

    def _get_kernel_matrix(self, stats_list: List[FeatureStatistics], train_stats: FeatureStatistics):
        if self.kernel == 'g':
            return FeatureKernelMatrix(tf.concat([s.get_g() for s in stats_list], axis=1))
        elif self.kernel == 'F_inv':
            return FeatureCovKernelMatrix(tf.concat([s.get_g() for s in stats_list], axis=1),
                                          train_stats.get_F_reg_inv())
        elif self.kernel == 'qbc-energy':
            ens_vars = tf.concat([s.get_ens_stats()['Energy-Var'] for s in stats_list], axis=1)
            return DiagonalKernelMatrix(ens_vars)
        elif self.kernel == 'qbc-force':
            ens_vars = tf.concat([s.get_ens_stats()['Force-Var'] for s in stats_list], axis=1)
            return DiagonalKernelMatrix(ens_vars)
        elif self.kernel == 'ae-energy':
            ens_vars = tf.concat([s.get_ens_stats(labeled_data=True)['Energy-AE'] for s in stats_list], axis=1)
            return DiagonalKernelMatrix(ens_vars)
        elif self.kernel == 'ae-force':
            ens_vars = tf.concat([s.get_ens_stats(labeled_data=True)['Force-AE'] for s in stats_list], axis=1)
            return DiagonalKernelMatrix(ens_vars)
        elif self.kernel == 'random':
            return DiagonalKernelMatrix(tf.random.uniform([sum([len(s.dc) for s in stats_list])]))
        else:
            raise RuntimeError(f"Unknown active learning kernel {self.kernel}!")
